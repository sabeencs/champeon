<?php
require APPPATH . '/libraries/JWT.php';


class ImplementJwt
{
  

    //////////The function generate token/////////////
    PRIVATE $key1 = "qwerty@123456"; // url: https://www.youtube.com/watch?v=zD4IGp1lBWs
    PRIVATE $key = "QZ2SMr1uvpD56lSnbRRoiq2C16J3e9TFGNayUBxFwYXgNMC8O0LWTj1VT5zzgnvtJsHs9qWdIPxlmfybRDXHSYKvCc6ek5hmzNGAWZ2OoPBOd44oVEtfJIic3u4yEKU7"; // url: https://www.youtube.com/watch?v=zD4IGp1lBWs
//     PRIVATE $key2 = "Z19eKpVHFJN6kQQsATejDANwsyIYlRWMRj3zEPvCpXM97w0SPuG3In6LC2d4YubdbOttFBUk2WxiG9FKyrrYDzE31mmQUzCBBVx1wyikNZfaHHs7MWft0j8Xoc8lVbnD";
    public function GenerateToken($data)
    {         
        $jwt = JWT::encode($data, $this->key);
        return $jwt;
    }
   
    
   //////This function decode the token////////////////////
    public function DecodeToken($token)
    {         
        $decoded = JWT::decode($token, $this->key, array('HS256'));
        $decodedData = (array) $decoded;
        return $decodedData;
    }


}
?> 