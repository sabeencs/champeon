<!DOCTYPE html>
<html>
 <head>
  <title>CHAMPEON Main Page</title>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
 </head>
  <style>
.button {
  background-color: #4CAF50; /* Green */
  border: none;
  color: white;
  padding: 16px 32px;
  text-align: center;
  text-decoration: none;
  display: inline-block;
  font-size: 16px;
  margin: 4px 2px;
  transition-duration: 0.4s;
  cursor: pointer;
}

.button1 {
  margin-right:16px;
  background-color: white; 
  color: black; 
  border: 2px solid #4CAF50;
}

.button1:hover {
  background-color: #4CAF50;
  color: white;
}
.button1 {border-radius: 12px;}
    
.button1 {padding: 14px 35px;}
    
    hr { 
height:4px;
border-width:0;
color:purple;
background-color:purple;
    }
  </style>
  <body>
  <div class="container">
   <br />
   <a  href= "<?php echo base_url(); ?>web/v1/api/main">
            <img src="<?php echo base_url(); ?>uploads/item-gallery/champeon.png" align="right" width=15%>
          </a>
   <?php  
      if($this->session->userdata('username') != '')  
     {  
        echo '<h2>Welcome - '.$this->session->userdata('username').'</h2>';  

    ?> <button id="logoutButton" class="button1 float-right submit-button" >Logout</button>

    <?php                //echo '<label><a href="'.base_url().'web/v1/users/logout">Logout</a></label>';  
           }       else  
     {  
        redirect(base_url() . 'web/v1/users/login');  
     }
      
      ?>
        <hr>

   <h3 align="center">CHAMPEON Main Page</h3>
<!--    <br /> -->
    <hr>
    <button id="gamesButton" class="button1 float-left submit-button" >Games</button>
<!--        <br />
       <br /> -->
    <button id="tournamentsButton" class="button1 float-left submit-button" >Tournaments</button>
<!--        <br />
       <br /> -->
    <button id="matchesButton" class="button1 float-left submit-button" >Matches</button>
<!--        <br />
       <br /> -->
    <button id="teamsButton" class="button1 float-left submit-button" >Teams</button>

    <button id="poolsButton" class="button1 float-left submit-button" >Pools</button>
       <br />
       <br />
    <button id="teamPlayersButton" class="button1 float-left submit-button" >Team Players</button>
<!--        <br />
       <br /> -->
    <button id="finalPlayersButton" class="button1 float-left submit-button" >Final Players</button>
<!--        <br />
       <br /> -->

    <button id="changeMatchDetailsButton" class="button1 float-left submit-button" >Change Match Details</button>

    <hr>

    <button id="killFeedUpdateButton" class="button1 float-left submit-button" >Update Match Kill Feed</button>

    <button id="matchUpdateButton" class="button1 float-left submit-button" >Match Update Stats</button>

    <button id="teamPositionUpdateButton" class="button1 float-left submit-button" >Update Team Position</button>

    <button id="matchRefreshButton" class="button1 float-left submit-button" >Match Stats Refresh</button>

    <button id="killFeedsButton" class="button1 float-left submit-button" >Kill Feeds</button>

    <hr>
    
    <button id="createPlayerButton" class="button1 float-left submit-button" >Create New Player</button>
    
    <button id="createTeamButton" class="button1 float-left submit-button" >Create New Team</button>
    
    <hr>
    
    <button id="playerLogoUpdateButton" class="button1 float-left submit-button" >Update Player Logo</button>
    
    <button id="teamLogoUpdateButton" class="button1 float-left submit-button" >Update Team Logo</button>

    <hr>
    
    <button id="listUsersButton" class="button1 float-left submit-button" >Users</button>
    
    <button id="listTransactionsButton" class="button1 float-left submit-button" >Transactions</button>

    <hr>

    
<script type="text/javascript">
    document.getElementById("gamesButton").onclick = function () {
        location.href = "<?php echo base_url(); ?>web/v1/api/games";
    };
  
  document.getElementById("tournamentsButton").onclick = function () {
        location.href = "<?php echo base_url(); ?>web/v1/api/tournaments";
    };
  
  document.getElementById("matchesButton").onclick = function () {
        location.href = "<?php echo base_url(); ?>web/v1/api/matches";
    };

  document.getElementById("teamsButton").onclick = function () {
        location.href = "<?php echo base_url(); ?>web/v1/api/teams";
    };

  document.getElementById("poolsButton").onclick = function () {
        location.href = "<?php echo base_url(); ?>web/v1/api/pools";
    };

  document.getElementById("teamPlayersButton").onclick = function () {
        location.href = "<?php echo base_url(); ?>web/v1/api/team_players";
    };

  document.getElementById("finalPlayersButton").onclick = function () {
        location.href = "<?php echo base_url(); ?>web/v1/api/final_players";
    };

    document.getElementById("logoutButton").onclick = function () {
  location.href = "<?php echo base_url(); ?>web/v1/users/logout";
    };

    document.getElementById("changeMatchDetailsButton").onclick = function () {
  location.href = "<?php echo base_url(); ?>web/v1/api/match_status";
    };

    document.getElementById("matchUpdateButton").onclick = function () {
  location.href = "<?php echo base_url(); ?>web/v1/api/match_update";
    };

    document.getElementById("killFeedUpdateButton").onclick = function () {
  location.href = "<?php echo base_url(); ?>web/v1/api/kill_feed_update";
    };
  
    document.getElementById("killFeedsButton").onclick = function () {
  location.href = "<?php echo base_url(); ?>web/v1/api/kill_feed_list";
    };
  
    document.getElementById("teamPositionUpdateButton").onclick = function () {
  location.href = "<?php echo base_url(); ?>web/v1/api/team_position";
    };

  document.getElementById("createPlayerButton").onclick = function () {
  location.href = "<?php echo base_url(); ?>web/v1/api/players";
    };

  document.getElementById("createTeamButton").onclick = function () {
  location.href = "<?php echo base_url(); ?>web/v1/api/create_team";
    };

  document.getElementById("playerLogoUpdateButton").onclick = function () {
  location.href = "<?php echo base_url(); ?>web/v1/api/update_player_logo";
    };

  document.getElementById("teamLogoUpdateButton").onclick = function () {
  location.href = "<?php echo base_url(); ?>web/v1/api/update_team_logo";
    };

      document.getElementById("matchRefreshButton").onclick = function () {
  location.href = "<?php echo base_url(); ?>web/v1/api/match_refresh";
    };

  document.getElementById("listUsersButton").onclick = function () {
  location.href = "<?php echo base_url(); ?>web/v1/api/list_users";
    };

      document.getElementById("listTransactionsButton").onclick = function () {
  location.href = "<?php echo base_url(); ?>web/v1/api/list_transactions";
    };


    </script>
    
    
 </body>
</html>
