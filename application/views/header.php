<!DOCTYPE html>
<html lang="en">
<head>
  </head>
  <style>
html, body {
  background-color: #fff;
  color: #636b6f;
  font-family: 'Nunito', sans-serif;
  font-weight: 200;
  height: 100vh;
  margin: 0;
}
.button {
  background-color: #4CAF50; /* Green */
  border: none;
  color: white;
  padding: 16px 32px;
  text-align: center;
  text-decoration: none;
  display: inline-block;
  font-size: 16px;
  margin: 4px 2px;
  transition-duration: 0.4s;
  cursor: pointer;
}

.button1 {
  margin-left:20px;
  margin-right:250px;
  background-color: white; 
  color: black; 
  border: 2px solid #4CAF50;
}

.button1:hover {
  background-color: #4CAF50;
  color: white;
}
.button1 {border-radius: 12px;}
    
.button1 {padding: 14px 35px;}
</style>
  <body>
    
  
    <div class="container mt-5" >
        <br /> 
          <a  href= "<?php echo base_url(); ?>web/v1/api/main">
            <img src="<?php echo base_url(); ?>uploads/item-gallery/champeon.png" align="right" width=15%>
          </a>
          <?php  
          if($this->session->userdata('username') != '')  
              {  
                echo '<h2>Welcome - '.$this->session->userdata('username').'</h2>';  
                //echo '<label><a href="'.base_url().'web/v1/users/logout">Logout</a></label>';  
              }  
              else  
              {  
                redirect(base_url() . 'web/v1/users/login');  
              }
              
              ?>
        <div class="row">
          <button id="homeButton" class="button1 float-right submit-button" >Home</button>

          <button id="logoutButton" class="button1 float-right submit-button" >Logout</button>
          
          <script type="text/javascript">
            document.getElementById("homeButton").onclick = function () {
              location.href = "<?php echo base_url(); ?>web/v1/api/main";
            };

            document.getElementById("logoutButton").onclick = function () {
              location.href = "<?php echo base_url(); ?>web/v1/users/logout";
            };

          </script>
        </div>
    </div>
    
  </body>
</html>