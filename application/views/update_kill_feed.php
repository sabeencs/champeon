<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<title> Update Match Kill Feeds </title>
<!-- Fonts -->
<link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
<!-- Styles -->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" >
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<style>
html, body {
background-color: #fff;
color: #636b6f;
font-family: 'Nunito', sans-serif;
font-weight: 200;
height: 100vh;
margin: 0;
}
.full-height {
height: 100vh;
}
.flex-center {
align-items: center;
display: flex;
justify-content: center;
}
.position-ref {
position: relative;
}
.top-right {
position: absolute;
right: 10px;
top: 18px;
}
.content {
text-align: center;
}
.title {
font-size: 84px;
}
.links > a {
color: #636b6f;
padding: 0 25px;
font-size: 13px;
font-weight: 600;
letter-spacing: .1rem;
text-decoration: none;
text-transform: uppercase;
}
.m-b-md {
margin-bottom: 30px;
}
  .column {
  float: left;
  width: 98%;
  padding: 10px;
/*   height: 300px; /* Should be removed. Only for demonstration *-/ */
}
.column2 {
  float: left;
  width: 2%;
  padding: 10px;
/*   height: 300px; /* Should be removed. Only for demonstration *-/ */
}
.column3 {
  float: left;
  width: 30%;
  padding: 10px;
/*   height: 300px; /* Should be removed. Only for demonstration *-/ */
}
.column4 {
  float: left;
  width: 31%;
  padding: 10px;
/*   height: 300px; /* Should be removed. Only for demonstration *-/ */
}
.column5 {
  float: center;
  width: 95%;
  padding: 10px;
/*   height: 300px; /* Should be removed. Only for demonstration *-/ */
}
 .btn-lite {
    /* Green */
  border: white;
  color: white;
  padding: 15px;
  text-align: center;
  float: center;
   width: 40%;
  } 
</style>
</head>
  
  <body>
    
    <?php
    include('header.php');
    ?>


<div class="container mt-5">
  <div class="row">
    <div class="card">
      <div class="card-header">
        <h2 class="text-success">Update Match Stats </h2>
      </div>
       <div class="card-body"  style="background-color:#fcf;">
      <form>
        <div class="column4" style="background-color:#fff;">
        <div class="form-group">
        <label for="game">Game</label>
        <select class="form-control" id="game-dropdown">
          <option value="">Select Game</option>
          <?php
          // require_once "db.php";
          // $result = mysqli_query($conn,"SELECT * FROM countries");
          $games=$this->db->select('*')
                ->from('tb_games')
          			->where('is_active',1)
                ->get()->result_array();

                                                        //     print_r($games);
                                                        //         while($data = mysqli_fetch_array($games))
              foreach($games as $game_key => $game)
              {
                  echo "<option value='". $game['game_id'] ."'>" .$game['game_id'] .' - '. $game['game_name'] ."</option>";  // displaying data in option menu
              }
          ?>
          </select>
        </div>
        </div>
    <div class="column2" style="background-color:#fcf;">
    </div>

        <div class="column4" style="background-color:#fff;">
        <div class="form-group">
          <label for="tournament">Tournament</label>
          <select class="form-control" id="tournament-dropdown">
          <option value="">Select Tournament</option>
          </select>
        </div>                        
        </div>
    <div class="column2" style="background-color:#fcf;">
    </div>
        <div class="column4" style="background-color:#fff;">
        <div class="form-group">
          <label for="match">Matches</label>
          <select class="form-control" id="match-dropdown">
          <option value="">Select Match</option>
          </select>
        </div>
        </div>
        
<!--         <div class="form-group">
          <label for="team">Teams</label>
          <select class="form-control" id="team-dropdown">
          <option value="">Select Team</option>
          </select>
        </div>
        -->
        </form>
      </div>
    </div>
  </div>
</div> 
   <div class="container mt-5">

<!--    <form name="frmUser" method="post" action=""> -->
      <div class="row">
    <div class="column" style="background-color:#fff;">
     <div class="table-responsive">
      <table id="table1" class="table table-bordered table-striped ">
       <thead>
        <tr>
           <th width="6.5%">Sl. No</th>
           <th width="14%">Team Name</th>
           <th width="8.5%">Player Id</th>
           <th width="21%">Player Name</th>
           <th width="6.5%">Sl. No</th>
           <th width="14%">Team Name</th>
           <th width="8.5%">Player Id</th>
           <th width="21%">Player Name</th>
         </tr>
       </thead>
       <tbody></tbody>
      </table>
     </div>
    </div>
   </div>

      <form method="post" action="<?php echo base_url(); ?>web/v1/api/update_kill_feeds">  

         <div class="table-responsive">
          <table id=table2 class="table table-bordered table-striped">
           <thead>
<!--             <tr> -->
<!--                <th>sl_no</th> -->
<!--                <th>Player 1 Id</th>
               <th>Action</th>
               <th>Player 2 Id</th>
               <th>options</th> -->
<!--              </tr> -->
           </thead>
           <tbody>
          <tr>
              <td><input type="text" name="sl_no" class="form-control" value="1" readonly/></td>
              <td><input type="text" name="Player1_1" class="form-control" value="0"/></td>
              <td><input type="text" name="Action_1" class="form-control" value="0"/></td>
              <td><input type="text" name="Player2_1" class="form-control" value="0"/></td>
<!--               <td><input type="button" id="remove_row" value="-" class="btn btn-lite" style="background-color: #FF0000;" /></td> -->
            </tr>
<!--                 <tr>
              <td><input type="text" name="Player1_2" class="form-control" value="0"/></td>
              <td><input type="text" name="Action_2" class="form-control" value="0"/></td>
              <td><input type="text" name="Player2_2" class="form-control" value="0"/></td>
              <td><input type="button" id="remove_row" value="-" class="btn btn-lite" style="background-color: #FF0000;" /></td>
            </tr>
             <tr>
              <td><input type="text" name="Player1_3" class="form-control" value="0"/></td>
              <td><input type="text" name="Action_3" class="form-control" value="0"/></td>
              <td><input type="text" name="Player2_3" class="form-control" value="0"/></td>
              <td><input type="button" id="remove_row" value="-" class="btn btn-lite" style="background-color: #FF0000;" /></td>
            </tr> -->
            </tbody>
          </table>
        </div>
        <button type="button" id="addRow" onclick="addRow()" style="background-color: #4CAF50;">Add Item</button>
        <div align="center" style="margin-bottom:15px;">
          <td><input type="submit" name="submit" value="Submit" class="btn btn-info" /></td>
        </div>
      </form>
   
        

    
  </div> 

<script>
$(document).ready(function() {
  var tournament_id1;
  var match_id1;

  $('#game-dropdown').on('change', function() {
    var game_id = this.value;
    $.ajax({
      url: "<?php echo base_url(); ?>web/v1/api/fetch_tournaments_dropdown",
      type: "POST",
      data: {
        game_id: game_id
      },
      cache: false,
      success: function(tournaments){
        $("#tournament-dropdown").html(tournaments);
        $('#match-dropdown').html('<option value="">Select Tournament First</option>'); 
        $('#team-dropdown').html('<option value="">Select Tournament First</option>'); 
      }
    });
  });
    $('#tournament-dropdown').on('change', function() {
    var tournament_id = this.value;
     tournament_id1 = tournament_id;
    $.ajax({
      url: "<?php echo base_url(); ?>web/v1/api/fetch_matches_dropdown",
      type: "POST",
      data: {
        tournament_id: tournament_id
      },
      cache: false,
      success: function(result){
//         fetch_header(tournament_id1);
        $("#match-dropdown").html(result);
        $('#team-dropdown').html('<option value="">Select Match First</option>'); 
      }
    });
  });
    $('#match-dropdown').on('change', function() {
    var match_id = this.value;
      match_id1 = match_id;
      fetch_header_with_matchid(match_id1);
     $.ajax({
//        url: "<?php //echo base_url(); ?>web/v1/api/fetch_final_players",
        url: "<?php echo base_url(); ?>web/v1/api/fetch_players_ids",
       type: "POST",
          data: {
            match_id: match_id1
          },   success:function(data)
       {
//         $('#player_list').html(data);
//         $('tbody').html(data);
         $("#table1 > tbody").append(data);
       }
      });
  });

//   var i = 1;
//   $("#add_row").click(function() {
//   $('tr').find('input').prop('disabled',true)
//     $('#addr' + i).html("<td>" + (i + 1) + "</td> <td><input type='text' name='Player1' id='Player1' class='form-control' value='0'/></td> <td><input type="text" name="Action" id="Action" class="form-control" value="0"/></td> <td><input type='text' name='Player2' id='Player2' class='form-control' value='0'/></td>    <td><input type='button' id='add_row' value='+' class='btn btn-lite' style='background-color: #4CAF50;' />     <input type='button' id='remove_row' value='-' class='btn btn-lite' style='background-color: #FF0000;' /></td>            ");

//     $('#tab_logic').append('<tr id="addr' + (i + 1) + '"></tr>');
//     i++;
//   });
  
// $('#team-dropdown').on('change', function() {
//     var team_id = this.value;
//   var tournament_id = tournament_id1;
//     fetch_data3(team_id,tournament_id);
//     });

//  function fetch_data3($id,$id2)
//  {
//   var team_id = $id;
//   var tournament_id = $id2;
//   $.ajax({
//    url: "<?php //echo base_url(); ?>web/v1/api/fetch_data3",
//    type: "POST",
//       data: {
//         team_id: team_id,
//         tournament_id: tournament_id
//       },   success:function(data)
//    {
//     $('tbody').html(data);
//    }
//   });
//  }
  
//    function  setUpdateAction()
//   {
//     document.frmUser.action = "<?php //echo base_url(); ?>web/v1/api/update_mp_kills";
//     document.frmUser.submit();
//   }
  
   function fetch_header_with_matchid($id)
  {
  var match_id = $id;
  $.ajax({
   url: "<?php echo base_url(); ?>web/v1/api/fetch_header_with_matchid",
   type: "POST",
      data: {
        match_id: match_id
      },   success:function(data)
   {
     $("#table2 > thead").append(data);
   }
  });
 } 
  
  
//    var items = 0;
//     function addRow() {
//         items++;
 
//         var html = "<tr>";
//             html += "<td><input type='text' name='Player1_'" + items + "class='form-control' value='0'/></td>";
// //       "<td>" + items + "</td>";
// //             html += "<td><input type='text' name='itemName[]'></td>";
// //             html += "<td><input type='number' name='itemQuantity[]'></td>";
//             html += "<td><input type='text' name='Action_'" + items + "class='form-control' value='0'/></td>";
//             html += "<td><input type='text' name='Player2_'" + items + "class='form-control' value='0'/></td>";
//             html += "<td><button type='button' onclick='deleteRow(this);'>Delete</button></td>";
//         html += "</tr>";
// // //         var table_rows = document.getElementById("table2");
// // //         var row = table.insertRow(0);
// // //         var cell1 = row.insertCell(0);
// // //         var cell2 = row.insertCell(1);
// // //         var cell3 = row.insertCell(2);
// // //         cell1.innerHTML = "NEW CELL1";
// // //         cell2.innerHTML = "NEW CELL2";
// // //         cell3.innerHTML = "NEW CELL3";
//       var row = document.getElementById("tbody1").insertRow();
//       row.innerHTML = html;
//     }
  
  
     var items = 2;
      $("#addRow").click(function() {
//       $('tr').find('input').prop('disabled',true)
//       $('#addr' + i).html("<td>" + (i + 1) + "</td><td><input type='text' name='uid" + i + "'  placeholder='User ID' class='form-control input-md'/></td><td><input type='text' name='uname" + i + "' placeholder='Name' class='form-control input-md'/></td><td><input type='text' name='nic" + i + "' placeholder='NIC' class='form-control input-md'/></td><td><input type='text' name='amount" + i + "' placeholder='Amount' class='form-control input-md'/></td><td><input type='date' name='dt" + i + "' placeholder='Date' class='form-control input-md'/></td>");
        var html_code = "<tr>"; 
            html_code += "<td><input type='text' name='sl_no' class='form-control' value='" + items + "' readonly/></td>";
            html_code += "<td><input type='text' name='Player1_" + items + "' class='form-control' value='0'/></td>";
            html_code += "<td><input type='text' name='Action_" + items + "' class='form-control' value='0'/></td>";
            html_code += "<td><input type='text' name='Player2_" + items + "' class='form-control' value='0'/></td>";
//             html_code += "<td><button type='button' onclick='deleteRow(this);'>Delete</button></td>";
        html_code += "</tr>";

      $("#table2 > tbody").append(html_code);
      items++;
    }); 
  
  
    function deleteRow(button) {
        button.parentElement.parentElement.remove();
        // first parentElement will be td and second will be tr.
    }
  
//   function fetch_match_current_stats($id)
//  {
//   var match_id = $id;
//   $.ajax({
//    url: "<?php //echo base_url(); ?>web/v1/api/fetch_match_current_stats",
//    type: "POST",
//       data: {
//         match_id: match_id
//       },   success:function(data)
//    {
//     $('tbody').html(data);
//    }
//   });
//  } 
});
</script>
</body>
</html>


