<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<title> Tournaments </title>
<!-- Fonts -->
<link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
<!-- Styles -->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" >
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<style>
html, body {
background-color: #fff;
color: #636b6f;
font-family: 'Nunito', sans-serif;
font-weight: 200;
height: 100vh;
margin: 0;
}
.full-height {
height: 100vh;
}
.flex-center {
align-items: center;
display: flex;
justify-content: center;
}
.position-ref {
position: relative;
}
.top-right {
position: absolute;
right: 10px;
top: 18px;
}
.content {
text-align: center;
}
.title {
font-size: 84px;
}
.links > a {
color: #636b6f;
padding: 0 25px;
font-size: 13px;
font-weight: 600;
letter-spacing: .1rem;
text-decoration: none;
text-transform: uppercase;
}
.m-b-md {
margin-bottom: 30px;
}
  /* Create two equal columns that floats next to each other */
.column {
  float: left;
  width: 68%;
  padding: 10px;
/*   height: 300px; /* Should be removed. Only for demonstration *-/ */
}
.column2 {
  float: left;
  width: 2%;
  padding: 10px;
/*   height: 300px; /* Should be removed. Only for demonstration *-/ */
}
.column3 {
  float: left;
  width: 30%;
  padding: 10px;
/*   height: 300px; /* Should be removed. Only for demonstration *-/ */
}

</style>
  <body>

    <?php
    include('header.php');
    ?>
    

      <div class="container mt-5">

    <div class="row">
    <div class="card">
      <div class="card-header">
        <h2 class="text-success">Tournament List </h2>
      </div>
      <div class="card-body"  style="background-color:#fcf;">
      <form>
        <div class="form-group">
        <label for="game">Game</label>
        <select class="form-control" id="game-dropdown">
          <option value="">Select Game</option>
          <?php
          // require_once "db.php";
          // $result = mysqli_query($conn,"SELECT * FROM countries");
          $games=$this->db->select('*')
                ->from('tb_games')
          			->where('is_active',1)
                ->get()->result_array();

                                                        //     print_r($games);
                                                        //         while($data = mysqli_fetch_array($games))
              foreach($games as $game_key => $game)
              {
                  echo "<option value='". $game['game_id'] ."'>" .$game['game_id'] .' - '. $game['game_name'] ."</option>";  // displaying data in option menu
              }
          ?>
          </select>
        </div>
        
<!--         <div class="form-group">
          <label for="tournament">Tournament</label>
          <select class="form-control" id="tournament-dropdown">
          <option value="">Select Tournament</option>
          </select>
        </div>                        

        <div class="form-group">
          <label for="match">Matches</label>
          <select class="form-control" id="match-dropdown">
          <option value="">Select Match</option>
          </select>
        </div>
         -->
<!--         <div class="form-group">
          <label for="team">Teams</label>
          <select class="form-control" id="team-dropdown">
          <option value="">Select Team</option>
          </select>
        </div>
        -->
        </form>
      </div>
    </div>
  </div>
</div> 
  <div class="container mt-5">

<!--    <form name="frmUser" method="post" action=""> -->
      <div class="row">
    <div class="column" style="background-color:#fff;">
     <div class="table-responsive">
      <table class="table table-bordered table-striped">
       <thead>
        <tr>
           <th>Sl. No</th>
           <th>Tournament Id</th>
           <th>Tournament Name</th>
           <th>Tournament Type</th>
         </tr>
       </thead>
       <tbody></tbody>
      </table>
     </div>
    </div>
    <div class="column2" style="background-color:#fff;">
    </div>
    <div class="column3" style="background-color:#d0e1e1;">

      <form method="post" action="<?php echo base_url(); ?>web/v1/api/tournament_validation">  
<!--          <select class="form-control" id="game-dropdown"> -->
<!--           <option value="">Select Game</option> -->
          <label for="game_id">Select Game:</label>
           <select id="game_id" name="game_id">
              <option value="">Select a Game</option>
          <?php
          // require_once "db.php";
          // $result = mysqli_query($conn,"SELECT * FROM countries");
          $games=$this->db->select('*')
                ->from('tb_games')
          			->where('is_active',1)
                ->get()->result_array();

                                                        //         while($data = mysqli_fetch_array($games))
              foreach($games as $game_key => $game)
              {
                  echo "<option value='". $game['game_id'] ."'>" .$game['game_id'] .' - '. $game['game_name'] ."</option>";  // displaying data in option menu
              }
          ?>
          </select>
         <div class="form-group">  
             <label>Enter Tournament Name</label>  
             <input type="text" name="tournament_name" class="form-control" />  
          </div>  
          <input type="radio" id="multi" name="tournament_type" value="multi"><label for="multi">Multi Player</label><br>
          <input type="radio" id="battle" name="tournament_type" value="battle"><label for="battle">Battle Royale</label><br>

        <div align="center" style="margin-bottom:15px;">
           <button type="submit" name="submit" value="Submit" class="btn btn-success btn-l">Add Tournament</button>
         </div>    
      </form>
        
    </div>
   </div>
  </div> 

<script>
$(document).ready(function() {
//   var tournament_id1;
   var game_id1=0;

  $('#game-dropdown').on('change', function() {
    var game_id = this.value;
    game_id1=game_id;
    fetch_tournaments(game_id1);
  });
 
 
  
  function fetch_tournaments($id)
 {
  var game_id = $id;
  $.ajax({
   url: "<?php echo base_url(); ?>web/v1/api/fetch_tournaments",
   type: "POST",
      data: {
        game_id: game_id
      },   success:function(data)
   {
    $('tbody').html(data);
   }
  });
 } 
  
});
</script>
</body>
</html>


