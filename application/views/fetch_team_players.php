<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<title> Team Players </title>
<!-- Fonts -->
<link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
<!-- Styles -->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" >
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<style>
html, body {
background-color: #fff;
color: #636b6f;
font-family: 'Nunito', sans-serif;
font-weight: 200;
height: 100vh;
margin: 0;
}
.full-height {
height: 100vh;
}
.flex-center {
align-items: center;
display: flex;
justify-content: center;
}
.position-ref {
position: relative;
}
.top-right {
position: absolute;
right: 10px;
top: 18px;
}
.content {
text-align: center;
}
.title {
font-size: 84px;
}
.links > a {
color: #636b6f;
padding: 0 25px;
font-size: 13px;
font-weight: 600;
letter-spacing: .1rem;
text-decoration: none;
text-transform: uppercase;
}
.m-b-md {
margin-bottom: 30px;
}
    /* Create two equal columns that floats next to each other */
.column {
  float: left;
  width: 68%;
  padding: 10px;
/*   height: 300px; /* Should be removed. Only for demonstration *-/ */
}
.column2 {
  float: left;
  width: 2%;
  padding: 10px;
/*   height: 300px; /* Should be removed. Only for demonstration *-/ */
}
.column3 {
  float: left;
  width: 30%;
  padding: 10px;
/*   height: 300px; /* Should be removed. Only for demonstration *-/ */
}
.column4 {
  float: left;
  width: 23%;
  padding: 10px;
/*   height: 300px; /* Should be removed. Only for demonstration *-/ */
}

 
</style>
  <body>

    <?php
    include('header.php');
    ?>

      <div class="container mt-5">
        <!--   <div class="row"> -->
    <div class="card">
      <div class="card-header">
        <h2 class="text-success">Team Players List </h2>
      </div>
      <div class="card-body" style="background-color:#fcf;">
      <form>
        <div class="column4" style="background-color:#fff;">
        <div class="form-group">
        <label for="game">Game</label>
        <select class="form-control" id="game-dropdown">
          <option value="">Select Game</option>
          <?php
          // require_once "db.php";
          // $result = mysqli_query($conn,"SELECT * FROM countries");
          $games=$this->db->select('*')
                ->from('tb_games')
          			->where('is_active',1)
                ->get()->result_array();

                                                        //     print_r($games);
                                                        //         while($data = mysqli_fetch_array($games))
              foreach($games as $game_key => $game)
              {
                  echo "<option value='". $game['game_id'] ."'>" .$game['game_id'] .' - '. $game['game_name'] ."</option>";  // displaying data in option menu
              }
          ?>
          </select>
        </div>
        </div>
    <div class="column2" style="background-color:#fcf;">
    </div>

        <div class="column4" style="background-color:#fff;">
        <div class="form-group">
          <label for="tournament">Tournament</label>
          <select class="form-control" id="tournament-dropdown">
          <option value="">Select Tournament</option>
          </select>
        </div>                        
        </div>
    <div class="column2" style="background-color:#fcf;">
    </div>
        <div class="column4" style="background-color:#fff;">
        <div class="form-group">
          <label for="match">Matches</label>
          <select class="form-control" id="match-dropdown">
          <option value="">Select Match</option>
          </select>
        </div>
        </div>
        
    <div class="column2" style="background-color:#fcf;">
    </div>
        <div class="column4" style="background-color:#fff;">
          <div class="form-group">
          <label for="team">Teams</label>
          <select class="form-control" id="team-dropdown">
          <option value="">Select Team</option>
          </select>
        </div>
        </div>
        </form>
      </div>
    </div>
<!--   </div> -->
</div> 
  <div class="container mt-5">

<!--    <form name="frmUser" method="post" action=""> -->
      <div class="row">
    <div class="column" style="background-color:#fff;">
     <div class="table-responsive">
      <table class="table table-bordered table-striped">
       <thead>
        <tr>
           <th>Sl. No</th>
<!--            <th>Team Name</th> -->
           <th>Player Id</th>
           <th>Player Name</th>
         </tr>
       </thead>
       <tbody></tbody>
      </table>
     </div>
    </div>
    <div class="column2" style="background-color:#fff;">
    </div>
    <div class="column3" style="background-color:#d0e1e1;">

      <form method="post" action="<?php echo base_url(); ?>web/v1/api/update_selected_team_players">  
<!--          <select class="form-control" id="game-dropdown"> -->
<!--           <option value="">Select Game</option> -->
                <label for="game">Game</label>
        <select class="form-control" id="game-dropdown2">
          <option value="">Select Game</option>
          <?php
          // require_once "db.php";
          // $result = mysqli_query($conn,"SELECT * FROM countries");
          $games=$this->db->select('*')
                ->from('tb_games')
          			->where('is_active',1)
                ->get()->result_array();

                                                        //     print_r($games);
                                                        //         while($data = mysqli_fetch_array($games))
              foreach($games as $game_key => $game)
              {
                  echo "<option value='". $game['game_id'] ."'>" .$game['game_id'] .' - '. $game['game_name'] ."</option>";  // displaying data in option menu
              }
          ?>
        </select>
          
          <div class="form-group">
            <label for="tournament">Tournament</label>
            <select class="form-control" name="tournament_id" id="tournament-dropdown2">
              <option value="">Select Tournament</option>
            </select>
          </div>    
          
          <div class="form-group">
            <label for="match">Match</label>
            <select class="form-control" name="match_id" id="match-dropdown2">
              <option value="">Select Match</option>
            </select>
          </div>    

          <div class="form-group">
            <label for="team">Team</label>
            <select class="form-control" name="team_id" id="team-dropdown2">
              <option value="">Select Team</option>
            </select>
          </div>
        
         <div class="table-responsive">
          <table class="table table-bordered table-striped">
           <thead2>
<!--             <tr>
               <th>Select</th> -->
    <!--            <th>Team Name</th> -->
<!--                <th>Player Id</th>
               <th>Player Name</th>
             </tr> -->
           </thead2>
           <tbody2></tbody2>
          </table>
         </div>
        
        <div align="center" style="margin-bottom:15px;">
           <button type="submit" name="add_submit" value="Submit" class="btn btn-success btn-l">Add Player</button>
         </div> 
        <?php
//         if(isset($_POST['add_submit'])){
        
//           $numberofCheckbox = count($_POST['sel_players']);
//            $j = 0;
//           while($j < $numberofCheckbox){
//             echo "player id is : ". $_POST['sel_players'][$j] . "\n";
//             $j++;
//           }
//         }
        
        ?>
        
      </form>
        
    </div>
   </div>
        

    
  </div> 

<script>
$(document).ready(function() {
  var tournament_id1;
  var tournament_id2;
   var game_id1=0;
  var match_id1;
  var match_id2;

  $('#game-dropdown').on('change', function() {
    var game_id = this.value;
    $.ajax({
      url: "<?php echo base_url(); ?>web/v1/api/fetch_tournaments_dropdown",
      type: "POST",
      data: {
        game_id: game_id
      },
      cache: false,
      success: function(tournaments){
        $("#tournament-dropdown").html(tournaments);
        $('#match-dropdown').html('<option value="">Select Tournament First</option>'); 
      }
    });
  });
  
  $('#tournament-dropdown').on('change', function() {
    var tournament_id = this.value;
    tournament_id1 = this.value;
    $.ajax({
      url: "<?php echo base_url(); ?>web/v1/api/fetch_matches_dropdown",
      type: "POST",
      data: {
        tournament_id: tournament_id
      },
      cache: false,
      success: function(data){
        $("#match-dropdown").html(data);
      }
    });
  });
 
  $('#match-dropdown').on('change', function() {
    var match_id = this.value;
     match_id1 = match_id;
    $.ajax({
      url: "<?php echo base_url(); ?>web/v1/api/fetch_teams_dropdown",
      type: "POST",
      data: {
        match_id: match_id
      },
      cache: false,
      success: function(result){
        $("#team-dropdown").html(result);
      }
     });
    });

$('#team-dropdown').on('change', function() {
    var team_id = this.value;
  var tournament_id = tournament_id1;
    fetch_players_in_teams(team_id,tournament_id);
    });
  
 $('#game-dropdown2').on('change', function() {
    var game_id = this.value;
    $.ajax({
      url: "<?php echo base_url(); ?>web/v1/api/fetch_tournaments_dropdown",
      type: "POST",
      data: {
        game_id: game_id
      },
      cache: false,
      success: function(tournaments){
        $("#tournament-dropdown2").html(tournaments);
        $('#match-dropdown2').html('<option value="">Select Tournament First</option>'); 
        $('#team-dropdown2').html('<option value="">Select Tournament First</option>'); 
      }
    });
  });
  
  $('#tournament-dropdown2').on('change', function() {
    var tournament_id = this.value;
    var tournament_id2 = this.value;
    $.ajax({
      url: "<?php echo base_url(); ?>web/v1/api/fetch_matches_dropdown",
      type: "POST",
      data: {
        tournament_id: tournament_id
      },
      cache: false,
      success: function(data){
        $("#match-dropdown2").html(data);
         $('#team-dropdown2').html('<option value="">Select Match First</option>'); 
      }
    });
  });
 
  $('#match-dropdown2').on('change', function() {
    var match_id = this.value;
     match_id2 = match_id;
      $.ajax({
      url: "<?php echo base_url(); ?>web/v1/api/fetch_teams_dropdown",
      type: "POST",
      data: {
        match_id: match_id
      },
      cache: false,
      success: function(result){
        $("#team-dropdown2").html(result);
      }
     });
    });

$('#team-dropdown2').on('change', function() {
    var team_id = this.value;
//   var tournament_id = tournament_id2;
     fetch_players_in_teams2(team_id);
    });

 function fetch_players_in_teams($id,$id2)
 {
  var team_id = $id;
  var tournament_id = $id2;
  $.ajax({
   url: "<?php echo base_url(); ?>web/v1/api/fetch_players_in_teams",
   type: "POST",
      data: {
        team_id: team_id,
        tournament_id: tournament_id
      },   success:function(data)
   {
    $('tbody').html(data);
   }
  });
 }
  
   function fetch_players_in_teams2($id)
 {
  var team_id = $id;
  $.ajax({
   url: "<?php echo base_url(); ?>web/v1/api/fetch_players_in_teams2",
   type: "POST",
      data: {
        team_id: team_id
      },   success:function(data)
   {
    $('tbody2').html(data);
   }
  });
 }
  
});
</script>
</body>
</html>


