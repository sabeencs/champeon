<!DOCTYPE html>
<html>
 <head>
  <title>Players</title>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
 </head>
<style>
* {
  box-sizing: border-box;
}

/* Create two equal columns that floats next to each other */
.column {
  float: left;
  width: 45%;
  padding: 10px;
/*   height: 300px; /* Should be removed. Only for demonstration *-/ */
}
.column2 {
  float: left;
  width: 2%;
  padding: 10px;
/*   height: 300px; /* Should be removed. Only for demonstration *-/ */
}

/* Clear floats after the columns */
.row:after {
  content: "";
  display: table;
  clear: both;
}
 
</style>
  <body>

    <?php
    include('header.php');
    ?>
    
    <div class="container">
   <br />
   
   <h3 align="center">Players</h3>
   <br />
  <div class="row">
    <div class="column" style="background-color:#fff;">
     <div class="table-responsive">
      <table class="table table-bordered table-striped">
       <thead>
        <tr>
         <th>Sl. No</th>
         <th>Player Id</th>
         <th>Player Name</th>
         <th>Team Name</th>
        </tr>
       </thead>
       <tbody></tbody>
      </table>
     </div>
    </div>
    <div class="column2" style="background-color:#fff;">
    </div>
    <div class="column" style="background-color:#d0e1e1;">
     <form method="post" action="<?php echo base_url(); ?>web/v1/api/player_validation">  

        <div class="form-group">  
           <label>Enter Player Name</label>  
           <input type="text" name="player_name" class="form-control" />  
        </div>  
        <div class="form-group">
        <label for="player_mp_role">Player Multiplayer Role</label>
        <select class="form-control" name="player_mp_role" id="mprole-dropdown">
          <option value="Left" >Left</option>
          <option value="Right">Right</option>
          <option value="Middle" >Middle</option>
        </select>
        </div>
        <div class="form-group">
        <label for="player_br_role">Player Battle Royale Role</label>
        <select class="form-control"  name="player_br_role" id="brrole-dropdown">
          <option value="Assualt">Assualt</option>
          <option value="Snipper">Snipper</option>
          <option value="Medic">Medic</option>
          <option value="Support">Support</option>
        </select>
        </div>
       <div class="form-group">  
           <label for="team_name">Team Name</label>  
           <select class="form-control"  name="team_name" id="team-dropdown">
          <option value="">Select Team</option>
          <?php
          // require_once "db.php";
          // $result = mysqli_query($conn,"SELECT * FROM countries");
          $teams=$this->db->select('team_id , team_name')
                ->from('tb_teams')
                ->get()->result_array();

                                                        //     print_r($games);
                                                        //         while($data = mysqli_fetch_array($games))
              foreach($teams as $team_key => $team)
              {
                  echo "<option value='". $team['team_id'] ."'>" .$team['team_id'] .' - '. $team['team_name'] ."</option>";  // displaying data in option menu
              }
          ?>
          </select>
        </div>  
       
      <div align="center" style="margin-bottom:15px;">
        <button type="submit" name="submit" value="Submit" class="btn btn-success btn-l">Add Player</button>
      </div>
      </form>

    
<!--     </div>
   </div>
     <form action="<?php //echo base_url(); ?>payment/payment/create_payment" method="post">


               <div class="col-50">
                  <label for="totalAmounnt">TotalAmount</label>
                  <input type="text" id="" name="amount" placeholder="" value="0">
              </div>
              <button class="btn btn-warning mt-3" type="submit">Proceed to Buy</button>
        </form>
  </div>
     -->

 </body>
</html>

<script type="text/javascript">
$(document).ready(function(){

 fetch_players();

 function fetch_players()
 {
  $.ajax({
   url: "<?php echo base_url(); ?>web/v1/api/fetch_players",
   success:function(data)
   {
    $('tbody').html(data);
   }
  });
 }
});
</script>