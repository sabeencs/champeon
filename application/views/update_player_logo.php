<!DOCTYPE html>
<html>
 <head>
  <title>Update Player Logo</title>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
 </head>
<style>
* {
  box-sizing: border-box;
}

/* Create two equal columns that floats next to each other */
.column {
  float: left;
  width: 45%;
  padding: 10px;
/*   height: 300px; /* Should be removed. Only for demonstration *-/ */
}
.column2 {
  float: left;
  width: 2%;
  padding: 10px;
/*   height: 300px; /* Should be removed. Only for demonstration *-/ */
}

/* Clear floats after the columns */
.row:after {
  content: "";
  display: table;
  clear: both;
}
 
</style>
  <body>

    <?php
    include('header.php');
    ?>
    
    <div class="container">
   <br />
   
   <h3 align="center">Player Logo</h3>
   <br />
  <div class="row">

    <div class="column" style="background-color:#d0e1e1;">
     <form action="<?php echo base_url(); ?>web/v1/api/player_logo_upload" method="post" enctype="multipart/form-data">
    
      <label for="player_id">Player Name</label>  
       <select class="form-control"  name="player_id" id="player-dropdown">
      <option value="">Select Player</option>
      <?php
      // require_once "db.php";
      // $result = mysqli_query($conn,"SELECT * FROM countries");
      $players=$this->db->select('player_id , player_name')
            ->from('tb_players')
            ->get()->result_array();

                                                    //     print_r($games);
                                                    //         while($data = mysqli_fetch_array($games))
          foreach($players as $player_key => $player)
          {
              echo "<option value='". $player['player_id'] ."'>" .$player['player_id'] .' - '. $player['player_name'] ."</option>";  // displaying data in option menu
          }
      ?>
      </select>

       <label>Select Image File to Upload:</label>  

       <input type="file" name="image">

        <div align="center" style="margin-bottom:15px;">
          <button type="submit" name="submit" value="submit" class="btn btn-success btn-l" disabled >Update Logo</button>
        </div>
      
      </form>
    </div>
  </div>

      <br>
      <br>

      <div name="image2">
        <img id="image2" src="<?php echo base_url(); ?>uploads/item-gallery/2020/12/7dc73b12979a40f93e871ec68ed56822_300x300.png"/>
<!--         <img src="" /> -->
      </div>
<!--     </div>
   </div>
     <form action="<?php //echo base_url(); ?>payment/payment/create_payment" method="post">


               <div class="col-50">
                  <label for="totalAmounnt">TotalAmount</label>
                  <input type="text" id="" name="amount" placeholder="" value="0">
              </div>
              <button class="btn btn-warning mt-3" type="submit">Proceed to Buy</button>
        </form>
  </div>
     -->

 </body>
</html>

<script type="text/javascript">
$(document).ready(function(){
$("#image2").hide();
//   $('button:submit').attr('disabled',true);
  
  $('input:file').on('change', function() {
                if ($(this).val()){
//                      $('button:submit').attr('disabled',false);; 
                  $('button:submit').removeAttr('disabled'); 
                }
                else {
                    $('button:submit').attr('disabled',true);
                }
            });
  
  $('#player-dropdown').on('change', function() {
    var player_id = this.value;
    fetch_current_logo(player_id);
    });

 function fetch_current_logo($id)
 {
 var player_id = $id;
  $.ajax({
   url: "<?php echo base_url(); ?>web/v1/api/fetch_current_player_logo",
   type: "POST",
      data: {
        player_id: player_id
      },   success:function(data)
    {
//       window.alert(data);
      $('#image2').attr("src",data);
      $("#image2").show();
   }
  });
 }
});
</script>