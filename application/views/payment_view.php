<html>
  <head>
    <title> RazorPay Integration in PHP - phpexpertise.com </title>
    <meta name="viewport" content="width=device-width">
    <style>
      .razorpay-payment-button {
        color: #ffffff !important;
        background-color: #7266ba;
        border-color: #7266ba;
        font-size: 14px;
        padding: 10px;
      }
    </style>
  </head>
  <body>
    <form method="POST" action="<?php echo base_url(); ?>payment/payment/create_payment">
    <!-- Note that the amount is in paise = 50 INR -->
      <input type="text" name="amount" id="amount" class="form-control" value="0"/>
        <div align="center" style="margin-bottom:15px;">
           <button type="submit" name="submit" value="Submit" class="btn btn-success btn-l">Add Amount</button>
         </div>    
    </form>
  </body>
</html>