<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<title> Match Refresh </title>
<!-- Fonts -->
<link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
<!-- Styles -->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" >
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<style>
html, body {
background-color: #fff;
color: #636b6f;
font-family: 'Nunito', sans-serif;
font-weight: 200;
height: 100vh;
margin: 0;
}
.full-height {
height: 100vh;
}
.flex-center {
align-items: center;
display: flex;
justify-content: center;
}
.position-ref {
position: relative;
}
.top-right {
position: absolute;
right: 10px;
top: 18px;
}
.content {
text-align: center;
}
.title {
font-size: 84px;
}
.links > a {
color: #636b6f;
padding: 0 25px;
font-size: 13px;
font-weight: 600;
letter-spacing: .1rem;
text-decoration: none;
text-transform: uppercase;
}
.m-b-md {
margin-bottom: 30px;
}
    /* Create two equal columns that floats next to each other */
.column {
  float: left;
/*   width: 90%; */
  padding: 10px;
/*   height: 300px; /* Should be removed. Only for demonstration *-/ */
}
.column2 {
  float: left;
  width: 2%;
  padding: 10px;
/*   height: 300px; /* Should be removed. Only for demonstration *-/ */
}
.column3 {
  float: left;
  width: 30%;
  padding: 10px;
/*   height: 300px; /* Should be removed. Only for demonstration *-/ */
}
.column4 {
  float: left;
  width: 31%;
  padding: 10px;
/*   height: 300px; /* Should be removed. Only for demonstration *-/ */
}

</style>
  <body>

    <?php
    include('header.php');
    ?>

      <div class="container mt-5">

        <!--   <div class="row"> -->
    <div class="card">
      <div class="card-header">
        <h2 class="text-success">Match Refresh </h2>
      </div>
      <div class="card-body"  style="background-color:#fcf;">
      <form>
        <div class="column4" style="background-color:#fff;">
        <div class="form-group">
        <label for="game">Game</label>
        <select class="form-control" id="game-dropdown">
          <option value="">Select Game</option>
          <?php
          // require_once "db.php";
          // $result = mysqli_query($conn,"SELECT * FROM countries");
          $games=$this->db->select('*')
                ->from('tb_games')
          			->where('is_active',1)
                ->get()->result_array();

                                                        //     print_r($games);
                                                        //         while($data = mysqli_fetch_array($games))
              foreach($games as $game_key => $game)
              {
                  echo "<option value='". $game['game_id'] ."'>" .$game['game_id'] .' - '. $game['game_name'] ."</option>";  // displaying data in option menu
              }
          ?>
          </select>
        </div>
        </div>
    <div class="column2" style="background-color:#fcf;">
    </div>

        <div class="column4" style="background-color:#fff;">
        <div class="form-group">
          <label for="tournament">Tournament</label>
          <select class="form-control" id="tournament-dropdown">
          <option value="">Select Tournament</option>
          </select>
        </div>                        
        </div>
    <div class="column2" style="background-color:#fcf;">
    </div>
        <div class="column4" style="background-color:#fff;">
        <div class="form-group">
          <label for="match">Matches</label>
          <select class="form-control" id="match-dropdown">
          <option value="">Select Match</option>
          </select>
        </div>
        </div>
        
<!--         <div class="form-group">
          <label for="team">Teams</label>
          <select class="form-control" id="team-dropdown">
          <option value="">Select Team</option>
          </select>
        </div>
        -->
        </form>
      </div>
    </div>
<!--   </div> -->
</div> 
  <div class="container mt-5">

<!--    <form name="frmUser" method="post" action=""> -->
      <div class="row">
        <form method="post" action="<?php echo base_url(); ?>web/v1/api/point_refresh">  
          <div class="column" style="background-color:#fff;">
           <div class="table-responsive">
            <table class="table table-bordered table-striped">
             <thead>
              <tr>
                 <th>Match Id</th>
                 <th>Match Name</th>
                 <th>Match Time</th>
                 <th>Match Status</th>
               </tr>
             </thead>
             <tbody></tbody>
            </table>
           </div>
          <div align="center" style="margin-bottom:15px;">
            <td><button type="submit" name="update" value="Submit" class="btn btn-info" disabled/>Refresh</td>
  <!--     <button type="button" name="update_button" id="update_button" class="btn btn-success btn-l">Update</button> -->
       </div>
      </div>

    </form>
   </div>
  </div> 

<script>
$(document).ready(function() {
//   var tournament_id1;
   var game_id1=0;
  var match_id1;

  $('#game-dropdown').on('change', function() {
    var game_id = this.value;
    $.ajax({
      url: "<?php echo base_url(); ?>web/v1/api/fetch_tournaments_dropdown",
      type: "POST",
      data: {
        game_id: game_id
      },
      cache: false,
      success: function(tournaments){
        $("#tournament-dropdown").html(tournaments);
        $('#match-dropdown').html('<option value="">Select Tournament First</option>'); 
      }
    });
  });
  
  $('#tournament-dropdown').on('change', function() {
    var tournament_id = this.value;
    $.ajax({
      url: "<?php echo base_url(); ?>web/v1/api/fetch_matches_dropdown",
      type: "POST",
      data: {
        tournament_id: tournament_id
      },
      cache: false,
      success: function(data){
        $("#match-dropdown").html(data);
      }
    });
  });
 
  $('#match-dropdown').on('change', function() {
    var match_id = this.value;
     match_id1 = match_id;
    fetch_match_refesh_status(match_id1);
     if (match_id1){
//                      $('button:submit').attr('disabled',false);; 
                  $('button:submit').removeAttr('disabled'); 
                }
                else {
                    $('button:submit').attr('disabled',true);
                }
  });
  

  
  function fetch_match_refesh_status($id)
 {
  var match_id = $id;
  $.ajax({
   url: "<?php echo base_url(); ?>web/v1/api/fetch_match_refesh_status",
   type: "POST",
      data: {
        match_id: match_id
      },   success:function(data)
   {
    $('tbody').html(data);
   }
  });
 } 
  
});
</script>
</body>
</html>


