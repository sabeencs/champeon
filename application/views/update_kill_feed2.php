<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<title> Update Match Kill Feeds </title>
<!-- Fonts -->
<link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
<!-- Styles -->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" >
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<style>
html, body {
background-color: #fff;
color: #636b6f;
font-family: 'Nunito', sans-serif;
font-weight: 200;
height: 100vh;
margin: 0;
}
.full-height {
height: 100vh;
}
.flex-center {
align-items: center;
display: flex;
justify-content: center;
}
.position-ref {
position: relative;
}
.top-right {
position: absolute;
right: 10px;
top: 18px;
}
.content {
text-align: center;
}
.title {
font-size: 84px;
}
.links > a {
color: #636b6f;
padding: 0 25px;
font-size: 13px;
font-weight: 600;
letter-spacing: .1rem;
text-decoration: none;
text-transform: uppercase;
}
.m-b-md {
margin-bottom: 30px;
}
  .column {
  float: left;
  width: 68%;
  padding: 10px;
/*   height: 300px; /* Should be removed. Only for demonstration *-/ */
}
.column2 {
  float: left;
  width: 2%;
  padding: 10px;
/*   height: 300px; /* Should be removed. Only for demonstration *-/ */
}
.column3 {
  float: left;
  width: 30%;
  padding: 10px;
/*   height: 300px; /* Should be removed. Only for demonstration *-/ */
}
.column4 {
  float: left;
  width: 31%;
  padding: 10px;
/*   height: 300px; /* Should be removed. Only for demonstration *-/ */
}
.column5 {
  float: center;
  width: 95%;
  padding: 10px;
/*   height: 300px; /* Should be removed. Only for demonstration *-/ */
}
  
</style>
</head>
  
  <body>
    
    <?php
    include('header.php');
    ?>


<div class="container mt-5">
  <div class="row">
    <div class="card">
      <div class="card-header">
        <h2 class="text-success">Update Match Stats </h2>
      </div>
       <div class="card-body"  style="background-color:#fcf;">
      <form>
        <div class="column4" style="background-color:#fff;">
        <div class="form-group">
        <label for="game">Game</label>
        <select class="form-control" id="game-dropdown">
          <option value="">Select Game</option>
          <?php
          // require_once "db.php";
          // $result = mysqli_query($conn,"SELECT * FROM countries");
          $games=$this->db->select('*')
                ->from('tb_games')
          			->where('is_active',1)
                ->get()->result_array();

                                                        //     print_r($games);
                                                        //         while($data = mysqli_fetch_array($games))
              foreach($games as $game_key => $game)
              {
                  echo "<option value='". $game['game_id'] ."'>" .$game['game_id'] .' - '. $game['game_name'] ."</option>";  // displaying data in option menu
              }
          ?>
          </select>
        </div>
        </div>
    <div class="column2" style="background-color:#fcf;">
    </div>

        <div class="column4" style="background-color:#fff;">
        <div class="form-group">
          <label for="tournament">Tournament</label>
          <select class="form-control" id="tournament-dropdown">
          <option value="">Select Tournament</option>
          </select>
        </div>                        
        </div>
    <div class="column2" style="background-color:#fcf;">
    </div>
        <div class="column4" style="background-color:#fff;">
        <div class="form-group">
          <label for="match">Matches</label>
          <select class="form-control" id="match-dropdown">
          <option value="">Select Match</option>
          </select>
        </div>
        </div>
        
<!--         <div class="form-group">
          <label for="team">Teams</label>
          <select class="form-control" id="team-dropdown">
          <option value="">Select Team</option>
          </select>
        </div>
        -->
        </form>
      </div>
    </div>
  </div>
</div> 
  <div class="container mt-5">

<!--    <form name="frmUser" method="post" action=""> -->
      <form method="post" action="<?php echo base_url(); ?>web/v1/api/update_kill_feeds2">  
        <div class="table-responsive">
        <table class="table table-bordered table-striped">
         <thead>
         </thead>
         <tbody>
                 <tr>
                   <td width="8%"> <input type="text" name="sl_no" class="form-control" value="1" readonly/> </td>
                   <td width="36%">
                     <div class="column5" style="background-color:#bfa;">
                      <div class="form-group">
                          <select class="form-control" name="player-dropdown1" id="player-dropdown1">
                            <option value="1">Select Player</option>
                          </select>
                      </div>
                    </div>
                   </td>
                   <td width="20%">
                     <div class="column5" style="background-color:#bfa;">
                      <div class="form-group">
                          <select class="form-control" name="action-dropdown1" id="action-dropdown1">
                            <option value="2">Select Action</option>
                            <option value="kill">kill</option>
                            <option value="knock">knock</option>
                          </select>
                      </div>
                    </div>
                   </td>
                   <td width="36%">
                     <div class="column5" style="background-color:#bfa;">
                      <div class="form-group">
                          <select class="form-control" name="player-dropdown2" id="player-dropdown2">
                            <option value="3">Select Player</option>
                          </select>
                      </div>
                    </div>
                   </td>
                 </tr>


          </tbody> 
        </table>
    
    <button type="button" id="addRow" onclick="addRow()" style="background-color: #4CAF50;">Add Item</button>
       <div align="center" style="margin-bottom:15px;">
              <td><input type="submit" name="submit" value="Submit" class="btn btn-info" /></td>
    <!--     <button type="button" name="update_button" id="update_button" class="btn btn-success btn-l">Update</button> -->
       </div>
     </div>
    </form>
       

    
  </div> 

<script>
$(document).ready(function() {
  var tournament_id1;
  var match_id1;
  var new_item=2;
  $('#game-dropdown').on('change', function() {
    var game_id = this.value;
    $.ajax({
      url: "<?php echo base_url(); ?>web/v1/api/fetch_tournaments_dropdown",
      type: "POST",
      data: {
        game_id: game_id
      },
      cache: false,
      success: function(tournaments){
        $("#tournament-dropdown").html(tournaments);
        $('#match-dropdown').html('<option value="">Select Tournament First</option>'); 
        $('#team-dropdown').html('<option value="">Select Tournament First</option>'); 
      }
    });
  });
    $('#tournament-dropdown').on('change', function() {
    var tournament_id = this.value;
     tournament_id1 = tournament_id;
    $.ajax({
      url: "<?php echo base_url(); ?>web/v1/api/fetch_matches_dropdown",
      type: "POST",
      data: {
        tournament_id: tournament_id
      },
      cache: false,
      success: function(result){
//         fetch_header(tournament_id1);
        $("#match-dropdown").html(result);
        $('#team-dropdown').html('<option value="">Select Match First</option>'); 
      }
    });
  });
    $('#match-dropdown').on('change', function() {
    var match_id = this.value;
      match_id1 = match_id;
     fetch_header_with_matchid(match_id1);
    $.ajax({
      url: "<?php echo base_url(); ?>web/v1/api/fetch_players_dropdown",
//       url: "<?php //echo base_url(); ?>web/v1/api/fetch_players_id",
      type: "POST",
      data: {
        match_id: match_id
      },
      cache: false,
      success: function(result){
        for(var i=1;i<=new_item;i++)
          {
           $("#player-dropdown" + i).html(result);
          }
         
//         $("#player-dropdown1").html(result);
//         $("#player-dropdown2").html(result);
//         $("#player-dropdown3").html(result);
//         $("#player-dropdown4").html(result);
        
      }
    });
  });

  
   var items = 2;
  
      $("#addRow").click(function() {
//       $('tr').find('input').prop('disabled',true)
//       $('#addr' + i).html("<td>" + (i + 1) + "</td><td><input type='text' name='uid" + i + "'  placeholder='User ID' class='form-control input-md'/></td><td><input type='text' name='uname" + i + "' placeholder='Name' class='form-control input-md'/></td><td><input type='text' name='nic" + i + "' placeholder='NIC' class='form-control input-md'/></td><td><input type='text' name='amount" + i + "' placeholder='Amount' class='form-control input-md'/></td><td><input type='date' name='dt" + i + "' placeholder='Date' class='form-control input-md'/></td>");
          new_item = (items * 2) - 1;
          var html_code = "<tr>"; 
              html_code += "<td width='8%'> <input type='text' name='sl_no' class='form-control' value='" + items + "' readonly/></td>";
              html_code += "<td width='36%'>";
              html_code += "<div class='column5' style='background-color:#bfa;'>";
              html_code += "<div class='form-group'>";
//               html_code += "<label for='player" + new_item + "'>Player " + new_item + "</label>";
              html_code += "<select class='form-control' name='player-dropdown" + new_item + "' id='player-dropdown" + new_item + "'>";
              html_code += "<option value=''>Select Player</option>";
              html_code += "</select>";
              html_code += "</div>";
              html_code += "</div>";
              html_code += "</td>";
        
              html_code += "<td width='20%'>";
              html_code += "<div class='column5' style='background-color:#bfa;'>";
              html_code += "<div class='form-group'>";
//               html_code += "<label for='action" + items + "'>Action</label>";
              html_code += "<select class='form-control' name='action-dropdown" + items + "' id='action-dropdown" + items + "'>";
              html_code += "<option value=''>Select Action</option>";
              html_code += "<option value='kill'>kill</option>";
              html_code += "<option value='knock'>knock</option>";
              html_code += "</select>";
              html_code += "</div>";
              html_code += "</div>";
              html_code += "</td>";
        
              new_item++;
              
              html_code += "<td width='36%'>";
              html_code += "<div class='column5' style='background-color:#bfa;'>";
              html_code += "<div class='form-group'>";
//               html_code += "<label for='player" + new_item + "'>Player " + new_item + "</label>";
              html_code += "<select class='form-control' name='player-dropdown" + new_item + "' id='player-dropdown" + new_item + "'>";
              html_code += "<option value=''>Select Player</option>";
              html_code += "</select>";
              html_code += "</div>";
              html_code += "</div>";
              html_code += "</td>";
              html_code += "</tr>";
     
        
      items++;
        
      $('tbody').append(html_code);
        
      $.ajax({
      url: "<?php echo base_url(); ?>web/v1/api/fetch_players_dropdown",
      type: "POST",
      data: {
        match_id: match_id1
      },
      cache: false,
      success: function(result){
        for(var j=new_item-1;j<=new_item;j++)
          {
           $("#player-dropdown" + j).html(result);
          }
      }
    });  
        
        
    }); 
  
// $('#team-dropdown').on('change', function() {
//     var team_id = this.value;
//   var tournament_id = tournament_id1;
//     fetch_data3(team_id,tournament_id);
//     });

//  function fetch_data3($id,$id2)
//  {
//   var team_id = $id;
//   var tournament_id = $id2;
//   $.ajax({
//    url: "<?php //echo base_url(); ?>web/v1/api/fetch_data3",
//    type: "POST",
//       data: {
//         team_id: team_id,
//         tournament_id: tournament_id
//       },   success:function(data)
//    {
//     $('tbody').html(data);
//    }
//   });
//  }
  
//    function  setUpdateAction()
//   {
//     document.frmUser.action = "<?php //echo base_url(); ?>web/v1/api/update_mp_kills";
//     document.frmUser.submit();
//   }
  
   function fetch_header_with_matchid($id)
  {
  var match_id = $id;
  $.ajax({
   url: "<?php echo base_url(); ?>web/v1/api/fetch_header_with_matchid",
   type: "POST",
      data: {
        match_id: match_id
      },   success:function(data)
   {
    $('thead').html(data);
   }
  });
 } 
  
  
//   $('#player-dropdown1').on('change', function() {
//     var player1 = this.value;
//    });
  
//   function fetch_match_current_stats($id)
//  {
//   var match_id = $id;
//   $.ajax({
//    url: "<?php //echo base_url(); ?>web/v1/api/fetch_match_current_stats",
//    type: "POST",
//       data: {
//         match_id: match_id
//       },   success:function(data)
//    {
//     $('tbody').html(data);
//    }
//   });
//  } 
});
</script>
</body>
</html>


