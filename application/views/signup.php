<!DOCTYPE html>  
 <html>  
 <head>  
      <title>ESPORTS | <?php echo $title; ?></title>  
      <link rel="stylesheet" 

href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />  
 </head>  
 <body>  
      <div class="container">  
           <br /><br /><br />  
           <form method="post" action="<?php echo base_url(); ?>api_old/signup_validation">  
                <div class="form-group">  
                     <label>Enter Email</label>  
                     <input type="text" name="email" class="form-control" />  
                     <span class="text-danger"><?php echo form_error('email'); ?></span>                 
                </div>  
                <div class="form-group">  
                     <label>Enter Password</label>  
                     <input type="password" name="password" class="form-control" />  
                     <span class="text-danger"><?php echo form_error('password'); ?></span>                 
                </div>  
                <div class="form-group">  
                     <label>Enter mobile</label>  
                     <input type="text" name="mobile" class="form-control" />  
                     <span class="text-danger"><?php echo form_error('mobile'); ?></span>  
                </div>  
                <div class="form-group">  
                     <label>Enter Username</label>  
                     <input type="text" name="username" class="form-control" />  
                     <span class="text-danger"><?php echo form_error('username'); ?></span>                 
                </div>  
                <div class="form-group">  
                     <label>Enter First Name</label>  
                     <input type="text" name="first_name" class="form-control" />  
                     <span class="text-danger"><?php echo form_error('first_name'); ?></span>                 
                </div>  
                <div class="form-group">  
                     <label>Enter Last Name</label>  
                     <input type="text" name="last_name" class="form-control" />  
                     <span class="text-danger"><?php echo form_error('last_name'); ?></span>                 
                </div>  
                <div class="form-group">  
                     <input type="submit" name="insert" value="signup" class="btn btn-info" />  
                     <?php echo '<label class="text-danger">'.$this->session->flashdata("error").'</label>'; ?>  
                </div>  
           </form>  
      </div>  
 </body>  
 </html>