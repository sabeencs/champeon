<!DOCTYPE html>
<html>
 <head>
  <title>Games</title>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
 </head>
<style>
* {
  box-sizing: border-box;
}

/* Create two equal columns that floats next to each other */
.column {
  float: left;
  width: 45%;
  padding: 10px;
/*   height: 300px; /* Should be removed. Only for demonstration *-/ */
}
.column2 {
  float: left;
  width: 2%;
  padding: 10px;
/*   height: 300px; /* Should be removed. Only for demonstration *-/ */
}

/* Clear floats after the columns */
.row:after {
  content: "";
  display: table;
  clear: both;
}
 
</style>
  <body>

    <?php
    include('header.php');
    ?>
    
    <div class="container">
   <br />
   
   <h3 align="center">Games</h3>
   <br />
  <div class="row">
    <div class="column" style="background-color:#fff;">
     <div class="table-responsive">
      <table class="table table-bordered table-striped">
       <thead>
        <tr>
         <th>Sl. No</th>
         <th>Game Id</th>
         <th>Game Name</th>
  <!--        <th>Delete</th> -->
        </tr>
       </thead>
       <tbody></tbody>
      </table>
     </div>
    </div>
    <div class="column2" style="background-color:#fff;">
    </div>
    <div class="column" style="background-color:#d0e1e1;">
     <form method="post" action="<?php echo base_url(); ?>web/v1/api/games_validation">  

        <div class="form-group">  
           <label>Enter Game Name</label>  
           <input type="text" name="game_name" class="form-control" />  
        </div>  
       
      <div align="center" style="margin-bottom:15px;">
        <button type="submit" name="submit" value="Submit" class="btn btn-success btn-l">Add Game</button>
      </div>
      </form>

    
<!--     </div>
   </div>
     <form action="<?php //echo base_url(); ?>payment/payment/create_payment" method="post">


               <div class="col-50">
                  <label for="totalAmounnt">TotalAmount</label>
                  <input type="text" id="" name="amount" placeholder="" value="0">
              </div>
              <button class="btn btn-warning mt-3" type="submit">Proceed to Buy</button>
        </form>
  </div>
     -->

 </body>
</html>

<script type="text/javascript">
$(document).ready(function(){

 fetch_games();

 function fetch_games()
 {
  $.ajax({
   url: "<?php echo base_url(); ?>web/v1/api/fetch_games",
   success:function(data)
   {
    $('tbody').html(data);
   }
  });
 }
});
</script>