<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Api extends CI_Controller
{
  
  public function __construct()
	{
		parent::__construct();
// 		$this->load->model('api_model');
		$this->load->library('form_validation');
    date_default_timezone_set('Asia/Kolkata');
	}

    //----------------------------------------------------------------------------------/// 

 function main(){
   $this->load->view('web_main_view.php');
 }
  
  //----------------------------------------------------------------------------------/// 

 function games(){
   $this->load->view('fetch_games.php');
 }
  
     //----------------------------------------------------------------------------------/// 

 function games_validation(){
   $game_name	=	$this->input->post('game_name');

   $location = base_url()."web/v1/api/games";


   if($game_name == null)
   {
     echo '<script>alert(" No Names Entered..!!")</script>'; 
     echo '<META HTTP-EQUIV="Refresh" Content="0; URL='.$location.'">';
   }
   else
   {
     $game_details = $this->db->select('*')
                      ->from('tb_games')
                      ->where('game_name',$game_name)
                      ->get()->result_array();
  //                     ->count_all_results();   

     $games_count = count($game_details,0);  


     if( $games_count > 0 )
     {
       if($game_details[0]['is_active'] == 1)
       {
         echo '<script>alert(" FAILED . . !! \n Game Name Already Exists..!!")</script>'; 
         echo '<META HTTP-EQUIV="Refresh" Content="0; URL='.$location.'">';
       }
       else
       {
         $active_status = array(
            'is_active' => 1
          );

         $this->db->where('game_id', $game_details[0]['game_id']);
         $this->db->update('tb_games', $active_status); 
         echo '<script>alert(" REACTIVATED . . !! \n Game is Reactivated..!!")</script>'; 
         echo '<META HTTP-EQUIV="Refresh" Content="0; URL='.$location.'">';
       }
     }
     else
     {

       $data = array(
          'game_name'		=>	$this->input->post('game_name'),
          'is_active'		=>	1
        );

      $this->db->insert('tb_games', $data);

      echo '<script>alert(" Success . . !! \n Game Inserted..!!")</script>'; 
      echo '<META HTTP-EQUIV="Refresh" Content="0; URL='.$location.'">';

     }
   }
 }
  
    //------------------------------------------------------------------------------------------------------------------///

  
  function fetch_games(){
    $games=$this->db->select('*')
                ->from('tb_games')
                ->where('is_active',1)
                ->get()->result_array();
   			 
  $output = '';

if(count($games) > 0)
{
  $i=1;
 foreach($games as $game_key => $game)
 {
  $output .= '
  <tr>
     <td><input type="text" name="sl_no" id="sl_no" class="form-control" value="'.$i.'" readonly/></td>
     <td><input type="text" name="game_id" id="game_id" class="form-control" value="'.$game['game_id'].'" readonly/></td>
     <td><input type="text" name="game_name" id="game_name" class="form-control" value="'.$game['game_name'].'" readonly/></td>
  </tr>
';
   $i++;
 }
}
else
{
 $output .= '
 <tr>
  <td colspan="4" align="center">No Data Found</td>
 </tr>
 ';
}

echo $output;
}
  
    //------------------------------------------------------------------------------------------------------------------///

  function tournaments(){
   $this->load->view('fetch_tournaments.php');
 }

  //------------------------------------------------------------------------------------------------------------------///
 
 function fetch_tournaments(){
		
   $game_id = $this->input->post('game_id');
 
   $tournaments = $this->db->select(' tournament_id , tournament_name , tournament_type ')
			->from('tb_tournaments')
  	  ->where('fk_game_id',$game_id)
			->get()->result_array();

$output = '';

if(count($tournaments) > 0)
{
  $i=1;
   foreach($tournaments as $tournament_key => $tournament)
 {
     if($tournament['tournament_type'] == 7000)
    {
        $tournament['tournament_type'] = "Multi Player";
    }  
    else //($tournament['tournament_type'] == 6000)
    {
        $tournament['tournament_type'] = "Battle Royale";
    }  
  $output .= '
  <tr>
   <td><input type="text" name="sl_no" id="sl_no" class="form-control" value="'.$i.'" readonly/></td>
   <td><input type="text" name="tournament_id" id="tournament_id" class="form-control" value="'.$tournament['tournament_id'].'" readonly/></td>
   <td><input type="text" name="tournament_name" id="tournament_name" class="form-control" value="'.$tournament['tournament_name'].'" readonly/></td>
   <td><input type="text" name="tournament_type" id="tournament_type" class="form-control" value="'.$tournament['tournament_type'].'" readonly/></td>
  </tr>
  ';
   $i++;
 }
}
else
{
 $output .= '
 <tr>
  <td colspan="4" align="center">No Data Found</td>
 </tr>
 ';
}

echo $output;
}

  
      //------------------------------------------------------------------------------------------------------------------///

  
  function tournament_validation(){

   $location = base_url()."web/v1/api/tournaments";

   $game_id	=	$this->input->post('game_id');
//    $game_name	=	$this->input->post('game_name');
   $tournament_name	=	$this->input->post('tournament_name');
   $tournament_type_text	=	$this->input->post('tournament_type');
      
   if($tournament_name == null)
   {
     echo '<script>alert(" No Names Entered..!!")</script>'; 
//      $this->tournaments();
     echo '<META HTTP-EQUIV="Refresh" Content="0; URL='.$location.'">';
   }
   else
   {
//      $game_details =  $this->db->select('*')
//                       ->from('tb_games')
//                       ->where('game_name',$game_name)
//                       ->get()->result_array();
     
//      $game_id = $game_details[0]['game_id'];
     
     if($tournament_type_text == 'multi')
     {
       $tournament_type = 7000;
     }
     else
     {
       $tournament_type = 6000;
     }  
     
     $tournament_details = $this->db->select('*')
                      ->from('tb_tournaments')
                      ->where('tournament_name',$tournament_name)
                      ->where('fk_game_id',$game_id)
                      ->get()->result_array();
  //                     ->count_all_results();   

     $tournaments_count = count($tournament_details,0);  


     if( $tournaments_count > 0 )
     {
         echo '<script>alert(" FAILED . . !! \n Tournament Name Under Same Game Already Exists..!!")</script>'; 
         echo '<META HTTP-EQUIV="Refresh" Content="0; URL='.$location.'">';
     }
     else
     {

       $data = array(
          'tournament_name'		=>	$this->input->post('tournament_name'),
          'tournament_type'		=>	$tournament_type,
          'fk_game_id'		=>	$game_id,
          'fk_logo_id'		=>	1
        );

      $this->db->insert('tb_tournaments', $data);

      echo '<script>alert(" Success . . !! \n Tournament Inserted..!!")</script>'; 
      echo '<META HTTP-EQUIV="Refresh" Content="0; URL='.$location.'">';

     }
   }
 }
  
  
     //------------------------------------------------------------------------------------------------------------------///

  function matches(){
   $this->load->view('fetch_matches.php');
 }

  //------------------------------------------------------------------------------------------------------------------///

  
 function fetch_tournaments_dropdown(){
		
   $game_id = $this->input->post('game_id');
   
$tournaments = $this->db->select('*')
			->from('tb_tournaments')
  	  ->where('fk_game_id',$game_id)
			->get()->result_array();
        echo "<option value='"."'>".'Select Tournament'."</option>";  
        foreach($tournaments as $tournament_key => $tournament)
        {
            echo "<option value='". $tournament['tournament_id'] ."'>" .$tournament['tournament_id'] .' - '. $tournament['tournament_name'] ."</option>";  // displaying data in option menu
        }   
   
 } 
  
    //------------------------------------------------------------------------------------------------------------------///

  function fetch_matches(){
    
    $tournament_id = $this->input->post('tournament_id');

    $matches = $this->db->select(' match_id , match_name , date_and_time , match_status ')
			->from('tb_match_list')
  	  ->where('fk_tournament_id',$tournament_id)
			->get()->result_array();

$output = '';

if(count($matches) > 0)
{
  $i=1;
   foreach($matches as $match_key => $match)
 {
     if($match['match_status'] == 0)
    {
        $match['match_status'] = "Upcoming";
    }  
    else if($match['match_status'] == 1)
    {
        $match['match_status'] = "Live";
    }  
    else if($match['match_status'] == 2)
    {
        $match['match_status'] = "In Review";
    }  
    else //if($match['match_status'] > 2)
    {
        $match['match_status'] = "Finished";
    }  
  $output .= '
  <tr>
   <td><input type="text" name="sl_no" id="sl_no" class="form-control" value="'.$i.'" readonly/></td>
   <td><input type="text" name="match_id" id="match_id" class="form-control" value="'.$match['match_id'].'" readonly/></td>
   <td><input type="text" name="match_name" id="match_name" class="form-control" value="'.$match['match_name'].'" readonly/></td>
   <td><input type="text" name="date_and_time" id="date_and_time" class="form-control" value="'.$match['date_and_time'].'" readonly/></td>
   <td><input type="text" name="match_status" id="match_status" class="form-control" value="'.$match['match_status'].'" readonly/></td>
  </tr>
  ';
   $i++;
 }
}
else
{
 $output .= '
 <tr>
  <td colspan="4" align="center">No Data Found</td>
 </tr>
 ';
}

echo $output;
    
  }
  
      //------------------------------------------------------------------------------------------------------------------///
  
  function match_validation(){

   $location = base_url()."web/v1/api/matches";

   $tournament_id	=	$this->input->post('tournament_id');
//    $game_name	=	$this->input->post('game_name');
   $match_name	=	$this->input->post('match_name');
   $match_date_time	=	$this->input->post('match_date_and_time');
   $public_link	=	$this->input->post('public_link');
   $private_link	=	$this->input->post('private_link');
      
   if($match_name == null)
   {
     echo '<script>alert(" No Names Entered..!!")</script>'; 
     echo '<META HTTP-EQUIV="Refresh" Content="0; URL='.$location.'">';
   }
   else if($tournament_id == null)
   {
     echo '<script>alert(" Tournament ID is NULL..!!")</script>'; 
     echo '<META HTTP-EQUIV="Refresh" Content="0; URL='.$location.'">';
   }
   else if(strtotime($match_date_time) <= strtotime("now"))
   {
       echo '<script>alert(" Cannot add Match to a previous time..!!")</script>'; 
       echo '<META HTTP-EQUIV="Refresh" Content="0; URL='.$location.'">';
   }
   else
   {
     
     
     $match_details = $this->db->select('*')
                      ->from('tb_match_list')
                      ->where('match_name',$match_name)
                      ->where('fk_tournament_id',$tournament_id)
                      ->get()->result_array();
  //                     ->count_all_results();   

     $matches_count = count($match_details,0);  


     if( $matches_count > 0 )
     {
         echo '<script>alert(" FAILED . . !! \n Match Name Under Same Tournament Already Exists..!!")</script>'; 
         echo '<META HTTP-EQUIV="Refresh" Content="0; URL='.$location.'">';
     }
     else
     {

       $data = array(
          'match_name'		=>	$match_name,
          'date_and_time'		=>	$match_date_time,
          'fk_tournament_id'		=>	$tournament_id,
          'match_status'		=>	0
        );

      $this->db->insert('tb_match_list', $data);
       
       
       $match_details1 = $this->db->select('*')
                      ->from('tb_match_list')
                      ->where('match_name',$match_name)
                      ->where('fk_tournament_id',$tournament_id)
                      ->get()->result_array();
       
       $match_id = (int)$match_details1[0]["match_id"];
       
       if($public_link != null)
       {
       $data2 = array(
          'fk_match_id'		=>	$match_id,
          'link'		=>	$public_link,
          'link_type'		=>	'public'
        );
        
       $this->db->insert('tb_streams', $data2);
         
       }
       
       if($private_link != null)
       {
       $data3 = array(
          'fk_match_id'		=>	$match_id,
          'link'		=>	$private_link,
          'link_type'		=>	'private'
        );
        
       $this->db->insert('tb_streams', $data3);
         
       }
       
      echo '<script>alert(" Success . . !! \n Match Inserted..!!")</script>'; 
      echo '<META HTTP-EQUIV="Refresh" Content="0; URL='.$location.'">';

     }
   }
 }
  
    
    //------------------------------------------------------------------------------------------------------------------///

  function teams(){
   $this->load->view('fetch_teams.php');
 }

  //------------------------------------------------------------------------------------------------------------------///
  function fetch_teams(){
    
    $match_id = $this->input->post('match_id');

    $teams = $this->db->select(' team_id , team_name ')
			->from('tb_teams_list')
      ->join('tb_teams', 'tb_teams_list.fk_team_id = tb_teams.team_id','left')
  	  ->where('fk_match_id',$match_id)
			->get()->result_array();

$output = '';

if(count($teams) > 0)
{
  $i=1;
   foreach($teams as $team_key => $team)
 {
  $output .= '
  <tr>
   <td><input type="text" name="sl_no" id="sl_no" class="form-control" value="'.$i.'" readonly/></td>
   <td><input type="text" name="team_id" id="team_id" class="form-control" value="'.$team['team_id'].'" readonly/></td>
   <td><input type="text" name="team_name" id="team_name" class="form-control" value="'.$team['team_name'].'" readonly/></td>
  </tr>
  ';
   $i++;
 }
}
else
{
 $output .= '
 <tr>
  <td colspan="4" align="center">No Data Found</td>
 </tr>
 ';
}

echo $output;
    
  }
  //------------------------------------------------------------------------------------------------------------------///


 function fetch_matches_dropdown(){
		
   $tournament_id = $this->input->post('tournament_id');
   
$matches = $this->db->select('*')
			->from('tb_match_list')
  	  ->where('fk_tournament_id',$tournament_id)
			->get()->result_array();
        echo "<option value='"."'>".'Select Match'."</option>";  
        foreach($matches as $match_key => $match)
        {
            echo "<option value='". $match['match_id'] ."'>" .$match['match_id'] .' - '. $match['match_name'] ."</option>";  // displaying data in option menu
        }   
   
 } 
  
   
      //------------------------------------------------------------------------------------------------------------------///
  
  function team_validation(){

   $location = base_url()."web/v1/api/teams";

   $match_id	=	$this->input->post('match_id');
//    $game_name	=	$this->input->post('game_name');
   $team_id	=	$this->input->post('team_id');

    
   if($match_id == null)
   {
     echo '<script>alert(" No Match Selected..!!")</script>'; 
     echo '<META HTTP-EQUIV="Refresh" Content="0; URL='.$location.'">';
   }
   else if($team_id == null)
   {
     echo '<script>alert(" No Team Selected..!!")</script>'; 
     echo '<META HTTP-EQUIV="Refresh" Content="0; URL='.$location.'">';
   }
   else
   {
     
     
     $match_details = $this->db->select('tournament_type')
                      ->from('tb_teams_list')
                      ->join('tb_match_list', 'tb_teams_list.fk_match_id = tb_match_list.match_id','left')
                      ->join('tb_tournaments', 'tb_match_list.fk_tournament_id = tb_tournaments.tournament_id','left')
                      ->where('fk_match_id',$match_id)
                      ->get()->result_array();
  
     $match_count = count($match_details,0);  

     $team_details = $this->db->select('*')
                      ->from('tb_teams_list')
                      ->where('fk_match_id',$match_id)
                      ->where('fk_team_id',$team_id)
                      ->get()->result_array();
  //                     ->count_all_results();   

     $teams_count = count($team_details,0);  


     if( $teams_count > 0 )
     {
         echo '<script>alert(" FAILED . . !! \n Team Already Exists in Same Match..!!")</script>'; 
         echo '<META HTTP-EQUIV="Refresh" Content="0; URL='.$location.'">';
     }
     else if( ($match_details[0]['tournament_type'] == 7000) && ($match_count >= 2) )
     {
         echo '<script>alert(" FAILED . . !! \n Maximum Number of Teams Already Reached..!!")</script>'; 
         echo '<META HTTP-EQUIV="Refresh" Content="0; URL='.$location.'">';
     }
     else if( ($match_details[0]['tournament_type'] == 6000) && ($match_count >= 16) )
     {
         echo '<script>alert(" FAILED . . !! \n Maximum Number of Teams Already Reached..!!")</script>'; 
         echo '<META HTTP-EQUIV="Refresh" Content="0; URL='.$location.'">';
     }
     else
     {

       $data = array(
          'fk_match_id'		=>	$match_id,
          'fk_team_id'		=>	$team_id,
          'team_position'		=>	0
        );

      $this->db->insert('tb_teams_list', $data);

      echo '<script>alert(" Success . . !! \n Team Inserted..!!")</script>'; 
      echo '<META HTTP-EQUIV="Refresh" Content="0; URL='.$location.'">';

     }
   }
 }
  
  //------------------------------------------------------------------------------------------------------------------///
  
   function team_players(){
   $this->load->view('fetch_team_players.php');
 }
  //------------------------------------------------------------------------------------------------------------------///
  
   function final_players(){
   $this->load->view('fetch_final_players.php');
 }
  
   //------------------------------------------------------------------------------------------------------------------///


 function fetch_teams_dropdown(){
		
    $match_id = $this->input->post('match_id');
   
    $teams = $this->db->select('team_id , team_name')
			->from('tb_teams_list')
      ->join('tb_teams', 'tb_teams_list.fk_team_id = tb_teams.team_id','left')
  	  ->where('fk_match_id',$match_id)
			->get()->result_array();
   
    echo "<option value='"."'>".'Select Team'."</option>";  
    foreach($teams as $team_key => $team)
    {
        echo "<option value='". $team['team_id'] ."'>" .$team['team_id'] .' - '. $team['team_name'] ."</option>";  // displaying data in option menu
    }   
   
 } 
  
   //------------------------------------------------------------------------------------------------------------------///

   function fetch_players_dropdown(){
		
    $match_id = $this->input->post('match_id');

    $teams_list = $this->db->select('teams_list_id')
			->from('tb_teams_list')
  	  ->where('fk_match_id',$match_id)
			->get()->result_array();
    

    $output = '';

    if(count($teams_list) > 0)
    {
//       $i=1;       
      echo "<option value='"."'>".'Select Player'."</option>";  
      foreach($teams_list as $team_list_key => $team_list)
      {
        $final_players = $this->db->select(' team_player_id , player_name ')
          ->from('tb_final_team_list')
          ->join('tb_team_players', 'tb_final_team_list.fk_team_player_id = tb_team_players.team_player_id','left')
          ->join('tb_players', 'tb_team_players.fk_player_id = tb_players.player_id','left')
          ->join('tb_teams', 'tb_players.fk_team_id = tb_teams.team_id','left')
          ->where('fk_team_list_id',$team_list['teams_list_id'])
          ->get()->result_array();

         foreach($final_players as $player_key => $player)
         {
          echo "<option value='". $player['team_player_id'] ."'>" .$player['team_player_id'] .' - '. $player['player_name'] ."</option>";  // displaying data in option menu
         }
       }
    } 
     
   else
     {
       echo "<option value='"."'>".'No Data Found'."</option>";  
     }

   
 } 
  


 //------------------------------------------------------------------------------------------------------------------///
  
 function fetch_players_in_teams(){
    
    $team_id = $this->input->post('team_id');
    $tournament_id = $this->input->post('tournament_id');
    
   $players = $this->db->select(' team_player_id , player_name ')
			->from('tb_team_players')
      ->join('tb_players', 'tb_team_players.fk_player_id = tb_players.player_id','left')
  	  ->where('tb_team_players.fk_team_id',$team_id)
  	  ->where('fk_tournament_id',$tournament_id)
			->get()->result_array();

  $output = '';

  if(count($players) > 0)
  {
    $i=1;
     foreach($players as $player_key => $player)
   {
    $output .= '
    <tr>
     <td><input type="text" name="sl_no" id="sl_no" class="form-control" value="'.$i.'" readonly/></td>
     <td><input type="text" name="player_id" id="player_id" class="form-control" value="'.$player['team_player_id'].'" readonly/></td>
     <td><input type="text" name="player_name" id="player_name" class="form-control" value="'.$player['player_name'].'" readonly/></td>
    </tr>
    ';
     $i++;
   }
  }
  else
  {
   $output .= '
   <tr>
    <td colspan="4" align="center">No Data Found</td>
   </tr>
   ';
  }

echo $output;
    
  }
  //------------------------------------------------------------------------------------------------------------------///
  
 function fetch_players_in_teams2(){
    
    $team_id = $this->input->post('team_id');
    
   $players = $this->db->select('player_id , player_name ')
			->from('tb_players')
  	  ->where('fk_team_id',$team_id)
			->get()->result_array();

  $output = '';

  if(count($players) > 0)
  {
    $output .= '
            <tr>
               <th>Select &nbsp;</th>
               <th>&nbsp;&nbsp;Sl. No &nbsp;</th>
               <th>&nbsp;&nbsp;Player Id &nbsp;</th>
               <th>&nbsp;&nbsp;&nbsp;Player Name </th>
             </tr>
    ';
    $i=1;
     foreach($players as $player_key => $player)
   {
    $output .= '
    <tr>
     <td><input type="checkbox" name="sel_players[]" value="'.$player['player_id'].'"/></td>
     <td><input type="text" name="sl_no" id="sl_no" class="form-control" value="'.$i.'" readonly/></td>
     <td><input type="text" name="player_id_'.$i.'" id="player_id" class="form-control" value="'.$player['player_id'].'" readonly/></td>
     <td><input type="text" name="player_name_'.$i.'" id="player_name" class="form-control" value="'.$player['player_name'].'" readonly/></td>
    </tr>
    ';
     $i++;
   }
  }
  else
  {
   $output .= '
   <tr>
    <td colspan="4" align="center">No Data Found</td>
   </tr>
   ';
  }

echo $output;
    
  }
  //------------------------------------------------------------------------------------------------------------------///
  
public function update_selected_team_players()
	{

  $location = base_url()."web/v1/api/team_players";

  $selected_players=array();
   $team_id	=	$this->input->post('team_id');
   $tournament_id	=	$this->input->post('tournament_id');
   $max_count	=	$this->input->post('sl_no');
   $selected_players	=	$this->input->post('sel_players');
   $count_selected_players = count($selected_players,0);

  $player_details = array();   
   $z=0;
   for($x=1 ; $x <= $max_count ; $x++)
   {
     
//      echo $x;
     $player_id	=	$this->input->post('player_id_'.$x);
      
     for($y=0 ; $y < $count_selected_players ; $y++)
     {
       if($player_id == $selected_players[$y])
       {

//          $player_name	=	$this->input->post('player_name_'.$x);

         $player_details[$z]['fk_player_id'] = $player_id;
         $player_details[$z]['fk_team_id'] = $team_id;
         $player_details[$z]['fk_tournament_id'] = $tournament_id;
         $z++;
       }
     }
   }
  
  
  $query_status =  $this->db->insert_batch('tb_team_players', $player_details);

  if($query_status)
  {
   echo '<script>alert(" Success . . !! \n Players Inserted..!!")</script>'; 
   echo '<META HTTP-EQUIV="Refresh" Content="0; URL='.$location.'">';
  }

  else
  {
   echo '<script>alert(" Failed . . !! \n Some ERROR Occured... Players Not Inserted..!!")</script>'; 
   echo '<META HTTP-EQUIV="Refresh" Content="0; URL='.$location.'">';
  }
//   $this->fetch();

//   echo '<script>alert("Success..!!")</script>'; 
//   echo "Success..!!";
//          $response=array('status'=>true, 'message'=>'Success' , 'data'=>$player_details, 'team id'=>$team_id, 'tournament id'=>$tournament_id );
 
//   header('Content-Type: application/json');
//     echo json_encode($response);
  
	}  

  
  //------------------------------------------------------------------------------------------------------------------///
  //------------------------------------------------------------------------------------------------------------------///

  function fetch_final_players(){
    
    $match_id = $this->input->post('match_id');

    $teams_list = $this->db->select('teams_list_id')
			->from('tb_teams_list')
  	  ->where('fk_match_id',$match_id)
			->get()->result_array();
    

    $output = '';

    if(count($teams_list) > 0)
    {
      $i=1;       
    foreach($teams_list as $team_list_key => $team_list)
    {
      $final_players = $this->db->select(' team_name , team_player_id , player_name ')
        ->from('tb_final_team_list')
        ->join('tb_team_players', 'tb_final_team_list.fk_team_player_id = tb_team_players.team_player_id','left')
        ->join('tb_players', 'tb_team_players.fk_player_id = tb_players.player_id','left')
        ->join('tb_teams', 'tb_players.fk_team_id = tb_teams.team_id','left')
        ->where('fk_team_list_id',$team_list['teams_list_id'])
        ->get()->result_array();

       foreach($final_players as $player_key => $player)
       {
        $output .= '
        <tr>
         <td><input type="text" name="sl_no" id="sl_no" class="form-control" value="'.$i.'" readonly/></td>
         <td><input type="text" name="team_name" id="team_name" class="form-control" value="'.$player['team_name'].'" readonly/></td>
         <td><input type="text" name="player_id" id="player_id" class="form-control" value="'.$player['team_player_id'].'" readonly/></td>
         <td><input type="text" name="player_name" id="player_name" class="form-control" value="'.$player['player_name'].'" readonly/></td>
        </tr>
        ';
         $i++;
       }
      }
    }
    else
    {
     $output .= '
     <tr>
      <td colspan="4" align="center">No Data Found</td>
     </tr>
     ';
    }

echo $output;
    
  }
  //------------------------------------------------------------------------------------------------------------------///

   
  //------------------------------------------------------------------------------------------------------------------///
  //------------------------------------------------------------------------------------------------------------------///

  function fetch_final_players_2(){
    
    $team_id = $this->input->post('team_id');
    $tournament_id = $this->input->post('tournament_id');

    $players = array();

    $players = $this->db->select('team_player_id , player_name')
			->from('tb_team_players')
      ->join('tb_players', 'tb_team_players.fk_player_id = tb_players.player_id','left')
  	  ->where('tb_team_players.fk_team_id',$team_id)
  	  ->where('tb_team_players.fk_tournament_id',$tournament_id)
			->get()->result_array();
    
//     echo count($players,0); 
//     echo '\n team id : '.$team_id;
//     echo '\n tournamnet id : '.$tournament_id;

    $output = '';


    if(count($players) > 0)
    {
      
    $output .= '
            <tr>
               <th>Select &nbsp;</th>
               <th>&nbsp;&nbsp;Sl. No &nbsp;</th>
               <th>&nbsp;&nbsp;Player Id &nbsp;</th>
               <th>&nbsp;&nbsp;&nbsp;Player Name </th>
             </tr>
    ';
      $i=1;       

      foreach($players as $player_key => $player)
       {
        $output .= '
        <tr>
         <td><input type="checkbox" name="sel_players[]" value="'.$player['team_player_id'].'"/></td>
         <td><input type="text" name="sl_no" id="sl_no" class="form-control" value="'.$i.'" readonly/></td>
         <td><input type="text" name="player_id_'.$i.'" id="player_id" class="form-control" value="'.$player['team_player_id'].'" readonly/></td>
         <td><input type="text" name="player_name_'.$i.'" id="player_name" class="form-control" value="'.$player['player_name'].'" readonly/></td>
        </tr>
        ';
         $i++;
       }
      
    }
    else
    {
      

     $output .= '
     <tr>
      <td colspan="4" align="center">No Data Found</td>
     </tr>
     ';
    }

  echo $output;
    
  }

   //------------------------------------------------------------------------------------------------------------------///
  //------------------------------------------------------------------------------------------------------------------///

  function fetch_players_ids(){
    
    $match_id = $this->input->post('match_id');

    $teams_list = $this->db->select('teams_list_id')
			->from('tb_teams_list')
  	  ->where('fk_match_id',$match_id)
			->get()->result_array();
    

    $output = '';

    if(count($teams_list) > 0)
    {
    
    foreach($teams_list as $team_list_key => $team_list)
    {
      $final_players = $this->db->select(' team_name , team_player_id , player_name ')
        ->from('tb_final_team_list')
        ->join('tb_team_players', 'tb_final_team_list.fk_team_player_id = tb_team_players.team_player_id','left')
        ->join('tb_players', 'tb_team_players.fk_player_id = tb_players.player_id','left')
        ->join('tb_teams', 'tb_players.fk_team_id = tb_teams.team_id','left')
        ->where('fk_team_list_id',$team_list['teams_list_id'])
        ->get()->result_array();
        
      $i = 1;
      $output .= '<tr height="50%">';
       foreach($final_players as $player_key => $player)
       {
        $output .= '
         <td><input type="text" name="sl_no" id="sl_no" class="form-control" value="'.$i.'" readonly/></td>
         <td><input type="text" name="team_name" id="team_name" class="form-control" value="'.$player['team_name'].'" readonly/></td>
         <td><input type="text" name="player_id" id="player_id" class="form-control" value="'.$player['team_player_id'].'" readonly/></td>
         <td><input type="text" name="player_name" id="player_name" class="form-control" value="'.$player['player_name'].'" readonly/></td>
        ';
         if($i % 2 == 0)
         {
           $output .= '
            </tr>
            <tr>
            ';
         }
        $i++;
       }
      }
    }
    else
    {
     $output .= '
      No Data Found
     ';
    }

echo $output;
    
  }
  //------------------------------------------------------------------------------------------------------------------///

  
  
  //------------------------------------------------------------------------------------------------------------------///
  //------------------------------------------------------------------------------------------------------------------///

public function update_selected_final_players()
	{

  $location = base_url()."web/v1/api/final_players";

  $selected_players=array();
   $team_id	=	$this->input->post('team_id');
   $match_id	=	$this->input->post('match_id');
   $tournament_id	=	$this->input->post('tournament_id');
   $max_count	=	$this->input->post('sl_no');
   $selected_players	=	$this->input->post('sel_players');
   $count_selected_players = count($selected_players,0);

  
  $tournament_details = $this->db->select('tournament_type')
    ->from('tb_tournaments')
    ->where('tournament_id',$tournament_id)
    ->get()->result_array();
  
  $tournament_type = $tournament_details[0]['tournament_type'];
  
  $teams_list_ids = $this->db->select('teams_list_id')
			->from('tb_teams_list')
  	  ->where('fk_team_id',$team_id)
  	  ->where('fk_match_id',$match_id)
			->get()->result_array();
  
  $teams_list_id = $teams_list_ids[0]['teams_list_id'];
  
  $match_stats = array();
  $player_details = array();   
   $z=0;
   for($x=1 ; $x <= $max_count ; $x++)
   {
     
//      echo $x;
     $player_id	=	$this->input->post('player_id_'.$x);
      
     for($y=0 ; $y < $count_selected_players ; $y++)
     {
       if($player_id == $selected_players[$y])
       {

//          $player_name	=	$this->input->post('player_name_'.$x);
         $match_stats[$z]['fk_team_player_id'] = $player_id;
         $match_stats[$z]['fk_match_id'] = $match_id;

         $player_details[$z]['fk_team_player_id'] = $player_id;
         $player_details[$z]['fk_team_list_id'] = $teams_list_id;
         $z++;
       }
     }
   }
  
  
  $query_status =  $this->db->insert_batch('tb_final_team_list', $player_details);

  if( $tournament_type == 6000 )
  {
    $this->db->insert_batch('tb_br_match_details', $match_stats);
  }
  else
  {
    $this->db->insert_batch('tb_mp_match_details', $match_stats);
  }
  
  if($query_status)
  {
   echo '<script>alert(" Success . . !! \n Players Inserted..!!")</script>'; 
   echo '<META HTTP-EQUIV="Refresh" Content="0; URL='.$location.'">';
  }

  else
  {
   echo '<script>alert(" Failed . . !! \n Some ERROR Occured... Players Not Inserted..!!")</script>'; 
   echo '<META HTTP-EQUIV="Refresh" Content="0; URL='.$location.'">';
  }
//   $this->fetch();

//   echo '<script>alert("Success..!!")</script>'; 
//   echo "Success..!!";
//          $response=array('status'=>true, 'message'=>'Success' , 'data'=>$player_details, 'team id'=>$team_id, 'tournament id'=>$tournament_id );
 
//   header('Content-Type: application/json');
//     echo json_encode($response);
  
	}  

     //------------------------------------------------------------------------------------------------------------------///

  function pools(){
   $this->load->view('fetch_pools.php');
 }

  //------------------------------------------------------------------------------------------------------------------///
  function fetch_pools(){
    
    $match_id = $this->input->post('match_id');

    $pools = $this->db->select(' pool_id , pool_type_id , pool_type_name , total_amount , total_slots , reg_fee ')
			->from('tb_prize_pool_list')
      ->join('tb_pool_types', 'tb_prize_pool_list.fk_pool_type_id = tb_pool_types.pool_type_id','left')
  	  ->where('fk_match_id',$match_id)
			->get()->result_array();

$output = '';

if(count($pools) > 0)
{
  $i=1;
   foreach($pools as $pool_key => $pool)
 {
  $output .= '
  <tr>
   <td><input type="text" name="sl_no" id="sl_no" class="form-control" value="'.$i.'" readonly/></td>
   <td><input type="text" name="pool_id" id="pool_id" class="form-control" value="'.$pool['pool_id'].'" readonly/></td>
   <td><input type="text" name="pool_type_id" id="pool_type_id" class="form-control" value="'.$pool['pool_type_id'].'" readonly/></td>
   <td><input type="text" name="pool_type_name" id="pool_type_name" class="form-control" value="'.$pool['pool_type_name'].'" readonly/></td>
   <td><input type="text" name="total_amount" id="total_amount" class="form-control" value="'.$pool['total_amount'].'" readonly/></td>
   <td><input type="text" name="total_slots" id="total_slots" class="form-control" value="'.$pool['total_slots'].'" readonly/></td>
   <td><input type="text" name="reg_fee" id="reg_fee" class="form-control" value="'.$pool['reg_fee'].'" readonly/></td>
  </tr>
  ';
   $i++;
 }
}
else
{
 $output .= '
 <tr>
  <td colspan="4" align="center">No Data Found</td>
 </tr>
 ';
}

echo $output;
    
  }
  
       //------------------------------------------------------------------------------------------------------------------///
  
  function pool_validation(){

   $location = base_url()."web/v1/api/pools";

   $match_id	=	$this->input->post('match_id');
//    $game_name	=	$this->input->post('game_name');
   $pool_type_id	=	$this->input->post('pool_type_id');

    
   if($match_id == null)
   {
     echo '<script>alert(" No Match Selected..!!")</script>'; 
     echo '<META HTTP-EQUIV="Refresh" Content="0; URL='.$location.'">';
   }
   else if($pool_type_id == null)
   {
     echo '<script>alert(" No Pools Selected..!!")</script>'; 
     echo '<META HTTP-EQUIV="Refresh" Content="0; URL='.$location.'">';
   }
   else
   {
     
     
     

       $data = array(
          'fk_match_id'		=>	$match_id,
          'fk_pool_type_id'		=>	$pool_type_id
        );

      $this->db->insert('tb_prize_pool_list', $data);

      echo '<script>alert(" Success . . !! \n Pool Inserted..!!")</script>'; 
      echo '<META HTTP-EQUIV="Refresh" Content="0; URL='.$location.'">';
   }
  }

 //------------------------------------------------------------------------------------------------------------------///

public function pool_validation2()
	{

  $location = base_url()."web/v1/api/pools";

   $selected_pools=array();
   $match_id	=	$this->input->post('match_id');
   $max_count	=	$this->input->post('sl_no');
   $selected_pools	=	$this->input->post('sel_pools');
   $count_selected_pools = count($selected_pools,0);

  $pools_selected = array();
   $z=0;
   for($x=1 ; $x <= $max_count ; $x++)
   {
     
//      echo $x;
     $pool_type_id	=	$this->input->post('pool_type_id_'.$x);
      
     for($y=0 ; $y < $count_selected_pools ; $y++)
     {
       if($pool_type_id == $selected_pools[$y])
       {

//          $player_name	=	$this->input->post('player_name_'.$x);
         $pools_selected[$z]['fk_match_id'] = $match_id;
         $pools_selected[$z]['fk_pool_type_id'] = $pool_type_id;

         $z++;
       }
     }
   }
  
  
  $query_status =  $this->db->insert_batch('tb_prize_pool_list', $pools_selected);

  
  if($query_status)
  {
   echo '<script>alert(" Success . . !! \n Pools Inserted..!!")</script>'; 
   echo '<META HTTP-EQUIV="Refresh" Content="0; URL='.$location.'">';
  }

  else
  {
   echo '<script>alert(" Failed . . !! \n Some ERROR Occured... Pools Not Inserted..!!")</script>'; 
   echo '<META HTTP-EQUIV="Refresh" Content="0; URL='.$location.'">';
  }
//   $this->fetch();

//   echo '<script>alert("Success..!!")</script>'; 
//   echo "Success..!!";
//          $response=array('status'=>true, 'message'=>'Success' , 'data'=>$player_details, 'team id'=>$team_id, 'tournament id'=>$tournament_id );
 
//   header('Content-Type: application/json');
//     echo json_encode($response);
  
	}  

     //------------------------------------------------------------------------------------------------------------------///
 //------------------------------------------------------------------------------------------------------------------///

  function fetch_all_pools(){
    
    
    $pools = array();

    $pools = $this->db->select('pool_type_id , pool_type_name')
			->from('tb_pool_types')
			->get()->result_array();
    
//     echo count($players,0); 
//     echo '\n team id : '.$team_id;
//     echo '\n tournamnet id : '.$tournament_id;

    $output = '';


    if(count($pools) > 0)
    {
      
    $output .= '
            <tr>
               <th>Select &nbsp;</th>
               <th>&nbsp;&nbsp;Sl. No &nbsp;</th>
               <th>&nbsp;&nbsp;Pool Id &nbsp;</th>
               <th>&nbsp;&nbsp;&nbsp;Pool Name </th>
             </tr>
    ';
      $i=1;       

      foreach($pools as $pool_key => $pool)
       {
        $output .= '
        <tr>
         <td><input type="checkbox" name="sel_pools[]" value="'.$pool['pool_type_id'].'"/></td>
         <td><input type="text" name="sl_no" id="sl_no" class="form-control" value="'.$i.'" readonly/></td>
         <td><input type="text" name="pool_type_id_'.$i.'" id="pool_type_id" class="form-control" value="'.$pool['pool_type_id'].'" readonly/></td>
         <td><input type="text" name="pool_type_name_'.$i.'" id="pool_type_name" class="form-control" value="'.$pool['pool_type_name'].'" readonly/></td>
        </tr>
        ';
         $i++;
       }
      
    }
    else
    {
      

     $output .= '
     <tr>
      <td colspan="4" align="center">No Data Found</td>
     </tr>
     ';
    }

  echo $output;
    
  }
  
  //------------------------------------------------------------------------------------------------------------------///
  //------------------------------------------------------------------------------------------------------------------///

   //------------------------------------------------------------------------------------------------------------------///

  function match_update(){
   $this->load->view('update_match_stats.php');
 }

     //------------------------------------------------------------------------------------------------------------------///
 
   function fetch_header(){
  
    $tournament_id = $this->input->post('tournament_id');
   
//     $matchDetail = $this->db->select('tournament_type')
//         ->from('tb_match_list')
//         ->join('tb_tournaments','tb_match_list.fk_tournament_id = tb_tournaments.tournament_id','left')
//         ->where('match_id',$match_id)
//         ->get()->result_array();

    $matchDetail = $this->db->select('tournament_type')
        ->from('tb_tournaments')
        ->where('tournament_id',$tournament_id)
        ->get()->result_array();
    
    $tournament_type=$matchDetail[0]['tournament_type'];

    $output = '';

    if( $tournament_type == 6000 )//Battle Royale
    {
   $output .= '
     <tr>
      <td colspan="7" align="center"><input type="hidden" name="match_type" value="Battle Royale" readonly/></td>
     </tr>
     <tr>
       <th>Sl. No</th>
       <th>Player Id</th>
       <th>Player Name</th>
       <th>Kills</th>
       <th>New Kills</th>
       <th>Knocks</th>
       <th>New Knocks</th>
     </tr>
     ';
    }
    else//Multi player
    {
       $output .= '
     <tr>
      <td colspan="9" align="center"><input type="hidden" name="match_type" value="Multi Player" readonly/></td>
     </tr>
     <tr>
       <th>Sl. No</th>
       <th>Player Id</th>
       <th>Player Name</th>
       <th>Kills</th>
       <th>New Kills</th>
       <th>Assits</th>
       <th>New Assits</th>
       <th>Deaths</th>
       <th>New Deaths</th>
     </tr>
     ';
    }

    echo $output;

  }

  
  //------------------------------------------------------------------------------------------------------------------///

   function fetch_header_with_matchid(){
  
    $match_id = $this->input->post('match_id');
   

   $output = '
     <tr>
      <td colspan="7" align="center"><input type="hidden" name="match_id" value="'.$match_id.'" readonly/></td>
     </tr>
           <tr>
               <th width="8%">sl_no</th>
               <th width="36%">Player 1 Id</th>
               <th width="20%">Action</th>
               <th width="36%">Player 2 Id</th>
           </tr>
     ';
    
    echo $output;

  }
      //------------------------------------------------------------------------------------------------------------------///
 
  function fetch_match_current_stats(){
  
   
   $match_id = $this->input->post('match_id');
   
    $matchDetail = $this->db->select('tournament_type')
        ->from('tb_match_list')
        ->join('tb_tournaments','tb_match_list.fk_tournament_id = tb_tournaments.tournament_id','left')
        ->where('match_id',$match_id)
        ->get()->result_array();
    
    $tournament_type=$matchDetail[0]['tournament_type'];

    if( $tournament_type == 6000 )
    {
      $players = $this->db->select(' br_match_details_id , player_name , kills , knocks ')
          ->from('tb_br_match_details')
          ->join('tb_team_players','tb_br_match_details.fk_team_player_id = tb_team_players.team_player_id','left')
          ->join('tb_players','tb_team_players.fk_player_id = tb_players.player_id','left')
          ->where('fk_match_id',$match_id)
    //   	  ->where('fk_tournament_id',$tournament_id)
          ->get()->result_array();
    $output = '';

    if(count($players) > 0)
    {
      $i=1;
     foreach($players as $player_key => $player)
     {
      $output .= '
      <tr>
       <td><input type="text" name="sl_no" id="sl_no" class="form-control" value="'.$i.'" readonly/></td>
       <td><input type="text" name="team_player_id_'.$i.'" id="team_player_id" class="form-control" value="'.$player['br_match_details_id'].'" readonly/></td>
       <td><input type="text" name="player_name_'.$i.'" id="player_name" class="form-control" value="'.$player['player_name'].'" readonly/></td>
       <td><input type="text" name="kills_'.$i.'" id="kills" class="form-control" value="'.$player['kills'].'" readonly/></td>
       <td><input type="text" name="new_kills_'.$i.'" id="new_kills" class="form-control" value="0"/></td>
       <td><input type="text" name="knocks_'.$i.'" id="knocks" class="form-control" value="'.$player['knocks'].'" readonly/></td>
       <td><input type="text" name="new_knocks_'.$i.'" id="new_knocks" class="form-control" value="0"/></td>
      </tr>
      ';
       $i++;
       }
    }
    else
    {
     $output .= '
     <tr>
      <td colspan="4" align="center">No Data Found</td>
     </tr>
     ';
    }
    }
    else
    {
      
       $players = $this->db->select(' mp_match_details_id , player_name , kills , assists , deaths ')
          ->from('tb_mp_match_details')
          ->join('tb_team_players','tb_mp_match_details.fk_team_player_id = tb_team_players.team_player_id','left')
          ->join('tb_players','tb_team_players.fk_player_id = tb_players.player_id','left')
          ->where('fk_match_id',$match_id)
    //   	  ->where('fk_tournament_id',$tournament_id)
          ->get()->result_array();
    $output = '';

    if(count($players) > 0)
    {
      $i=1;
     foreach($players as $player_key => $player)
     {
      $output .= '
      <tr>
       <td><input type="text" name="sl_no" id="sl_no" class="form-control" value="'.$i.'" readonly/></td>
       <td><input type="text" name="team_player_id_'.$i.'" id="team_player_id" class="form-control" value="'.$player['mp_match_details_id'].'" readonly/></td>
       <td><input type="text" name="player_name_'.$i.'" id="player_name" class="form-control" value="'.$player['player_name'].'" readonly/></td>
       <td><input type="text" name="kills_'.$i.'" id="kills" class="form-control" value="'.$player['kills'].'" readonly/></td>
       <td><input type="text" name="new_kills_'.$i.'" id="new_kills" class="form-control" value="0"/></td>
       <td><input type="text" name="assists_'.$i.'" id="assists" class="form-control" value="'.$player['assists'].'" readonly/></td>
       <td><input type="text" name="new_assists_'.$i.'" id="new_assists" class="form-control" value="0"/></td>
       <td><input type="text" name="deaths_'.$i.'" id="deaths" class="form-control" value="'.$player['deaths'].'" readonly/></td>
       <td><input type="text" name="new_deaths_'.$i.'" id="new_deaths" class="form-control" value="0"/></td>
      </tr>
      ';
       $i++;
       }
    }
    else
    {
     $output .= '
     <tr>
      <td colspan="4" align="center">No Data Found</td>
     </tr>
     ';
    }
  }
echo $output;
}

  
  //------------------------------------------------------------------------------------------------------------------///
  
public function update_kills()
	{
  
  $location = base_url()."web/v1/api/match_update";
//      $location = base_url()."api_old/fetch";

  $match_type = $this->input->post('match_type');
   $max_count	=	$this->input->post('sl_no');
   
  if($match_type == "Multi Player")
  {
//     echo "Multiplayer";
  $match_details = array();   
   
   for($x=1 ; $x <= $max_count ; $x++)
   {
     
//      echo $x;
     $mp_match_details_id	=	$this->input->post('team_player_id_'.$x);

     $kills	=	$this->input->post('kills_'.$x);
	   $new_kills	=	$this->input->post('new_kills_'.$x);
     $updated_kills = $kills + $new_kills;
     
     $assists	=	$this->input->post('assists_'.$x);
	   $new_assists	=	$this->input->post('new_assists_'.$x);
     $updated_assists = $assists + $new_assists;
     
     $deaths	=	$this->input->post('deaths_'.$x);
	   $new_deaths	=	$this->input->post('new_deaths_'.$x);
     $updated_deaths = $deaths + $new_deaths;

     $match_details[$x-1]['mp_match_details_id'] = $mp_match_details_id;
     $match_details[$x-1]['kills'] = $updated_kills;
     $match_details[$x-1]['assists'] = $updated_assists;
     $match_details[$x-1]['deaths'] = $updated_deaths;

   }
   
   $this->db->update_batch('tb_mp_match_details', $match_details, 'mp_match_details_id');

  }
  else
  {
//     echo "Battle Royale";
    $match_details = array();   
   
   for($x=1 ; $x <= $max_count ; $x++)
   {
     
//      echo $x;
     $br_match_details_id	=	$this->input->post('team_player_id_'.$x);

     $kills	=	$this->input->post('kills_'.$x);
	   $new_kills	=	$this->input->post('new_kills_'.$x);
     $updated_kills = $kills + $new_kills;
     
     $knocks	=	$this->input->post('knocks_'.$x);
	   $new_knocks	=	$this->input->post('new_knocks_'.$x);
     $updated_knocks = $knocks + $new_knocks;
     
//      $deaths	=	$this->input->post('deaths_'.$x);
// 	   $new_deaths	=	$this->input->post('new_deaths_'.$x);
//      $updated_deaths = $deaths + $new_deaths;

     $match_details[$x-1]['br_match_details_id'] = $br_match_details_id;
     $match_details[$x-1]['kills'] = $updated_kills;
     $match_details[$x-1]['knocks'] = $updated_knocks;
//      $match_details[$x-1]['deaths'] = $updated_deaths;

   }
   
   $this->db->update_batch('tb_br_match_details', $match_details, 'br_match_details_id');
}  

        echo '<script>alert(" Success . . !! \n Match Stats Updated..!!")</script>'; 
      echo '<META HTTP-EQUIV="Refresh" Content="0; URL='.$location.'">';

	}  
  
    //------------------------------------------------------------------------------------------------------------------///


  function kill_feed_update(){
   $this->load->view('update_kill_feed2.php');
 }

  
    //------------------------------------------------------------------------------------------------------------------///
  
public function update_kill_feeds()
	{
  
  $location = base_url()."web/v1/api/kill_feed_update";

//   print_r($_POST);
  
   $match_id = $this->input->post('match_id');
   $max_count	=	$this->input->post('sl_no');
   
   $match_details = array();   
   
   for($x=1 ; $x <= $max_count ; $x++)
   {
     $player_id_1	=	$this->input->post('Player1_'.$x); //'player-dropdown1'.$y
	   $player_id_2	=	$this->input->post('Player2_'.$x); //.$z
     $action = $this->input->post('Action_'.$x);

//      $z=$x*2;
//      $y=$z-1;
     
//      $player_id_1	=	$this->input->post('player-dropdown'.$y); //'player-dropdown1'.$y
// 	   $player_id_2	=	$this->input->post('player-dropdown'.$z); //.$z
//      $action = $this->input->post('action-dropdown'.$x);
     
     if ( ( $player_id_1 != 0 ) && ( $player_id_2 !=0 ) )
     {
       $match_details[$x-1]['fk_match_id'] = $match_id;
       $match_details[$x-1]['fk_team_player_id_1'] = $player_id_1;
       $match_details[$x-1]['fk_team_player_id_2'] = $player_id_2;
//        $match_details[$x-1]['action'] = $action;

       //  ---------- KILL = 0 ----------
       //  ---------- KNOCK = 1 ----------
       //  ---------- ASSIST = 2 ----------
       
       if( $action == 0 )
       {
          $match_details[$x-1]['action'] = "kill";
       }
       elseif( $action == 1 )
       {
          $match_details[$x-1]['action'] = "knock";
       }
       elseif( $action == 2 )
       {
         $match_details[$x-1]['action'] = "assist";
       }
         
      }

  }
    
  
//   print_r($match_details);
   $query_status =  $this->db->insert_batch('tb_kill_feeds', $match_details);

  if($query_status)
  {
   echo '<script>alert(" Success . . !! \n Kill Feeds Inserted..!!")</script>'; 
   echo '<META HTTP-EQUIV="Refresh" Content="0; URL='.$location.'">';
  }

  else
  {
   echo '<script>alert(" Failed . . !! \n Some ERROR Occured... Kill Feeds Not Inserted..!!")</script>'; 
   echo '<META HTTP-EQUIV="Refresh" Content="0; URL='.$location.'">';
  }

	}  
  
      //------------------------------------------------------------------------------------------------------------------///
  
public function update_kill_feeds2()
	{
  
  $location = base_url()."web/v1/api/kill_feed_update";

//   print_r($_POST);
  
   $match_id = $this->input->post('match_id');
   $max_count	=	$this->input->post('sl_no');
   
   $match_details = array();   
   
   for($x=1 ; $x <= $max_count ; $x++)
   {
//      $player_id_1	=	$this->input->post('Player1_'.$x); //'player-dropdown1'.$y
// 	   $player_id_2	=	$this->input->post('Player2_'.$x); //.$z
//      $action = $this->input->post('Action_'.$x);

     $z=$x*2;
     $y=$z-1;
     
     $player_id_1	=	$this->input->post('player-dropdown'.$y); //'player-dropdown1'.$y
	   $player_id_2	=	$this->input->post('player-dropdown'.$z); //.$z
     $action = $this->input->post('action-dropdown'.$x);
     
     if ( ( $player_id_1 != 0 ) && ( $player_id_2 !=0 ) )
     {
       $match_details[$x-1]['fk_match_id'] = $match_id;
       $match_details[$x-1]['fk_team_player_id_1'] = $player_id_1;
       $match_details[$x-1]['fk_team_player_id_2'] = $player_id_2;
       $match_details[$x-1]['action'] = $action;

       //  ---------- KILL = 0 ----------
       //  ---------- KNOCK = 1 ----------
       //  ---------- ASSIST = 2 ----------
       
//        if( $action == 0 )
//        {
//           $match_details[$x-1]['action'] = "kill";
//        }
//        elseif( $action == 1 )
//        {
//           $match_details[$x-1]['action'] = "knock";
//        }
//        elseif( $action == 2 )
//        {
//          $match_details[$x-1]['action'] = "assist";
//        }
         
      }

  }
    
  
//   print_r($match_details);
   $query_status =  $this->db->insert_batch('tb_kill_feeds', $match_details);

  if($query_status)
  {
   echo '<script>alert(" Success . . !! \n Kill Feeds Inserted..!!")</script>'; 
   echo '<META HTTP-EQUIV="Refresh" Content="0; URL='.$location.'">';
  }

  else
  {
   echo '<script>alert(" Failed . . !! \n Some ERROR Occured... Kill Feeds Not Inserted..!!")</script>'; 
   echo '<META HTTP-EQUIV="Refresh" Content="0; URL='.$location.'">';
  }

	}  
  
  
  
    //------------------------------------------------------------------------------------------------------------------///


  function kill_feed_list(){
   $this->load->view('kill_feed_lists.php');
 }

     //------------------------------------------------------------------------------------------------------------------///

  function fetch_kill_feeds(){
    
    $match_id = $this->input->post('match_id');

    $kills = $this->db->select(' fk_team_player_id_1  , fk_team_player_id_2  , action ')
			->from('tb_kill_feeds')
  	  ->where('fk_match_id',$match_id)
			->get()->result_array();

$output = '';

if(count($kills) > 0)
{
  $i=1;
   foreach($kills as $kill_key => $kill)
 {
    $player_1 = $this->db->select('player_name')
			->from('tb_team_players')
      ->join('tb_players','tb_team_players.fk_player_id = tb_players.player_id','left')
  	  ->where('team_player_id',$kill['fk_team_player_id_1'])
			->get()->result_array();
     
     $player_1_name = $player_1[0]['player_name'];
     
      $player_2 = $this->db->select('player_name')
			->from('tb_team_players')
      ->join('tb_players','tb_team_players.fk_player_id = tb_players.player_id','left')
  	  ->where('team_player_id',$kill['fk_team_player_id_2'])
			->get()->result_array();
     
     $player_2_name = $player_2[0]['player_name'];
      
  $output .= '
  <tr>
   <td><input type="text" name="sl_no" id="sl_no" class="form-control" value="'.$i.'" readonly/></td>
   <td><input type="text" name="player_1_name" id="player_1_name" class="form-control" value="'.$player_1_name.'" readonly/></td>
   <td><input type="text" name="action" id="action" class="form-control" value="'.$kill['action'].'ed" readonly/></td>
   <td><input type="text" name="player_2_name" id="player_2_name" class="form-control" value="'.$player_2_name.'" readonly/></td>
  </tr>
  ';
   $i++;
 }
}
else
{
 $output .= '
 <tr>
  <td colspan="4" align="center">No Data Found</td>
 </tr>
 ';
}

echo $output;
    
  }
   
  

       //------------------------------------------------------------------------------------------------------------------///

  function match_status(){
   $this->load->view('change_match_details.php');
 }

   
    //------------------------------------------------------------------------------------------------------------------///

  function fetch_match_status(){
    
    $match_id = $this->input->post('match_id');

    $public_link = 'No Link';
    $private_link = 'No Link';
    
    $matches = $this->db->select(' match_id , match_name , date_and_time , match_status ')
			->from('tb_match_list')
  	  ->where('match_id',$match_id)
			->get()->result_array();
    
    $links = $this->db->select(' link , link_type ')
			->from('tb_streams')
  	  ->where('fk_match_id',$match_id)
			->get()->result_array();
    
    if(count($links) > 0)
    {
      foreach($links as $link_key => $link)
      {
        if(($link['link']!=null)&&($link['link_type']=='public'))
        {
          $public_link = $link['link'];
        }
        if(($link['link']!=null)&&($link['link_type']=='private'))
        {
          $private_link = $link['link'];
        }
      }
    }

$output = '';

if(count($matches) > 0)
{
//   $i=1;
   foreach($matches as $match_key => $match)
 {
//    <td><input type="text" name="sl_no" id="sl_no" class="form-control" value="'.$i.'" readonly/></td>
    
  $output .= '
  <tr>
   <td><input type="text" name="match_id" id="match_id" class="form-control" value="'.$match['match_id'].'" readonly/></td>
   <td><input type="text" name="match_name" id="match_name" class="form-control" value="'.$match['match_name'].'" readonly/></td>
   <td><input type="text" name="date_and_time" id="date_and_time" class="form-control" value="'.$match['date_and_time'].'" /></td>
   <td><input type="text" name="match_status" id="match_status" class="form-control" value="'.$match['match_status'].'" /></td>
   <td><input type="text" name="public_link" id="public_link" class="form-control" value="'.$public_link.'" /></td>
   <td><input type="text" name="private_link" id="private_link" class="form-control" value="'.$private_link.'" /></td>
  </tr>
  ';
//    $i++;
 }
}
else
{
 $output .= '
 <tr>
  <td colspan="4" align="center">No Data Found</td>
 </tr>
 ';
}

echo $output;
    
  }
  
    //------------------------------------------------------------------------------------------------------------------///
  
public function match_status_update()
	{
  
  $location = base_url()."web/v1/api/match_status";
//      $location = base_url()."api_old/fetch";

//   $match_type = $this->input->post('match_type');
//    $max_count	=	$this->input->post('sl_no');
   

//   $match_details = array();   
   
//    for($x=1 ; $x <= $max_count ; $x++)
//    {
     
//      echo $x;
     

     $match_id = $this->input->post('match_id');
     $match_status = $this->input->post('match_status');
     $date_and_time = $this->input->post('date_and_time');
     $public_link = $this->input->post('public_link');
     $private_link = $this->input->post('private_link');
     $active_status = array(
            'date_and_time' => $date_and_time,
            'match_status' => $match_status
          );

         $this->db->where('match_id', $match_id);
         $this->db->update('tb_match_list', $active_status); 
  
      if(($public_link != 'No Link') || ($public_link != null ))
         {
          $stream_status1 = array(
                'link' => $public_link,
                );

           $this->db->where('fk_match_id', $match_id);
           $this->db->where('link_type', 'public');
           $this->db->update('tb_streams', $stream_status1); 
         }
      
      if(($private_link != 'No Link') || ($private_link != null ))
         {
          $stream_status2 = array(
                'link' => $private_link,
                );

           $this->db->where('fk_match_id', $match_id);
           $this->db->where('link_type', 'private');
           $this->db->update('tb_streams', $stream_status2); 
         }  
  
//    }
   
//    $this->db->update_batch('tb_mp_match_details', $match_details, 'mp_match_details_id');

  
 

        echo '<script>alert(" Success . . !! \n Match Stats Updated..!!")</script>'; 
      echo '<META HTTP-EQUIV="Refresh" Content="0; URL='.$location.'">';

	}  
  
    //------------------------------------------------------------------------------------------------------------------///
  //----------------------------------------------------------------------------------/// 

 function players(){
   $this->load->view('create_player.php');
 }
  
     //----------------------------------------------------------------------------------/// 
    //------------------------------------------------------------------------------------------------------------------///

  
  function fetch_players(){
    $players=$this->db->select('player_id , player_name , team_name')
              ->from('tb_players')
              ->join('tb_teams', 'tb_players.fk_team_id = tb_teams.team_id','left')
              ->get()->result_array();
   			 
  $output = '';

if(count($players) > 0)
{
  $i=1;
 foreach($players as $player_key => $player)
 {
  $output .= '
  <tr>
     <td><input type="text" name="sl_no" id="sl_no" class="form-control" value="'.$i.'" readonly/></td>
     <td><input type="text" name="player_id" id="player_id" class="form-control" value="'.$player['player_id'].'" readonly/></td>
     <td><input type="text" name="player_name" id="player_name" class="form-control" value="'.$player['player_name'].'" readonly/></td>
     <td><input type="text" name="team_name" id="team_name" class="form-control" value="'.$player['team_name'].'" readonly/></td>
  </tr>
';
   $i++;
 }
}
else
{
 $output .= '
 <tr>
  <td colspan="4" align="center">No Data Found</td>
 </tr>
 ';
}

echo $output;
}
  
    //------------------------------------------------------------------------------------------------------------------///
    //------------------------------------------------------------------------------------------------------------------///
  
  function player_validation(){

   $location = base_url()."web/v1/api/players";

   $player_name	=	$this->input->post('player_name');
//    $game_name	=	$this->input->post('game_name');
   $player_mp_role	=	$this->input->post('player_mp_role');
   $player_br_role	=	$this->input->post('player_br_role');

   $team_id	=	$this->input->post('team_name');

   if($player_name == null)
   {
     echo '<script>alert(" No Player Name..!!")</script>'; 
     echo '<META HTTP-EQUIV="Refresh" Content="0; URL='.$location.'">';
   }
   else
   {
       $data = array(
          'player_name'		=>	$player_name,
          'mp_role'		=>	$player_mp_role,
          'br_role'		=>	$player_br_role,
          'fk_team_id'		=>	$team_id

        );

      $this->db->insert('tb_players', $data);
// print_r($data);
      echo '<script>alert(" Success . . !! \n Player Inserted..!!")</script>'; 
      echo '<META HTTP-EQUIV="Refresh" Content="0; URL='.$location.'">';
   }
  }

 //------------------------------------------------------------------------------------------------------------------///
 //----------------------------------------------------------------------------------/// 

 function create_team(){
   $this->load->view('create_team.php');
 }
  
   //----------------------------------------------------------------------------------/// 
 
    //------------------------------------------------------------------------------------------------------------------///
  function fetch_teams_list(){
    
    $teams = $this->db->select(' team_id , team_name ')
			->from('tb_teams')
			->get()->result_array();

$output = '';

if(count($teams) > 0)
{
  $i=1;
   foreach($teams as $team_key => $team)
 {
  $output .= '
  <tr>
   <td><input type="text" name="sl_no" id="sl_no" class="form-control" value="'.$i.'" readonly/></td>
   <td><input type="text" name="team_id" id="team_id" class="form-control" value="'.$team['team_id'].'" readonly/></td>
   <td><input type="text" name="team_name" id="team_name" class="form-control" value="'.$team['team_name'].'" readonly/></td>
  </tr>
  ';
   $i++;
 }
}
else
{
 $output .= '
 <tr>
  <td colspan="4" align="center">No Data Found</td>
 </tr>
 ';
}

echo $output;
    
  }
  //------------------------------------------------------------------------------------------------------------------///
 //----------------------------------------------------------------------------------/// 

 function update_team_logo(){
   $this->load->view('update_team_logo.php');
 }
  
 //----------------------------------------------------------------------------------/// 

 function update_player_logo(){
   $this->load->view('update_player_logo.php');
 }
   //----------------------------------------------------------------------------------/// 


    //------------------------------------------------------------------------------------------------------------------///
  function fetch_current_team_logo(){
    
   $team_id	=	$this->input->post('team_id');
    
   $media_list = array();
    
   $media_list=$this->db->select('*')
       ->from('tb_medias')
       ->join('tb_teams', 'tb_medias.media_id = tb_teams.fk_logo_id','left')
       ->where('team_id',$team_id)
       ->get()->result_array();
  
      $media_url = base_url().$media_list[0]['media_url'].'/'.$media_list[0]['media_name'].'_300x300'.$media_list[0]['media_ext'];

// print_r ($media_list);
echo $media_url;
    
  }
  
  
      //------------------------------------------------------------------------------------------------------------------///
  function fetch_current_player_logo(){
    
   $player_id	=	$this->input->post('player_id');
    
   $media_list = array();
    
   $media_list=$this->db->select('*')
       ->from('tb_medias')
       ->join('tb_players', 'tb_medias.media_id = tb_players.fk_logo_id','left')
       ->where('player_id',$player_id)
       ->get()->result_array();
  
      $media_url = base_url().$media_list[0]['media_url'].'/'.$media_list[0]['media_name'].'_300x300'.$media_list[0]['media_ext'];

// print_r ($media_list);
echo $media_url;
    
  }
  
  
  //------------------------------------------------------------------------------------------------------------------///  
//---------------------------------------------------------------------------------------------------------------------------------------------------///
 function team_logo_upload()
   {
//    $file = $_FILES["image"];
//    $fileName = basename($_FILES["image"]["name"]);
//    print_r($file);
   $team_id	=	$this->input->post('team_id');
   $this->upload_team_logo_item("image",$team_id);
 }  
  
  //------------------------------------------------------------------------------------------------------------------///
  //------------------------------------------------------------------------------------------------------------------///
 function player_logo_upload()
   {
//    $file = $_FILES["image"];
//    $fileName = basename($_FILES["image"]["name"]);
//    print_r($file);
   $player_id	=	$this->input->post('player_id');
   $this->upload_player_logo_item("image",$player_id);
 }  
  
  //------------------------------------------------------------------------------------------------------------------///
  //------------------------------------------------------------------------------------------------------------------///

  function upload_team_logo_item($image_id,$team_id)
  {
    $this->load->library('image_lib');
    $this->load->helper('url');
    $current_year='./uploads/item-gallery/'.date('Y');
    $this_month='./uploads/item-gallery/'.date('Y').'/'.date('m');

      if(!is_dir($current_year)) //create the folder if it's not already exists
    {
          mkdir($current_year,0777,TRUE);
    }
      if(!is_dir($this_month)) //create the folder if it's not already exists
    {
          mkdir($this_month,0777,TRUE);
    }
    $config['upload_path'] = './uploads/item-gallery/'.date('Y').'/'.date('m');
    $config['allowed_types'] = 'gif|jpg|png|jpeg|JPEG';
    //$config['overwrite'] = 'TRUE';
    $config['encrypt_name'] = TRUE;
    $this->load->library('upload', $config);
    $this->upload->do_upload($image_id);
    //PHP
    //Configure upload.

    $store_image_link=$this->upload->data();

    $config['image_library'] = 'gd2';
    $config['source_image'] = $store_image_link['full_path'];
    $config['create_thumb'] = TRUE;
    $config['thumb_marker'] = '_300x300';
    $config['quality'] = '100%';
    $config['maintain_ratio'] = TRUE;
    $config['width'] = 300;
    $config['height'] = 300;

    $this->image_lib->initialize($config);
    $this->image_lib->resize();


    $logo_data=array('media_url'=>'uploads/item-gallery/'.date('Y').'/'.date('m'),'media_name'=>$store_image_link['raw_name'],
      'media_ext'=>$store_image_link['file_ext']
      );

    $this->db->insert('tb_medias',$logo_data);

    $uploaded_media_id = $this->db->select('media_id')
      ->from('tb_medias')
      ->where('media_name',$store_image_link['raw_name'])
      ->get()->result_array();    

    $current_media_id = $uploaded_media_id[0]['media_id'];

    $team_pic = array(
        'fk_logo_id' => $current_media_id
      );

    $this->db->where('team_id', $team_id);
    $this->db->update('tb_teams', $team_pic); 
    
   $location = base_url()."web/v1/api/update_team_logo";

    if($current_media_id != null)

    {
      echo '<script>alert(" Success . . !! \n Team Logo Uploaded..!!")</script>'; 
      echo '<META HTTP-EQUIV="Refresh" Content="0; URL='.$location.'">';
    }
    else
    {
      echo '<script>alert(" Failed . . !! \n Unable to Upload Team Logo..!!")</script>'; 
      echo '<META HTTP-EQUIV="Refresh" Content="0; URL='.$location.'">';
    }
  }
  
  //------------------------------------------------------------------------------------------------------------------///
  //------------------------------------------------------------------------------------------------------------------///

  function upload_player_logo_item($image_id,$player_id)
  {
    $this->load->library('image_lib');
    $this->load->helper('url');
    $current_year='./uploads/item-gallery/'.date('Y');
    $this_month='./uploads/item-gallery/'.date('Y').'/'.date('m');

      if(!is_dir($current_year)) //create the folder if it's not already exists
    {
          mkdir($current_year,0777,TRUE);
    }
      if(!is_dir($this_month)) //create the folder if it's not already exists
    {
          mkdir($this_month,0777,TRUE);
    }
    $config['upload_path'] = './uploads/item-gallery/'.date('Y').'/'.date('m');
    $config['allowed_types'] = 'gif|jpg|png|jpeg|JPEG';
    //$config['overwrite'] = 'TRUE';
    $config['encrypt_name'] = TRUE;
    $this->load->library('upload', $config);
    $this->upload->do_upload($image_id);
    //PHP
    //Configure upload.

    $store_image_link=$this->upload->data();

    $config['image_library'] = 'gd2';
    $config['source_image'] = $store_image_link['full_path'];
    $config['create_thumb'] = TRUE;
    $config['thumb_marker'] = '_300x300';
    $config['quality'] = '100%';
    $config['maintain_ratio'] = TRUE;
    $config['width'] = 300;
    $config['height'] = 300;

    $this->image_lib->initialize($config);
    $this->image_lib->resize();


    $logo_data=array('media_url'=>'uploads/item-gallery/'.date('Y').'/'.date('m'),'media_name'=>$store_image_link['raw_name'],
      'media_ext'=>$store_image_link['file_ext']
      );

    $this->db->insert('tb_medias',$logo_data);

    $uploaded_media_id = $this->db->select('media_id')
      ->from('tb_medias')
      ->where('media_name',$store_image_link['raw_name'])
      ->get()->result_array();    

    $current_media_id = $uploaded_media_id[0]['media_id'];

    $player_pic = array(
        'fk_logo_id' => $current_media_id
      );

    $this->db->where('player_id', $player_id);
    $this->db->update('tb_players', $player_pic); 
    
   $location = base_url()."web/v1/api/update_player_logo";

    if($current_media_id != null)

    {
      echo '<script>alert(" Success . . !! \n Player Logo Uploaded..!!")</script>'; 
      echo '<META HTTP-EQUIV="Refresh" Content="0; URL='.$location.'">';
    }
    else
    {
      echo '<script>alert(" Failed . . !! \n Unable to Upload Player Logo..!!")</script>'; 
      echo '<META HTTP-EQUIV="Refresh" Content="0; URL='.$location.'">';
    }
  }

   //------------------------------------------------------------------------------------------------------------------///
    //------------------------------------------------------------------------------------------------------------------///

  function team_position(){
   $this->load->view('update_team_position.php');
 }

  //------------------------------------------------------------------------------------------------------------------///
  function fetch_match_teams_stats(){
    
    $match_id = $this->input->post('match_id');

    $teams = $this->db->select(' teams_list_id , team_name , team_position')
			->from('tb_teams_list')
      ->join('tb_teams', 'tb_teams_list.fk_team_id = tb_teams.team_id','left')
  	  ->where('fk_match_id',$match_id)
			->get()->result_array();

$output = '';

if(count($teams) > 0)
{
  $i=1;
   foreach($teams as $team_key => $team)
 {
  $output .= '
  <tr>
   <td><input type="text" name="sl_no" id="sl_no" class="form-control" value="'.$i.'" readonly/></td>
   <td><input type="text" name="teams_list_id_'.$i.'" id="teams_list_id" class="form-control" value="'.$team['teams_list_id'].'" readonly/></td>
   <td><input type="text" name="team_name_'.$i.'" id="team_name" class="form-control" value="'.$team['team_name'].'" readonly/></td>
   <td><input type="text" name="team_position_'.$i.'" id="team_position" class="form-control" value="'.$team['team_position'].'" /></td>
  </tr>
  ';
   $i++;
 }
}
else
{
 $output .= '
 <tr>
  <td colspan="4" align="center">No Data Found</td>
 </tr>
 ';
}

echo $output;
    
  }
  
    
  //------------------------------------------------------------------------------------------------------------------///
  
public function update_team_positions()
	{
  
  $location = base_url()."web/v1/api/team_position";
//      $location = base_url()."api_old/fetch";

   $max_count	=	$this->input->post('sl_no');
   
//     echo "Multiplayer";
  $match_details = array();   
   
   for($x=1 ; $x <= $max_count ; $x++)
   {
     
//      echo $x;
     $teams_list_id	=	$this->input->post('teams_list_id_'.$x);

     $team_position	=	$this->input->post('team_position_'.$x);
	   $new_kills	=	$this->input->post('new_kills_'.$x);
     
     $match_details[$x-1]['teams_list_id'] = $teams_list_id;
     $match_details[$x-1]['team_position'] = $team_position;

   }
   
   $this->db->update_batch('tb_teams_list', $match_details, 'teams_list_id');

   

        echo '<script>alert(" Success . . !! \n Team Positions Updated..!!")</script>'; 
      echo '<META HTTP-EQUIV="Refresh" Content="0; URL='.$location.'">';

	}  
  
    //------------------------------------------------------------------------------------------------------------------///

     //------------------------------------------------------------------------------------------------------------------///

  function match_refresh(){
   $this->load->view('match_refresh.php');
 }

   
    //------------------------------------------------------------------------------------------------------------------///

  function fetch_match_refesh_status(){
    
    $match_id = $this->input->post('match_id');

    $matches = $this->db->select(' match_id , match_name , date_and_time , match_status ')
			->from('tb_match_list')
  	  ->where('match_id',$match_id)
			->get()->result_array();

$output = '';

    if($matches[0]['match_status'] == 0)
    {
        $matches[0]['match_status'] = "Upcoming";
    }  
    else if($matches[0]['match_status'] == 1)
    {
        $matches[0]['match_status'] = "Live";
    }  
    else if($matches[0]['match_status'] == 2)
    {
        $matches[0]['match_status'] = "In Review";
    }  
    else //if($match['match_status'] > 2)
    {
        $matches[0]['match_status'] = "Finished";
    }
    
    
if(count($matches) > 0)
{
//   $i=1;
   foreach($matches as $match_key => $match)
 {
//    <td><input type="text" name="sl_no" id="sl_no" class="form-control" value="'.$i.'" readonly/></td>
    
  $output .= '
  <tr>
   <td><input type="text" name="match_id" id="match_id" class="form-control" value="'.$match['match_id'].'" readonly/></td>
   <td><input type="text" name="match_name" id="match_name" class="form-control" value="'.$match['match_name'].'" readonly/></td>
   <td><input type="text" name="date_and_time" id="date_and_time" class="form-control" value="'.$match['date_and_time'].'" readonly/></td>
   <td><input type="text" name="match_status" id="match_status" class="form-control" value="'.$match['match_status'].'" readonly/></td>
  </tr>
  ';
//    $i++;
 }
}
else
{
 $output .= '
 <tr>
  <td colspan="4" align="center">No Data Found</td>
 </tr>
 ';
}

echo $output;
    
  }
  
    //------------------------------------------------------------------------------------------------------------------///
  //------------------------------------------------------------------------------------------------------------------///

  public function point_refresh()
	{
    $location = base_url()."web/v1/api/match_refresh";

     $match_id	=	$this->input->post('match_id');
     
     $status = $this-> pointRefresh($match_id);
    
//     echo 
    if($status==true)
//   echo "ok";
//       echo '<script>alert(" Success . . !! \n Match Stats Updated..!!")</script>'; 
    {
      echo '<script>alert(" Success . . !! \n Refresh Success")</script>'; 
      echo '<META HTTP-EQUIV="Refresh" Content="0; URL='.$location.'">';
    }
    else
    {
      echo '<script>alert(" Failed . . !! \n  Refresh Failed")</script>'; 
      echo '<META HTTP-EQUIV="Refresh" Content="0; URL='.$location.'">';
    }

}
   //------------------------------------------------------------------------------------------------------------------///
  //-------------------------------     Refresh everything during a match     -----------------------------------------------------///
  //$id = match_id 
    //------------------------------------------------------------------------------------------------------------------///

 function pointRefresh($id){
		
                                                      // 	$data = array(
                                                      //    array(
                                                      //       'game_id' => 1 ,
                                                      // //       'game_name' => 'My Name 2' ,
                                                      //       'game_name' => 'PUBG Mobile'
                                                      //    ),
                                                      //    array(
                                                      //       'game_id' => 2 ,
                                                      // //       'name' => 'Another Name 2' ,
                                                      //       'game_name' => 'COD Mobile'
                                                      //    )
                                                      // );

                                                      // $this->db->update_batch('tb_games', $data, 'game_id');
     $tournament_details=$this->db->select('*')
			->from('tb_match_list')
      ->join('tb_tournaments','tb_match_list.fk_tournament_id = tb_tournaments.tournament_id','left')
			->where('match_id',$id)
			->get()->result_array();
       
   
    if($tournament_details)
      {
                                                  //  $response=array('status'=>true, 'message'=>'Success','data'=>'');
                                                  //         $match_name=$tournament_details[0]["match_name"];
                                                  //         $tournament_name=$tournament_details[0]["tournament_name"];
        $tournament_type=$tournament_details[0]["tournament_type"];
      }
    

   
   if($tournament_type==6000)                       //tournament type is 6000 Battle royale
    {

        $br_details=$this->db->select('*')
          ->from('tb_br_match_details')
                                                          //       ->join('tb_tournaments', 'tb_match_list.fk_tournament_id = tb_tournaments.tournament_id')
                                                          //       ->join('tb_medias','tb_tournaments.fk_logo_id = tb_medias.media_id')
          ->where('fk_match_id',$id)
          ->get()->result_array();

        $count_br_details = count($br_details,0);  

        for ($x = 0; $x < $count_br_details; $x++) 
          { 

            $player_position = $this->db->select('team_position')
              ->from('tb_teams_list')
              ->join('tb_final_team_list', 'tb_teams_list.teams_list_id = tb_final_team_list.fk_team_list_id','left')
              ->join('tb_br_match_details', 'tb_final_team_list.fk_team_player_id = tb_br_match_details.fk_team_player_id','left')
              ->where('tb_br_match_details.fk_match_id',$id)
              ->where('tb_br_match_details.fk_team_player_id',$br_details[$x]['fk_team_player_id'])
              ->get()->result_array();


            $br_details[$x]['position'] = (int)$player_position[0]['team_position'];      


            $position_points = 0 ;  

            if( $br_details[$x]['position'] == 0 )
            {
              $position_points = 0 ;  
            }
            elseif( $br_details[$x]['position'] > 10 )
            {
              $position_points = 0 ;  
            }
            else
            {
              $position_points = 22 - ( 2 * $br_details[$x]['position'] );
            }

            $br_details[$x]['points'] = $position_points + ( $br_details[$x]['kills'] * 10 ) + ( $br_details[$x]['knocks'] * 5 ) ;
            $br_details[$x]['points'] = (int)$br_details[$x]['points'];
          }

        $this->db->update_batch('tb_br_match_details', $br_details, 'br_match_details_id');



        $user_sqd_player_details=$this->db->select('user_squad_player_id , fk_user_squad_id , my_player_id , my_player_points ')
          ->from('tb_user_squad_players')
          ->join('tb_user_squads', 'tb_user_squad_players.fk_user_squad_id = tb_user_squads.user_squad_id','left')
                                                                      //       ->join('tb_medias','tb_tournaments.fk_logo_id = tb_medias.media_id')
          ->where('fk_match_id',$id)
          ->get()->result_array();

        $count_sqd_player = count($user_sqd_player_details,0);  

       for ($y = 0; $y < $count_sqd_player; $y++) 
          { 
            $player_point =  $this->db->select('points')
              ->from('tb_br_match_details')
              ->where('fk_match_id',$id)
              ->where('fk_team_player_id',$user_sqd_player_details[$y]['my_player_id'])
              ->get()->result_array();


            $user_sqd_player_details[$y]['my_player_points'] = $player_point[0]['points'];
          }

        $this->db->update_batch('tb_user_squad_players', $user_sqd_player_details, 'user_squad_player_id');

    }                              //ending of tournament type is 6000 Battle royale

   
   
   else //($tournament_type==7000)                       //tournament type is 7000 Multiplayer
    {


        $mp_details=$this->db->select('*')
          ->from('tb_mp_match_details')
                                                          //       ->join('tb_tournaments', 'tb_match_list.fk_tournament_id = tb_tournaments.tournament_id')
                                                          //       ->join('tb_medias','tb_tournaments.fk_logo_id = tb_medias.media_id')
          ->where('fk_match_id',$id)
          ->get()->result_array();

        $count_mp_details = count($mp_details,0);  

        for ($x = 0; $x < $count_mp_details; $x++) 
          { 

            $player_position = $this->db->select('team_position')
              ->from('tb_teams_list')
              ->join('tb_final_team_list', 'tb_teams_list.teams_list_id = tb_final_team_list.fk_team_list_id','left')
              ->join('tb_mp_match_details', 'tb_final_team_list.fk_team_player_id = tb_mp_match_details.fk_team_player_id','left')
              ->where('tb_mp_match_details.fk_match_id',$id)
              ->where('tb_mp_match_details.fk_team_player_id',$mp_details[$x]['fk_team_player_id'])
              ->get()->result_array();


            $mp_details[$x]['win_lose'] = (int)$player_position[0]['team_position'];      


            $position_points = 0 ;  

            if( $mp_details[$x]['win_lose'] == 1 )                    //win
            {
              $position_points = 15 ;  
            }
            elseif( $mp_details[$x]['win_lose'] == 2 )                //lose
            {
              $position_points = 5 ;  
            }
            else                                                      //tie
            {
              $position_points = 10;
            }

            $mp_details[$x]['points'] = $position_points + ( $mp_details[$x]['kills'] * 5 ) + ( $mp_details[$x]['assists'] * 2 ) - ( $mp_details[$x]['deaths'] * 1 ) ;
            $mp_details[$x]['points'] = (int)$mp_details[$x]['points'];
          }

        $this->db->update_batch('tb_mp_match_details', $mp_details, 'mp_match_details_id');



        $user_sqd_player_details=$this->db->select('user_squad_player_id , fk_user_squad_id , my_player_id , my_player_points ')
          ->from('tb_user_squad_players')
          ->join('tb_user_squads', 'tb_user_squad_players.fk_user_squad_id = tb_user_squads.user_squad_id','left')
                                                                      //       ->join('tb_medias','tb_tournaments.fk_logo_id = tb_medias.media_id')
          ->where('fk_match_id',$id)
          ->get()->result_array();

        $count_sqd_player = count($user_sqd_player_details,0);  

       for ($y = 0; $y < $count_sqd_player; $y++) 
          { 
            $player_point =  $this->db->select('points')
              ->from('tb_mp_match_details')
              ->where('fk_match_id',$id)
              ->where('fk_team_player_id',$user_sqd_player_details[$y]['my_player_id'])
              ->get()->result_array();


            $user_sqd_player_details[$y]['my_player_points'] = $player_point[0]['points'];
          }

        $this->db->update_batch('tb_user_squad_players', $user_sqd_player_details, 'user_squad_player_id');

    }                              //ending of tournament type is 7000 Multiplayer
   
   
   
   
   
   
    $usr_sqd_details=$this->db->select('*')
			->from('tb_user_squads')
                                                      //       ->join('tb_tournaments', 'tb_match_list.fk_tournament_id = tb_tournaments.tournament_id')
                                                      //       ->join('tb_medias','tb_tournaments.fk_logo_id = tb_medias.media_id')
      ->where('fk_match_id',$id)
  		->get()->result_array();
   
    $count_usr_sqd = count($usr_sqd_details,0);  

    for ($z = 0; $z < $count_usr_sqd; $z++) 
      { 
        $position_points = 0 ;  

        $igl_point =  $this->db->select('my_player_points')
          ->from('tb_user_squad_players')
          ->join('tb_user_squads', 'tb_user_squad_players.fk_user_squad_id = tb_user_squads.user_squad_id','left')
          ->where('fk_match_id',$id)
          ->where('my_player_id',$usr_sqd_details[$z]['igl_team_player_id'])
          ->get()->result_array();

        $squad_point =  $this->db->select_sum('my_player_points')
          ->from('tb_user_squad_players')
          ->where('fk_user_squad_id',$usr_sqd_details[$z]['user_squad_id'])
          ->get()->result_array();
//       echo "IGL : ";
//       print_r($igl_point);
//       echo "\n";
      
//       echo "Total : ";
//       print_r($squad_point);
//       echo "\n";

      
         $usr_sqd_details[$z]['total_squad_points'] = $igl_point[0]['my_player_points'] + $squad_point[0]['my_player_points'] ;
         $usr_sqd_details[$z]['total_squad_points'] = (int)$usr_sqd_details[$z]['total_squad_points'];
      }
   
     $this->db->update_batch('tb_user_squads', $usr_sqd_details, 'user_squad_id');
   
   
   
   
   
        
    $user_sqd = $this->db->select('*')
			->from('tb_user_squads')
//       ->join('tb_user_squads', 'tb_user_squad_players.fk_user_squad_id = tb_user_squads.user_squad_id')
                                                                  //       ->join('tb_medias','tb_tournaments.fk_logo_id = tb_medias.media_id')
      ->where('fk_match_id',$id)
      ->order_by('total_squad_points','DESC')
      ->get()->result_array();
          
   
   
                                                                  //       $br_details1=$this->db->select('player_name as playerName , team_name as teamName , kills as playerKills , knocks as playerKnocks, position as teamPosition, points as playerTotalPoints')
                                                                  // 			  ->from('tb_br_match_details')
                                                                  //         ->join('tb_team_players','tb_br_match_details.fk_team_player_id = tb_team_players.team_player_id','left')
                                                                  //         ->join('tb_players','tb_team_players.fk_player_id = tb_players.player_id','left')
                                                                  //         ->join('tb_teams','tb_team_players.fk_team_id = tb_teams.team_id','left')
                                                                  //         ->where('fk_match_id',$id)
                                                                  //         ->order_by('points','DESC')
                                                                  // 			  ->get()->result_array();
   
   
		if($user_sqd)
    {
//     return($user_sqd);
// 			$response=array('status'=>true, 'message'=>'Success','data'=>$user_sqd);
		$response=true;
    }
    
		else
    {
// 			$response=array('status'=>false, 'message'=>'Failed','data'=>'');
				$response=false;
}
                                                                   ////return $match_list;
//     header('Content-Type: application/json');
 //   echo json_encode($response);
	return ($response);
 }	  
  
  
  //------------------------------------------------------------------------------------------------------------------///
  //------------------------------------------------------------------------------------------------------------------///
    //------------------------------------------------------------------------------------------------------------------///

  function list_users(){
   $this->load->view('list_users.php');
 }

  //------------------------------------------------------------------------------------------------------------------///
 
 function fetch_users(){
		
 
   $users = $this->db->select(' user_id , username , first_name , last_name ')
			->from('tb_users')
  	  ->where('is_active',1)
			->get()->result_array();

$output = '';

if(count($users) > 0)
{
  $i=1;
   foreach($users as $user_key => $user)
 {
  $output .= '
  <tr>
   <td><input type="text" name="sl_no" id="sl_no" class="form-control" value="'.$i.'" readonly/></td>
   <td><input type="text" name="user_id_'.$i.'" id="user_id" class="form-control" value="'.$user['user_id'].'" readonly/></td>
   <td><input type="text" name="username_'.$i.'" id="username" class="form-control" value="'.$user['username'].'" readonly/></td>
   <td><input type="text" name="first_name_'.$i.'" id="first_name" class="form-control" value="'.$user['first_name'].'" readonly/></td>
   <td><input type="text" name="last_name_'.$i.'" id="last_name" class="form-control" value="'.$user['last_name'].'" readonly/></td>
  </tr>
  ';
   $i++;
 }
}
else
{
 $output .= '
 <tr>
  <td colspan="4" align="center">No Data Found</td>
 </tr>
 ';
}

echo $output;
}
    //------------------------------------------------------------------------------------------------------------------///

  function list_transactions(){
   $this->load->view('list_transactions.php');
 }

  //------------------------------------------------------------------------------------------------------------------///
 
 function fetch_transactions(){
		
 
   $transactions = $this->db->select('username , transaction_id , tb_transactions.transaction_no as transaction_no , razor_transaction_id , tb_transactions.type as type , tb_transactions.amount as amount , txn_date_time , remarks ')
			->from('tb_razor_transactions')
      ->join('tb_transactions', 'tb_razor_transactions.transaction_no = tb_transactions.transaction_no','left')
      ->join('tb_users', 'tb_transactions.fk_user_id = tb_users.user_id','left')
  	  ->where('razor_transaction_id !=',null)
      ->get()->result_array();

$output = '';

if(count($transactions) > 0)
{
  $i=1;
   foreach($transactions as $transaction_key => $transaction)
 {
  $output .= '
  <tr>
   <td><input type="text" name="sl_no" id="sl_no" class="form-control" value="'.$i.'" readonly/></td>
   <td><input type="text" name="username_'.$i.'" id="username" class="form-control" value="'.$transaction['username'].'" readonly/></td>
   <td><input type="text" name="transaction_id_'.$i.'" id="transaction_id" class="form-control" value="'.$transaction['transaction_id'].'" readonly/></td>
   <td><input type="text" name="transaction_no_'.$i.'" id="transaction_no" class="form-control" value="'.$transaction['transaction_no'].'" readonly/></td>
   <td><input type="text" name="razor_transaction_id_'.$i.'" id="razor_transaction_id" class="form-control" value="'.$transaction['razor_transaction_id'].'" readonly/></td>
   <td><input type="text" name="type_'.$i.'" id="type" class="form-control" value="'.$transaction['type'].'" readonly/></td>
   <td><input type="text" name="amount_'.$i.'" id="amount" class="form-control" value="'.$transaction['amount'].'" readonly/></td>
   <td><input type="text" name="txn_date_time_'.$i.'" id="txn_date_time" class="form-control" value="'.$transaction['txn_date_time'].'" readonly/></td>
   <td><input type="text" name="remarks_'.$i.'" id="remarks" class="form-control" value="'.$transaction['remarks'].'" readonly/></td>
  </tr>
  ';
   $i++;
 }
}
else
{
 $output .= '
 <tr>
  <td colspan="4" align="center">No Data Found</td>
 </tr>
 ';
}
// echo '<script>alert(" Failed . . !! \n  Refresh Failed")</script>'; 

echo $output;
}
    //------------------------------------------------------------------------------------------------------------------///
    //------------------------------------------------------------------------------------------------------------------///

  
 
  
  
  
            //------------------------------------------------------------------------------------------------------------------///
            //--------------------              E N D          O F           F I L E             -------------------------------///
            //------------------------------------------------------------------------------------------------------------------///

}