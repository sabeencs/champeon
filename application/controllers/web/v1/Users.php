<?php defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/ImplementJwt.php';


class Users extends CI_Controller
{

  //------------------------------------------------------------------------------------------------------------------///
  //-------------------------------      WEB     USERS      -----------------------------------------------------///
  //------------------------------------------------------------------------------------------------------------------///
  
  
  public function __construct()
	{
		parent::__construct();
		$this->load->model('api_model');
		$this->load->library('form_validation');
    $this->objOfJwt = new ImplementJwt();
    date_default_timezone_set('Asia/Kolkata');
  }

  
  //------------------------------------------------------------------------------------------------------------------///
  //-------------------------------    Get contents of Token Header      -----------------------------------------------------///
  //------------------------------------------------------------------------------------------------------------------///
  
  
    public function GetTokenData()
    {
    $received_Token = $this->input->request_headers('Authorization');
    $token_array = explode(" ",$received_Token['Authorization']);
      try
            {
            $jwtData = $this->objOfJwt->DecodeToken($token_array[1]);
        
            $decrypted_string=openssl_decrypt($jwtData['uId'],"AES-128-ECB",passkey2);
            $jwtData['uId'] = (int)$decrypted_string;
        
//             $jwtData = $this->objOfJwt->DecodeToken($received_Token['Authorization']);
            //echo json_encode($jwtData);
            return($jwtData);
            }
            catch (Exception $e)
            {
            http_response_code('401');
            echo json_encode(array( "status" => false, "message" => $e->getMessage()));exit;
            }
    }
  
    
    //--------------------------- login ----------------------------/// 

      function login()  
      {  
           //https://www.esports.envose.com/main/login  
           $data['title'] = 'Sample Login Page';  
           $this->load->view('login.php',$data);  
      }  
     //----------------------------------------------------------------------------------/// 

      function logout()  
      {  
           $this->session->unset_userdata('username');  
           redirect(base_url() . 'web/v1/users/login');  
      }  
  

  
   //------------------------------------------------------------------------------------------------------------------///
  //-------------------------------    create new User - Sign Up      -----------------------------------------------------///
  //------------------------------------------------------------------------------------------------------------------///
 
   
  public function createUser()
	{

  
    
//     $data = '{
//                   "userEmail": "samples1@samplemail.com",
//                   "password": "passwordd",
//                   "userMobile": "+919999999992",
//                   "firstName": "test",
//                   "lastName": "test"
//               }';

  
//     $input_data = json_decode($data , true);

                                                            //     $user_id = 1;
    $input_data = json_decode(trim(file_get_contents('php://input')), true);
                                                            //    $json_str = file_get_contents('php://input');
                                                            //     $json_str = file_get_contents('php://pastie.org/p/5gwT78JhW3Wm94w4tmB7rt');

                                                            # Get as an object
                                                            //     $json_obj = json_decode($json_str);
                                                            //      echo $data;
                                                            //     print_r($input_data);

    $username = $input_data['username'] ;
    $password = $input_data['password'] ;
                                                          //     echo "\n match_id : ";
                                                          //     echo $match_id;
                                                          //     echo "\n";

                                                          //     $upload_squad_2 = array();        //for 2nd table data storage
                                                          //     $m = 0;                           //for 2nd table data storage
                                                          //     $max_squad_no=$this->db->select_max('user_squad_no')
    $username_used_count=$this->db->select('username')
      ->from('tb_web_users')
      ->where('username',$username)
      ->count_all_results();     
                                                          // 			->get()->result_array();
    
    if( $username_used_count > 0)
    {
 			$response=array('status'=>false, 'message'=>'Username already used');
    }
                                                              //     print_r($max_squad_no);
    else
    {
      

      
      $upload_user_1 = array(
				'username'	=>	$username,
				'password'		=>	$password,
        'created_at' => date("Y-m-d H:i:s"),
        'is_active' => '1'
      );
    
		$this->db->insert('tb_web_users', $upload_user_1);

    $user_id=$this->db->select('user_id')
			->from('tb_web_users')
			->where('username',$username)
			->get()->result_array();

    $current_user_id = (int)$user_id[0]['user_id'];
    
//     $upload_user_details_1 = array(
// 				'fk_user_id'	=>	$current_user_id
//       );

//     $this->db->insert('tb_user_details', $upload_user_details_1);
  
//     $this->db->insert('tb_bank', $upload_user_details_1);
    
//     $this->db->insert('tb_user_wallet', $upload_user_details_1);
  
      
     $tokenData['uId'] = $current_user_id;
      
      
     $string_to_encrypt=$tokenData['uId'];
     $encrypted_string=openssl_encrypt($string_to_encrypt,"AES-128-ECB",passkey2);
     $tokenData['uId'] = $encrypted_string; 

      
      
    $jwtToken = $this->objOfJwt->GenerateToken($tokenData);
                                                                      //     echo json_encode(array('Token'=>$jwtToken));
  
      
     $access_token = array(
        'access_token' => $jwtToken
    );
      
    $this->db->where('user_id', $current_user_id);
    $this->db->update('tb_web_users', $access_token); 
      
      
    $response=array('status'=>true, 'message'=>'User Created Successfully' , 'token'=>$jwtToken);
  
      
    }
    header('Content-Type: application/json');
    echo json_encode($response);
  
  }
  
  
    
  //------------------------------------------------------------------------------------------------------------------///
  //-------------------------------    User Login - Sign In      -----------------------------------------------------///
  //------------------------------------------------------------------------------------------------------------------///
 
    
  public function userLogin()
	{
//     $data = '{
//               "status": true,
//               "message": "Success",
//               "userData": {
//                   "userEmail": "sample2@samplemail.com",
//                   "password": "passwordd",
//                   "userMobile": "+919999999994",
//                   "firstName": "test",
//                   "lastName": "test"
//                   }
//               }';
//     $data = '{
//                   "userEmail": "vikbaca@gmail.com",
//                   "password": "vikbaca"
//               }';

//     $input_data = json_decode($data , true);

                                                            //     $user_id = 1;
//      $input_data = json_decode(trim(file_get_contents('php://input')), true);
                                                            //    $json_str = file_get_contents('php://input');
                                                            //     $json_str = file_get_contents('php://pastie.org/p/5gwT78JhW3Wm94w4tmB7rt');

                                                            # Get as an object
                                                            //     $json_obj = json_decode($json_str);
                                                              //      echo $data;
                                                              //     print_r($input_data);

                                                              //     $email_id = $input_data['userData']['userEmail'] ;
                                                              //     $password = $input_data['userData']['password'] ;
                                                              //     $mobile_no = $input_data['userData']['userMobile'] ;
                                                              //     $first_name = $input_data['userData']['firstName'] ;
                                                              //     $last_name = $input_data['userData']['lastName'] ;

    $username = $this->input->post('username');  
    $password = $this->input->post('password');  

//     $username = $input_data['username'] ;
//     $password = $input_data['password'] ;
    
    if($username == null)
    {
      $response=array('status'=>false, 'message'=>'Login Failed' , 'Data'=>$input_data);
    }
    else
    {                                                  
                                                            //     $mobile_no = $input_data['userMobile'] ;
                                                            //     $first_name = $input_data['firstName'] ;
                                                            //     $last_name = $input_data['lastName'] ;
                                                            //     echo "\n match_id : ";
                                                            //     echo $match_id;
                                                            //     echo "\n";
      $username_used_count = 0;
                                                            //     $upload_squad_2 = array();        //for 2nd table data storage
                                                            //     $m = 0;                           //for 2nd table data storage
                                                            //     $max_squad_no=$this->db->select_max('user_squad_no')
      $username_used_count=$this->db->select('username')
        ->from('tb_web_users')
        ->where('username',$username)
        ->count_all_results();     
                                                            // 			->get()->result_array();
                                                            //     $tokens = array();
                                                            //     $id_no = 1;
                                                            //     $user_name_count = 1;
      if( $username_used_count == 0)
      {
        $response=array('status'=>false, 'message'=>'User Not Found');
      }
                                                            //     elseif( $mobile_used_count > 0 )
                                                            //     {
                                                            //       $response=array('status'=>false, 'message'=>'Mobile Number already used');
                                                            //     }
                                                                //     print_r($max_squad_no);
      else
      {

        $user_details=$this->db->select(' user_id as uId ,username as userName, access_token as accessToken')
          ->from('tb_web_users')
          ->where('username',$username)
          ->where('password',$password)
          ->get()->result_array();
                                                                //         ->count_all_results();     
        $user_count = count($user_details,0);  


                                                                //         echo "\n user_name_count : ";
                                                                //         echo $user_name_count;
                                                                //         echo "\n";

          if( $user_count == 0 )
          {
            $response=array('status'=>false, 'message'=>'Invalid Password');
          }
          else
          {

            $user_details[0]['uId'] = (int)$user_details[0]['uId'];
            $current_user_id = $user_details[0]['uId'];

            $tokenData['uId'] = $current_user_id;
            $tokenData['iat'] = strtotime("now");                                    
            $tokenData['nbf'] = strtotime("now");                           
            $tokenData['exp'] = strtotime("+3600 Seconds");       
            
                  
            $string_to_encrypt=$tokenData['uId'];
            $encrypted_string=openssl_encrypt($string_to_encrypt,"AES-128-ECB",passkey2);
            $tokenData['uId'] = $encrypted_string; 

            
            $jwtToken2 = $this->objOfJwt->GenerateToken($tokenData);
                                                                      //     echo json_encode(array('Token'=>$jwtToken));


             $user_token = array(
                'user_token' => $jwtToken2,
                'user_token_creatred_at' => date("Y-m-d H:i:s",$tokenData['iat']),
                'fk_user_id' => $current_user_id
            );
            $this->db->insert('tb_web_user_tokens', $user_token); 



//             $tokens['accessToken'] = $user_details[0]['accessToken'];
//             $tokens['userToken'] = $jwtToken2;
// 
            $response=array('status'=>true, 'message'=>'User Login Success' , 'username'=>$username , 'accessToken'=>$user_details[0]['accessToken'] , 'userToken' => $jwtToken2);
            $this->session->set_userdata($response);  
            redirect(base_url() . 'web/v1/api/main');  
          }
  
      }
    
    }
    header('Content-Type: application/json');
    echo json_encode($response);
  
  }
  
    
  
  
  
 
  
  
  
  
  
}