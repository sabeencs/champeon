<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Api extends CI_Controller
{
  
  public function __construct()
	{
		parent::__construct();
		$this->load->model('api_model');
		$this->load->library('form_validation');
	}

 
 //---------------------- ADMIN SIDE ----------------------------/// 
 //---------------------- ADMIN SIDE ----------------------------/// 
 //---------------------- ADMIN SIDE ----------------------------/// 
 //---------------------- ADMIN SIDE ----------------------------/// 

//--------------------------- get all users ----------------------------/// 
  
  function get_all_users(){   
		
		$users=$this->db->select('*')
			->from('tb_users')
			->get()->result_array();
		if($users){
			
		
		$response=array('status'=>'true','data'=>$users);
		 
		 		}
		else{
			
			$response=array('status'=>'false','data'=>'');
     
		}
		
		echo json_encode($response);
		
	}
  
  //---------------------- get single user by user_id ----------------------------/// 
    
  function get_single_user($id){   
		
		$user=$this->db->select('*')
			->from('tb_users')
      ->where('user_id',$id)
			->get()->result_array();
		if($user){
			
		
		$response=array('status'=>'true','data'=>$user);
		 
		 		}
		else{
			
			$response=array('status'=>'false','data'=>'');
     
		}
		
		echo json_encode($response);
		
	}
  
  
  //---------------------- get all user details ----------------------------/// 
 
  function get_all_user_details(){   
		
		$user_details=$this->db->select('*')
			->from('tb_user_details')
			->get()->result_array();
		if($user_details){
			
		
		$response=array('status'=>'true','data'=>$user_details);
		 
		 		}
		else{
			
			$response=array('status'=>'false','data'=>'');
     
		}
		
		echo json_encode($response);
		
	}
  
 
  //---------------------- get single user details using user_details_id  ----------------------------/// 


  function get_single_user_details($id){   
		
		$user_details=$this->db->select('*')
			->from('tb_user_details')
      ->where('user_details_id',$id)
			->get()->result_array();
		if($user_details){
			
		
		$response=array('status'=>'true','data'=>$user_details);
		 
		 		}
		else{
			
			$response=array('status'=>'false','data'=>'');
     
		}
		
		echo json_encode($response);
		
	}
  

  //----------------------   //get single user details using user_id  ----------------------------/// 


  function get_user_details_by_user_id($id){   
		
		$user_details=$this->db->select('*')
			->from('tb_user_details')
      ->join('tb_users','tb_user_details.fk_user_id = tb_users.user_id')
      ->where('user_id',$id)
			->get()->result_array();
		if($user_details){
			
		
		$response=array('status'=>'true','data'=>$user_details);
		 
		 		}
		else{
			
			$response=array('status'=>'false','data'=>'');
     
		}
		
		echo json_encode($response);
		
	}
  
 //---------------------- get single user bank details  ----------------------------/// 

 
  function get_user_bank_details($id){   
		
		$bank_details=$this->db->select('*')
			->from('tb_bank')
      ->where('fk_user_id',$id)
			->get()->result_array();
		if($bank_details){
			
		
		$response=array('status'=>'true','data'=>$bank_details);
		 
		 		}
		else{
			
			$response=array('status'=>'false','data'=>'');
     
		}
		
		echo json_encode($response);
		
	}
  
  
 //---------------------- get all transactions   ----------------------------/// 
  

  function get_all_transactions(){   
		
		$transactions=$this->db->select('*')
			->from('tb_transactions')
			->get()->result_array();
		if($transactions){
			
		
		$response=array('status'=>'true','data'=>$transactions);
		 
		 		}
		else{
			
			$response=array('status'=>'false','data'=>'');
     
		}
		
		echo json_encode($response);
		
	}
  
  
 //---------------------- get single user transactions  ----------------------------/// 

  function get_user_transactions($id){   
		
		$transactions=$this->db->select('*')
			->from('tb_transactions')
      ->where('fk_user_id',$id)
			->get()->result_array();
		if($transactions){
			
		
		$response=array('status'=>'true','data'=>$transactions);
		 
		 		}
		else{
			
			$response=array('status'=>'false','data'=>'');
     
		}
		
		echo json_encode($response);
		
	}
  

  //----------------------  get a specific transactions by transaction id   ----------------------------/// 

  function get_transaction_by_transaction_id($id){   
		
		$transactions=$this->db->select('*')
			->from('tb_transactions')
      ->where('transaction_id',$id)
			->get()->result_array();
		if($transactions){
			
		
		$response=array('status'=>'true','data'=>$transactions);
		 
		 		}
		else{
			
			$response=array('status'=>'false','data'=>'');
     
		}
		
		echo json_encode($response);
		
	}
  

   //----------------------  get a specific transactions by transaction number  ----------------------------/// 

  function get_transaction_by_transaction_no($id){   
		
		$transactions=$this->db->select('*')
			->from('tb_transactions')
      ->where('transaction_no',$id)
			->get()->result_array();
		if($transactions){
			
		
		$response=array('status'=>'true','data'=>$transactions);
		 
		 		}
		else{
			
			$response=array('status'=>'false','data'=>'');
     
		}
		
		echo json_encode($response);
		
	}
  

 //----------------------  get reset pass code  ----------------------------/// 

  function get_reset_pass_code($id){   
		
		$token=$this->db->select('token_id')
			->from('tb_user_tokens')
      ->where('fk_user_id',$id)
      ->order_by('token_id desc') 
			->get()->result_array();
		if($token){
					
		$token_id = $token[0]["token_id"];
		 
		 		}
		else{
			
			$response=array('status'=>'false','data'=>'');
     
		}
		
    $reset_pass=$this->db->select('reset_pass_code,reset_pass_code_created_at')
			->from('tb_user_tokens')
      ->where('token_id',$token_id)
			->get()->result_array();
		if($reset_pass){
			
		
		$response=array('status'=>'true','data'=>$reset_pass);
		 
		 		}
		else{
			
			$response=array('status'=>'false','data'=>'');
     
		}
		
    
		echo json_encode($response);
		
	}
  
  
  //---------------------- get mobile otp   ----------------------------/// 

  function get_mobile_otp($id){   
		
		$token=$this->db->select('token_id')
			->from('tb_user_tokens')
      ->where('fk_user_id',$id)
      ->order_by('token_id desc') 
			->get()->result_array();
		if($token){
					
		$token_id = $token[0]["token_id"];
		 
		 		}
		else{
			
			$response=array('status'=>'false','data'=>'');
     
		}
		
    $mobile_otp=$this->db->select('mob_ver_otp,mob_ver_otp_created_at')
			->from('tb_user_tokens')
      ->where('token_id',$token_id)
			->get()->result_array();
		if($mobile_otp){
			
		
		$response=array('status'=>'true','data'=>$mobile_otp);
		 
		 		}
		else{
			
			$response=array('status'=>'false','data'=>'');
     
		}
		
    
		echo json_encode($response);
		
	}
  

  //---------------------- get access token  ----------------------------/// 

  function get_access_token($id){   
		
		$token=$this->db->select('token_id')
			->from('tb_user_tokens')
      ->where('fk_user_id',$id)
      ->order_by('token_id desc') 
			->get()->result_array();
		if($token){
					
		$token_id = $token[0]["token_id"];
		 
		 		}
		else{
			
			$response=array('status'=>'false','data'=>'');
     
		}
		
    $access_token=$this->db->select('access_token,access_token_created_at')
			->from('tb_user_tokens')
      ->where('token_id',$token_id)
			->get()->result_array();
		if($access_token){
			
		
		$response=array('status'=>'true','data'=>$access_token);
		 
		 		}
		else{
			
			$response=array('status'=>'false','data'=>'');
     
		}
		
    
		echo json_encode($response);
		
	}


//--------------------------  get all games  ----------------------------/// 

   function get_games(){   
		
		$games=$this->db->select('*')
			->from('tb_games')
			->get()->result_array();
		if($games){
			
		
		$response=array('status'=>'true','data'=>$games);
		 
		 		}
		else{
			
			$response=array('status'=>'false','data'=>'');
     
		}
		
		echo json_encode($response);
		
	}
  
  
  //---------------------- get single game by game_id ----------------------------/// 
    
  function get_game_by_id($id){   
		
		$game=$this->db->select('*')
			->from('tb_games')
      ->where('game_id',$id)
			->get()->result_array();
		if($game){
			
		
		$response=array('status'=>'true','data'=>$game);
		 
		 		}
		else{
			
			$response=array('status'=>'false','data'=>'');
     
		}
		
		echo json_encode($response);
		
	}
  
  

//--------------------------  get all medias  ----------------------------/// 

   function get_medias(){   
		
		$medias=$this->db->select('*')
			->from('tb_medias')
			->get()->result_array();
		if($medias){
			
		
		$response=array('status'=>'true','data'=>$medias);
		 
		 		}
		else{
			
			$response=array('status'=>'false','data'=>'');
     
		}
		
		echo json_encode($response);
		
	}
  
  
  //---------------------- get single media by media_id ----------------------------/// 
    
  function get_media_by_id($id){   
		
		$media=$this->db->select('*')
			->from('tb_medias')
      ->where('media_id',$id)
			->get()->result_array();
		if($media){
			
		
		$response=array('status'=>'true','data'=>$media);
		 
		 		}
		else{
			
			$response=array('status'=>'false','data'=>'');
     
		}
		
		echo json_encode($response);
		
	}  

  
  //---------------------- get single media by media extension ----------------------------/// 
    
  function get_media_by_ext($id){   
		
		$media=$this->db->select('*')
			->from('tb_medias')
      ->where('media_ext',$id)
			->get()->result_array();
		if($media){
			
		
		$response=array('status'=>'true','data'=>$media);
		 
		 		}
		else{
			
			$response=array('status'=>'false','data'=>'');
     
		}
		
		echo json_encode($response);
		
	}  

  
//--------------------------  get all tournaments  ----------------------------/// 

   function get_tournaments(){   
		
		$tournaments=$this->db->select('*')
			->from('tb_tournaments')
			->get()->result_array();
		if($tournaments){
			
		
		$response=array('status'=>'true','data'=>$tournaments);
		 
		 		}
		else{
			
			$response=array('status'=>'false','data'=>'');
     
		}
		
		echo json_encode($response);
		
	}
  
  
  //----------------------   //get single tournament details using tournament_id  ----------------------------/// 


  function get_tournament_details_by_id($id){   
		
		$tournament_details=$this->db->select('*')
			->from('tb_tournaments')
      ->join('tb_medias','tb_tournaments.fk_logo_id = tb_medias.media_id')
      ->join('tb_games','tb_tournaments.fk_game_id = tb_games.game_id')
      ->where('tournament_id',$id)
			->get()->result_array();
		if($tournament_details){
			
		
		$response=array('status'=>'true','data'=>$tournament_details);
		 
		 		}
		else{
			
			$response=array('status'=>'false','data'=>'');
     
		}
		
		echo json_encode($response);
		
	}
  
  
  //---------------------- get all tournaments by same game id ----------------------------/// 
    
  function get_tournaments_by_game_id($id){   
		
		$tournaments=$this->db->select('*')
			->from('tb_tournaments')
      ->join('tb_games','tb_tournaments.fk_game_id = tb_games.game_id')
      ->where('fk_game_id',$id)
			->get()->result_array();
		if($tournaments){
			
		
		$response=array('status'=>'true','data'=>$tournaments);
		 
		 		}
		else{
			
			$response=array('status'=>'false','data'=>'');
     
		}
		
		echo json_encode($response);
		
	}  

  
//--------------------------  get all matches  ----------------------------/// 

   function get_matches(){   
		
		$matches=$this->db->select('*')
			->from('tb_match_list')
			->get()->result_array();
		if($matches){
			
		
		$response=array('status'=>'true','data'=>$matches);
		 
		 		}
		else{
			
			$response=array('status'=>'false','data'=>'');
     
		}
		
		echo json_encode($response);
		
	}
  
  
  //----------------------   //get matches in a tournament by tournament id  ----------------------------/// 


  function get_matches_by_tournament_id($id){   
		
		$matches_in_tournament=$this->db->select('*')
			->from('tb_match_list')
      ->where('fk_tournament_id',$id)
			->get()->result_array();
		if($matches_in_tournament){
			
		
		$response=array('status'=>'true','data'=>$matches_in_tournament);
		 
		 		}
		else{
			
			$response=array('status'=>'false','data'=>'');
     
		}
		
		echo json_encode($response);
		
	}
  
  
  //---------------------- get matches in between 2 dates ----------------------------/// 
    
  function get_matches_by_date($date1,$date2){   
		
		$matches=$this->db->select('*')
			->from('tb_match_list')
//      ->where('date_and_time',$id)
      ->where('date_and_time BETWEEN "'. date('Y-m-d 00:00:00', strtotime($date1)). '" and "'. date('Y-m-d 23:59:59', strtotime($date2)).'"')
      ->get()->result_array();
		if($matches){
			
		
		$response=array('status'=>'true','data'=>$matches);
		 
		 		}
		else{
			
			$response=array('status'=>'false','data'=>'');
     
		}
		
		echo json_encode($response);
		
	}  
  
  
  
//--------------------------  get all teams  ----------------------------/// 

   function get_teams(){   
		
		$teams=$this->db->select('*')
			->from('tb_teams')
			->get()->result_array();
		if($teams){
			
		
		$response=array('status'=>'true','data'=>$teams);
		 
		 		}
		else{
			
			$response=array('status'=>'false','data'=>'');
     
		}
		
		echo json_encode($response);
		
	}
  

   
//--------------------------  get team details by team id  ----------------------------/// 

   function get_team_by_id($id){   
		
		$team_details=$this->db->select('*')
			->from('tb_teams')
      ->join('tb_medias','tb_teams.fk_logo_id = tb_medias.media_id')
      ->where('team_id',$id)
			->get()->result_array();
		if($team_details){
			
   		$response=array('status'=>'true','data'=>$team_details);
		 
		 		}
		else{
			
			$response=array('status'=>'false','data'=>'');
     
		}
		
		echo json_encode($response);
		
	}
  
  
  //--------------------------  ************************  ----------------------------/// 

  
  
  
  
  
  
  
  


}
