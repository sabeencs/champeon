<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Install extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->database();
    }
    public function index()
    {
         // $this->db->query(' DROP TABLE ' );
      
      
          // USER LOGIN TABLE
         $this->db->query('CREATE TABLE IF NOT EXISTS tb_users
         (
         user_id BIGINT(20) NOT NULL AUTO_INCREMENT,
         PRIMARY KEY(user_id),
         email VARCHAR(250) UNIQUE,
         mobile CHAR(15) UNIQUE,
         password VARCHAR(250),
         username VARCHAR(250) UNIQUE,
         first_name VARCHAR(250),
         last_name VARCHAR(250), 
         access_token VARCHAR(250) UNIQUE,
         created_at DATETIME,
         email_verified INT(1) DEFAULT 0,
         mobile_verified INT(1) DEFAULT 0,
         is_active INT(1)
         ) ENGINE=InnoDB DEFAULT CHARSET=utf8;');     
		
		
		 //fk_policy_id INT(11), FOREIGN KEY (fk_policy_id) REFERENCES tb_fire_policies(policy_id) vehicle_engine_cc INT(11)
      
      // MEDIAS 
      
      $this->db->query('CREATE TABLE IF NOT EXISTS tb_medias
         (
         media_id BIGINT(20) NOT NULL AUTO_INCREMENT,
         PRIMARY KEY(media_id),
         media_url VARCHAR(250),
         media_name VARCHAR(250),
         media_ext VARCHAR(10)
         ) ENGINE=InnoDB DEFAULT CHARSET=utf8;');  
      

            
        // USER DETAILS TABLE  
      //gender = 0 not say , 1 male, 2 female
      //Default user profile pic id in media is 3. So default fk_user_dp_id is 3
      
       $this->db->query('CREATE TABLE IF NOT EXISTS tb_user_details
         (
         user_details_id BIGINT(20) NOT NULL AUTO_INCREMENT,
         PRIMARY KEY(user_details_id),
         about VARCHAR(250),
         dob DATE,
         gender INT(1) DEFAULT 0,
         country VARCHAR(250),
         state VARCHAR(250),
         city VARCHAR(250),
         fk_user_dp_id BIGINT(20) DEFAULT 3,
         fk_user_id BIGINT(20), 
         my_referral_id VARCHAR(50),
         invited_referral_id VARCHAR(50),
         FOREIGN KEY (fk_user_dp_id) REFERENCES tb_medias(media_id),
         FOREIGN KEY (fk_user_id) REFERENCES tb_users(user_id)
         ) ENGINE=InnoDB DEFAULT CHARSET=utf8;');           
      
      
      
       //  Wallet DETAILS
      
      $this->db->query('CREATE TABLE IF NOT EXISTS tb_user_wallet
         (
         user_wallet_id BIGINT(20) NOT NULL AUTO_INCREMENT,
         PRIMARY KEY(user_wallet_id),
         wallet_balance FLOAT(20,2) DEFAULT 0.00,
         champ_coins FLOAT(20,2) DEFAULT 0.00,
         total_winnings FLOAT(20,2) DEFAULT 0.00,
         user_points INT(10) DEFAULT 0,
         fk_user_id BIGINT(20), 
         FOREIGN KEY (fk_user_id) REFERENCES tb_users(user_id)
         ) ENGINE=InnoDB DEFAULT CHARSET=utf8;');     
      

      
      //  BANK DETAILS
      
      $this->db->query('CREATE TABLE IF NOT EXISTS tb_bank
         (
         bank_details_id BIGINT(20) NOT NULL AUTO_INCREMENT,
         PRIMARY KEY(bank_details_id),
         full_name VARCHAR(250),
         bank_account_no VARCHAR(20),
         ifsc_code VARCHAR(11),
         bank_name VARCHAR(250),
         pan_card VARCHAR(20),
         is_default INT(1),
         fk_user_id BIGINT(20), 
         FOREIGN KEY (fk_user_id) REFERENCES tb_users(user_id)
         ) ENGINE=InnoDB DEFAULT CHARSET=utf8;');     
      
      
      
      //   TRANSACTIONS DETAILS
            
      $this->db->query('CREATE TABLE IF NOT EXISTS tb_transactions
         (
         transaction_id BIGINT(20) NOT NULL AUTO_INCREMENT,
         PRIMARY KEY(transaction_id),
         transaction_no VARCHAR(250),
         amount FLOAT(20,2),
         type VARCHAR(50),
         txn_date_time DATETIME,
         fk_user_id BIGINT(20),
         remarks VARCHAR(250),
         FOREIGN KEY (fk_user_id) REFERENCES tb_users(user_id)
         ) ENGINE=InnoDB DEFAULT CHARSET=utf8;');   
      
        
      //  RAZORPAY TRANSACTIONS DETAILS
            
      $this->db->query('CREATE TABLE IF NOT EXISTS tb_razor_transactions
         (
         razor_transaction_id BIGINT(20) NOT NULL AUTO_INCREMENT,
         PRIMARY KEY(razor_transaction_id),
         razorpay_order_id VARCHAR(250) UNIQUE,
         transaction_no VARCHAR(250),
         type VARCHAR(50),
         amount FLOAT(20,2)
         ) ENGINE=InnoDB DEFAULT CHARSET=utf8;');   
      
      
      
      //   USER TOKENS
            
      $this->db->query('CREATE TABLE IF NOT EXISTS tb_user_tokens
         (
         token_id BIGINT(20) NOT NULL AUTO_INCREMENT,
         PRIMARY KEY(token_id),
         reset_pass_code VARCHAR(20),
         reset_pass_code_created_at DATETIME,
         mob_ver_otp VARCHAR(20),
         mob_ver_otp_created_at DATETIME,
         user_token VARCHAR(250) UNIQUE,
         user_token_creatred_at DATETIME,
         fk_user_id BIGINT(20), 
         FOREIGN KEY (fk_user_id) REFERENCES tb_users(user_id)
         ) ENGINE=InnoDB DEFAULT CHARSET=utf8;');   

      
           
          // WEB USER LOGIN TABLE
         $this->db->query('CREATE TABLE IF NOT EXISTS tb_web_users
         (
         user_id BIGINT(20) NOT NULL AUTO_INCREMENT,
         PRIMARY KEY(user_id),
         username VARCHAR(250) UNIQUE,
         password VARCHAR(250),
         role VARCHAR(50) DEFAULT "editor",
         access_token VARCHAR(250) UNIQUE,
         created_at DATETIME,
         is_active INT(1)
         ) ENGINE=InnoDB DEFAULT CHARSET=utf8;');     
		
	
       //   WEB USER TOKENS

      $this->db->query('CREATE TABLE IF NOT EXISTS tb_web_user_tokens
         (
         token_id BIGINT(20) NOT NULL AUTO_INCREMENT,
         PRIMARY KEY(token_id),
         user_token VARCHAR(250) UNIQUE,
         user_token_creatred_at DATETIME,
         fk_user_id BIGINT(20), 
         FOREIGN KEY (fk_user_id) REFERENCES tb_web_users(user_id)
         ) ENGINE=InnoDB DEFAULT CHARSET=utf8;');   
      
	
      
      //   GAMES 
            
      $this->db->query('CREATE TABLE IF NOT EXISTS tb_games
         (
         game_id INT(20) NOT NULL AUTO_INCREMENT,
         PRIMARY KEY(game_id),
         game_name VARCHAR(250),
         fk_logo_id BIGINT(20),
         is_active INT(1)
         ) ENGINE=InnoDB DEFAULT CHARSET=utf8;');   
      
      
      // TOURNAMENTS
      
      $this->db->query('CREATE TABLE IF NOT EXISTS tb_tournaments
         (
         tournament_id INT(20) NOT NULL AUTO_INCREMENT,
         PRIMARY KEY(tournament_id),
         tournament_name VARCHAR(250),
         tournament_type INT(10),
         fk_game_id INT(20), 
         fk_logo_id BIGINT(20),
         FOREIGN KEY (fk_game_id) REFERENCES tb_games(game_id),
         FOREIGN KEY (fk_logo_id) REFERENCES tb_medias(media_id)
         ) ENGINE=InnoDB DEFAULT CHARSET=utf8;');   
       
      
       // MATCH LIST 
      
      $this->db->query('CREATE TABLE IF NOT EXISTS tb_match_list
         (
         match_id INT(20) NOT NULL AUTO_INCREMENT,
         PRIMARY KEY(match_id),
         match_name VARCHAR(250),
         date_and_time DATETIME,
         match_status INT(10) DEFAULT 0,
         fk_tournament_id INT(20),
         FOREIGN KEY (fk_tournament_id) REFERENCES tb_tournaments(tournament_id)
         ) ENGINE=InnoDB DEFAULT CHARSET=utf8;');  
      
      
           
      // TEAMS 
      
      $this->db->query('CREATE TABLE IF NOT EXISTS tb_teams
         (
         team_id INT(20) NOT NULL AUTO_INCREMENT,
         PRIMARY KEY(team_id),
         team_name VARCHAR(250),
         team_details VARCHAR(250),
         fk_logo_id BIGINT(20),
         FOREIGN KEY (fk_logo_id) REFERENCES tb_medias(media_id)
         ) ENGINE=InnoDB DEFAULT CHARSET=utf8;');  
      
       // TEAMS LIST 
      
      $this->db->query('CREATE TABLE IF NOT EXISTS tb_teams_list
         (
         teams_list_id INT(20) NOT NULL AUTO_INCREMENT,
         PRIMARY KEY(teams_list_id),
         fk_match_id INT(20),
         fk_team_id INT(20),
         team_position INT(20) DEFAULT 0,
         FOREIGN KEY (fk_match_id) REFERENCES tb_match_list(match_id),
         FOREIGN KEY (fk_team_id) REFERENCES tb_teams(team_id)
         ) ENGINE=InnoDB DEFAULT CHARSET=utf8;');  
      
     
      
        // PLAYER DETAILS 
      
      $this->db->query('CREATE TABLE IF NOT EXISTS tb_players
         (
         player_id INT(20) NOT NULL AUTO_INCREMENT,
         PRIMARY KEY(player_id),
         player_name VARCHAR(250),
         player_details VARCHAR(250),
         br_role VARCHAR(250),
         mp_role VARCHAR(250),
         fk_team_id INT(20),
         fk_logo_id BIGINT(20) DEFAULT 3,
         FOREIGN KEY (fk_team_id) REFERENCES tb_teams(team_id),
         FOREIGN KEY (fk_logo_id) REFERENCES tb_medias(media_id)
         ) ENGINE=InnoDB DEFAULT CHARSET=utf8;');  
      
    
        // TEAM PLAYER DETAILS 
      
      $this->db->query('CREATE TABLE IF NOT EXISTS tb_team_players
         (
         team_player_id BIGINT(20) NOT NULL AUTO_INCREMENT,
         PRIMARY KEY(team_player_id),
         player_credit FLOAT(20,1) DEFAULT 6,
         player_total_points INT(10) DEFAULT 0, 
         fk_player_id INT(20),
         fk_team_id INT(20),
         fk_tournament_id INT(20),
         FOREIGN KEY (fk_player_id) REFERENCES tb_players(player_id),
         FOREIGN KEY (fk_team_id) REFERENCES tb_teams(team_id),
         FOREIGN KEY (fk_tournament_id) REFERENCES tb_tournaments(tournament_id)
         ) ENGINE=InnoDB DEFAULT CHARSET=utf8;');  
      
            
       
       // FINAL TEAM LIST 
      
      $this->db->query('CREATE TABLE IF NOT EXISTS tb_final_team_list
         (
         final_team_list_id INT(20) NOT NULL AUTO_INCREMENT,
         PRIMARY KEY(final_team_list_id),
         fk_team_list_id INT(20),
         fk_team_player_id BIGINT(20),
         FOREIGN KEY (fk_team_list_id) REFERENCES tb_teams_list(teams_list_id),
         FOREIGN KEY (fk_team_player_id) REFERENCES tb_team_players(team_player_id)
         ) ENGINE=InnoDB DEFAULT CHARSET=utf8;');  
      
  
      
        // Battle Royale MATCH DETAILS 
      
      $this->db->query('CREATE TABLE IF NOT EXISTS tb_br_match_details
         (
         br_match_details_id BIGINT(20) NOT NULL AUTO_INCREMENT,
         PRIMARY KEY(br_match_details_id),
         kills INT(10) DEFAULT 0,
         knocks INT(10) DEFAULT 0,
         position INT(10) DEFAULT 0,
         points INT(10) DEFAULT 0,
         fk_team_player_id BIGINT(20),
         fk_match_id INT(20),
         FOREIGN KEY (fk_team_player_id) REFERENCES tb_team_players(team_player_id),
         FOREIGN KEY (fk_match_id) REFERENCES tb_match_list(match_id)
         ) ENGINE=InnoDB DEFAULT CHARSET=utf8;');  
      
      
      
      // Multi Player MATCH DETAILS 
      
      $this->db->query('CREATE TABLE IF NOT EXISTS tb_mp_match_details
         (
         mp_match_details_id BIGINT(20) NOT NULL AUTO_INCREMENT,
         PRIMARY KEY(mp_match_details_id),
         kills INT(10) DEFAULT 0,
         assists INT(10) DEFAULT 0,
         deaths INT(10) DEFAULT 0,
         win_lose VARCHAR(10),
         points INT(10) DEFAULT 0,
         fk_team_player_id BIGINT(20),
         fk_match_id INT(20),
         FOREIGN KEY (fk_team_player_id) REFERENCES tb_team_players(team_player_id),
         FOREIGN KEY (fk_match_id) REFERENCES tb_match_list(match_id)
         ) ENGINE=InnoDB DEFAULT CHARSET=utf8;');  
      
      // KILL FEED 
      
      $this->db->query('CREATE TABLE IF NOT EXISTS tb_kill_feeds
         (
         kill_feed_id BIGINT(20) NOT NULL AUTO_INCREMENT,
         PRIMARY KEY(kill_feed_id),
         fk_match_id INT(20),
         fk_team_player_id_1 BIGINT(20),
         fk_team_player_id_2 BIGINT(20),
         action VARCHAR(10),
         FOREIGN KEY (fk_team_player_id_1) REFERENCES tb_team_players(team_player_id),
         FOREIGN KEY (fk_team_player_id_2) REFERENCES tb_team_players(team_player_id),
         FOREIGN KEY (fk_match_id) REFERENCES tb_match_list(match_id)
         ) ENGINE=InnoDB DEFAULT CHARSET=utf8;');  
      
      
       
        // USER TEAM
      
      $this->db->query('CREATE TABLE IF NOT EXISTS tb_user_squads
         (
         user_squad_id BIGINT(20) NOT NULL AUTO_INCREMENT,
         PRIMARY KEY(user_squad_id),
         user_squad_no INT(20) DEFAULT 1,
         igl_team_player_id INT(20),
         fk_match_id INT(20),
         fk_user_id BIGINT(20),
         total_squad_points INT(20) DEFAULT 0,
         FOREIGN KEY (fk_match_id) REFERENCES tb_match_list(match_id),
         FOREIGN KEY (fk_user_id) REFERENCES tb_users(user_id)
         ) ENGINE=InnoDB DEFAULT CHARSET=utf8;');  
      
      
      // User Team Players
      
      $this->db->query('CREATE TABLE IF NOT EXISTS tb_user_squad_players
         (
         user_squad_player_id BIGINT(20) NOT NULL AUTO_INCREMENT,
         PRIMARY KEY(user_squad_player_id),
         my_player_id INT(20),
         my_player_points INT(20) DEFAULT 0,
         fk_user_squad_id BIGINT(20),
         FOREIGN KEY (fk_user_squad_id) REFERENCES tb_user_squads(user_squad_id)
         ) ENGINE=InnoDB DEFAULT CHARSET=utf8;');  
        
      
      // Pool Types
      
      $this->db->query('CREATE TABLE IF NOT EXISTS tb_pool_types
         (
         pool_type_id INT(20) NOT NULL AUTO_INCREMENT,
         PRIMARY KEY(pool_type_id),
         pool_type_name VARCHAR(20),
         total_amount INT(20),
         total_slots INT(20),
         reg_fee INT(20),
         win_percentage FLOAT(20,2) ,
         max_entry INT(20) DEFAULT 1
         ) ENGINE=InnoDB DEFAULT CHARSET=utf8;');  
      
      
       //prize distribution
      
      $this->db->query('CREATE TABLE IF NOT EXISTS tb_prize_distribution
         (
         prize_dist_id INT(20) NOT NULL AUTO_INCREMENT,
         PRIMARY KEY(prize_dist_id),
         fk_pool_type_id INT(20),
         start_slot INT(20),
         end_slot INT(20),
         amount FLOAT(20,2) DEFAULT 0,
         FOREIGN KEY (fk_pool_type_id) REFERENCES tb_pool_types(pool_type_id)
         ) ENGINE=InnoDB DEFAULT CHARSET=utf8;');  
      
      
      
      
        // PRIZE POOL LIST
      
      $this->db->query('CREATE TABLE IF NOT EXISTS tb_prize_pool_list
         (
         pool_id BIGINT(20) NOT NULL AUTO_INCREMENT,
         PRIMARY KEY(pool_id),
         reg_slots INT(20) DEFAULT 0,
         fk_match_id INT(20),
         fk_pool_type_id INT(20),
         FOREIGN KEY (fk_match_id) REFERENCES tb_match_list(match_id),
         FOREIGN KEY (fk_pool_type_id) REFERENCES tb_pool_types(pool_type_id)
         ) ENGINE=InnoDB DEFAULT CHARSET=utf8;');  
      
      
      //User Joined Pool
      
      $this->db->query('CREATE TABLE IF NOT EXISTS tb_user_join_pool
         (
         user_pool_id BIGINT(20) NOT NULL AUTO_INCREMENT,
         PRIMARY KEY(user_pool_id),
         fk_pool_id BIGINT(20),
         fk_user_squad_id BIGINT(20),
         rank INT(20),
         amount_won FLOAT(20,2) DEFAULT 0,
         FOREIGN KEY (fk_pool_id) REFERENCES tb_prize_pool_list(pool_id),
         FOREIGN KEY (fk_user_squad_id) REFERENCES tb_user_squads(user_squad_id)
         ) ENGINE=InnoDB DEFAULT CHARSET=utf8;');  
      
      
      //Live Stream Links
      
      $this->db->query('CREATE TABLE IF NOT EXISTS tb_streams
         (
         stream_id BIGINT(20) NOT NULL AUTO_INCREMENT,
         PRIMARY KEY(stream_id),
         fk_match_id INT(20),
         link VARCHAR(512),
         link_type VARCHAR(20),
         FOREIGN KEY (fk_match_id) REFERENCES tb_match_list(match_id)
         ) ENGINE=InnoDB DEFAULT CHARSET=utf8;');  
      
      
      //credit distribution

      $this->db->query('CREATE TABLE IF NOT EXISTS tb_credits_distribution
         (
         credit_id INT(20) NOT NULL AUTO_INCREMENT,
         PRIMARY KEY(credit_id),
         fk_game_id INT(20),
         credit_band_start FLOAT(10,2),
         credit_band_end FLOAT(10,2),
         points_req INT(20),
         total_points_start INT(20),
         total_points_end INT(20),
         tournament_type INT(20),
         FOREIGN KEY (fk_game_id) REFERENCES tb_games(game_id)
         ) ENGINE=InnoDB DEFAULT CHARSET=utf8;');  
      
      
        // PRIZE LIST
      /*
      $this->db->query('CREATE TABLE IF NOT EXISTS tb_prize
         (
         prize_id INT(20) NOT NULL AUTO_INCREMENT,
         PRIMARY KEY(prize_id),
         total_points INT(20),
         fk_pool_id INT(20),
         fk_user_team_id INT(20),
         FOREIGN KEY (fk_pool_id) REFERENCES tb_prize_pool_list(pool_id),
         FOREIGN KEY (fk_user_team_id) REFERENCES tb_user_teams(user_team_id)
         ) ENGINE=InnoDB DEFAULT CHARSET=utf8;');  
      */
      
      
      echo "Success";
      
      
      
      
      
	}
	
}