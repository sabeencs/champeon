<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Api_old extends CI_Controller
{
  
  public function __construct()
	{
		parent::__construct();
		$this->load->model('api_model');
		$this->load->library('form_validation');
    date_default_timezone_set('Asia/Kolkata');
	}

  

  
  
  //--------------------------- signup ----------------------------/// 

  
  public function signup()
	{
    $this->load->view("signup.php");
    
  }
  
  public function signup_validation()
	{
		$this->form_validation->set_rules('email', 'Email', 'required');
		$this->form_validation->set_rules('password', 'Password', 'required');
    $this->form_validation->set_rules('first_name', 'First Name', 'required');
		$this->form_validation->set_rules('username', 'Username', 'required');
 		$this->form_validation->set_rules('mobile', 'Mobile No', 'required');

		if($this->form_validation->run())
		{
			$data = array(
				'email'	=>	$this->input->post('email'),
				'password'		=>	$this->input->post('password'),
				'first_name'	=>	$this->input->post('first_name'),
				'last_name'	=>	$this->input->post('last_name'),
				'username'	=>	$this->input->post('username'),
				'mobile'	=>	$this->input->post('mobile'),
        'created_at' => date("Y-m-d H:i:s"),
        'is_active' => '1'
			);

			$this->api_model->signup_api($data);

			$array = array(
				'success'		=>	true
			);
		}
		else
		{
			$array = array(
				'error'					=>	true,
				'first_name_error'		=>	form_error('first_name'),
				'last_name_error'		=>	form_error('last_name'),
        'email'	=>	form_error('email'),
				'password'		=>	form_error('password'),
				'username'	=>	form_error('username'),
				'mobile'	=>	form_error('mobile')
			);
		}
		echo json_encode($array);
	}
	
  
    //--------------------------- login ----------------------------/// 

      function login()  
      {  
           //https://www.esports.envose.com/main/login  
          // $data['title'] = 'Sample Login Page';  
           $this->load->view('index2.html');  
      }  
   
 //----------------------------------------------------------------------------------/// 

      function login_validation()  
      {  
           $this->load->library('form_validation');  
           $this->form_validation->set_rules('username', 'Username', 'required');  
           $this->form_validation->set_rules('password', 'Password', 'required');  
           if($this->form_validation->run())  
           {  
                //true  
                $username = $this->input->post('username');  
                $password = $this->input->post('password');  
                //model function  
                $this->load->model('api_model');  
                if($this->api_model->can_login($username, $password))  
                {  
                     $session_data = array(  
                          'username'     =>     $username  
                     );  
                     $this->session->set_userdata($session_data);  
                     redirect(base_url() . 'api/enter');  
                }  
                else  
                {  
                     $this->session->set_flashdata('error', 'Invalid Username and Password');  
                     redirect(base_url() . 'api/login');  
                }  
           }  
           else  
           {  
                //false  
                $this->login();  
           }  
      }  
  

  //----------------------------------------------------------------------------------/// 
  
      function enter(){  
           if($this->session->userdata('username') != '')  
           {  
                echo '<h2>Welcome - '.$this->session->userdata('username').'</h2>';  
                echo '<label><a href="'.base_url().'api/logout">Logout</a></label>';  
           }  
           else  
           {  
                redirect(base_url() . 'api/login');  
           }  
      }  

  //----------------------------------------------------------------------------------/// 

      function logout()  
      {  
           $this->session->unset_userdata('username');  
           redirect(base_url() . 'web/v1/users/userLogin');  
//            redirect(base_url() . 'api/login');  
      }  
  
  //----------------------------------------------------------------------------------/// 

 function games(){
   $this->load->view('main_view.php');
 }
    //----------------------------------------------------------------------------------/// 
function upload(){
   $this->load->view('upload_view.php');
 }
    //----------------------------------------------------------------------------------/// 

 function peoples(){
   $this->load->view('main_view3.php');
 }
  
   //----------------------------------------------------------------------------------/// 

 function games_validation(){
   $game_name	=	$this->input->post('game_name');

   $games_count = $this->db->select('*')
                    ->from('tb_games')
                    ->where('game_name',$game_name)
    //                 ->get()->result_array();
                    ->count_all_results();   

   if( $games_count > 0 )
   {
       echo '<script>alert(" FAILED . . !! \n Game Name Already Exists..!!")</script>'; 
      $this->games();
   }
   else
   {

     $data = array(
				'game_name'		=>	$this->input->post('game_name')
			);
    
 		$this->db->insert('tb_games', $data);

    echo '<script>alert(" Success . . !! \n Game Inserted..!!")</script>'; 
    $this->games();

   }
 
 }
  
    //------------------------------------------------------------------------------------------------------------------///

  
  function fetch_games(){
    $games=$this->db->select('*')
                ->from('tb_games')
                ->get()->result_array();
   			 
  $output = '';

if(count($games) > 0)
{
  $i=1;
 foreach($games as $game_key => $game)
 {
  $output .= '
  <tr>
     <td><input type="text" name="sl_no" id="sl_no" class="form-control" value="'.$i.'" readonly/></td>
     <td><input type="text" name="game_id" id="game_id" class="form-control" value="'.$game['game_id'].'" readonly/></td>
     <td><input type="text" name="game_name" id="game_name" class="form-control" value="'.$game['game_name'].'" readonly/></td>
  </tr>
';
   $i++;
 }
}
else
{
 $output .= '
 <tr>
  <td colspan="4" align="center">No Data Found</td>
 </tr>
 ';
}

echo $output;
}
  
  
  
    //----------------------------------------------------------------------------------/// 
  
 function update_value(){

   $max_count	=	$this->input->post('sl_no');
   
   $peoples_details = array();   
   
   for($x=1 ; $x <= $max_count ; $x++)
   {
   $id	=	$this->input->post('id_'.$x);
   $value	=	$this->input->post('value_'.$x);
	 $new_value	=	$this->input->post('new_value_'.$x);
   $updated_value = $value + $new_value;
     
     
     $peoples_details[$x-1]['id'] = $id;
     $peoples_details[$x-1]['name'] = $this->input->post('name_'.$x);
     $peoples_details[$x-1]['value'] = $updated_value;

   }
   
   $this->db->update_batch('test_table_1', $peoples_details, 'id');
   
   $this->load->view('main_view3.php');

 }
  
  
  //------------------------------------------------------------------------------------------------------------------///

 function fetch_data(){
    $games=$this->db->select('*')
                ->from('tb_games')
                ->get()->result_array();
   			
// 		$response=array('status'=>false, 'message'=>'Failed','data'=>'');
    foreach($games as $game_key => $game)
    {
      echo "<tr>";
      echo "<td>" . $game['game_id'] . "</td>";
      echo "<td>" . $game['game_name'] . "</td>";
      echo "<td>" . 'Select Tournament' . "</td>";
      echo "<td>" . 'Select Tournament' . "</td>";
      echo "</tr>";
    }
//     header('Content-Type: application/json');
//     echo json_encode($games);
   
 }
  //------------------------------------------------------------------------------------------------------------------///

  
 function payments(){
   require_once(base_url().'/api/v1/config.php'); 
   $this->load->view('payment_view.php');
 }  //------------------------------------------------------------------------------------------------------------------///

  
 function fetch(){
   $this->load->view('fetch.php');
 }
    //------------------------------------------------------------------------------------------------------------------///

  
 function fetchTournaments(){
		
   $game_id = $this->input->post('game_id');
   
$tournaments = $this->db->select('*')
			->from('tb_tournaments')
  	  ->where('fk_game_id',$game_id)
			->get()->result_array();
        echo "<option value='"."'>".'Select Tournament'."</option>";  
        foreach($tournaments as $tournament_key => $tournament)
        {
            echo "<option value='". $tournament['tournament_id'] ."'>" .$tournament['tournament_id'] .' - '. $tournament['tournament_name'] ."</option>";  // displaying data in option menu
        }   
   
 } 
  
    //------------------------------------------------------------------------------------------------------------------///

 function fetchMatches(){
		
   $tournament_id = $this->input->post('tournament_id');
   
$matches = $this->db->select('*')
			->from('tb_match_list')
  	  ->where('fk_tournament_id',$tournament_id)
			->get()->result_array();
        echo "<option value='"."'>".'Select Match'."</option>";  
        foreach($matches as $match_key => $match)
        {
            echo "<option value='". $match['match_id'] ."'>" .$match['match_id'] .' - '. $match['match_name'] ."</option>";  // displaying data in option menu
        }   
   
 } 
  
    //------------------------------------------------------------------------------------------------------------------///

  function fetchTeams(){
		
   $match_id = $this->input->post('match_id');
   
$teams = $this->db->select(' team_id , team_name ')
			->from('tb_teams_list')
      ->join('tb_teams','tb_teams_list.fk_team_id = tb_teams.team_id','left')
  	  ->where('fk_match_id',$match_id)
			->get()->result_array();
                                                            
        echo "<option value='"."'>".'Select Team'."</option>";  
        foreach($teams as $team_key => $team)
        {
            echo "<option value='". $team['team_id'] ."'>" .$team['team_id'] .' - '. $team['team_name'] ."</option>";  // displaying data in option menu
        }   
   
 } 
    //------------------------------------------------------------------------------------------------------------------///

  function fetchPlayers(){
		
   $team_id = $this->input->post('team_id');
   $tournament_id = $this->input->post('tournament_id');
   
   $players = $this->db->select(' team_player_id , player_name , player_credit ')
			->from('tb_team_players')
      ->join('tb_players','tb_team_players.fk_player_id = tb_players.player_id','left')
  	  ->where('fk_team_id',$team_id)
  	  ->where('fk_tournament_id',$tournament_id)
			->get()->result_array();
                                                            
//         echo "<option value='"."'>".'Select Team'."</option>";  
        foreach($players as $player_key => $player)
        {
//             echo "<option value='". $player['team_player_id'] ."'>" .$player['player_name'] .' - '. $player['player_credit'] ."</option>";  // displaying data in option menu
          echo "<tr>";
          echo "<td>" . $player['team_player_id'] . "</td>";
          echo "<td>" . $player['player_name'] . "</td>";
          echo "<td>" . $player['player_credit'] . "</td>";
          echo "</tr>";
        }   
   
 } 
    //------------------------------------------------------------------------------------------------------------------///

  function fetch_data2(){
  
  
      $peoples=$this->db->select('*')
                ->from('test_table_1')
                ->get()->result_array();

//       $games=$this->db->select('*')
//                 ->from('games')
//                 ->get()->result_array();

//        <td>'.$people['game_id']  .'</td>
//    <td>'.$people['game_name'] .'</td>

    
$output = '';

if(count($peoples) > 0)
{
  $i=1;
 foreach($peoples as $peoples_key => $people)
 {
  $output .= '
  <tr>
     <td><input type="text" name="sl_no" id="sl_no" class="form-control" value="'.$i.'" readonly/></td>
     <td><input type="text" name="id_'.$i.'" id="id" class="form-control" value="'.$people['id'].'" readonly/></td>
     <td><input type="text" name="name_'.$i.'" id="name" class="form-control" value="'.$people['name'].'" readonly/></td>
     <td><input type="text" name="role_'.$i.'" id="role" class="form-control" value="'.$people['role'].'" readonly/></td>
     <td><input type="text" name="value_'.$i.'" id="value" class="form-control" value="'.$people['value'].'"/></td>
     <td><input type="text" name="new_value_'.$i.'" id="new_value" class="form-control" value="0"/></td>
  </tr>
';
   $i++;
 }
}
else
{
 $output .= '
 <tr>
  <td colspan="4" align="center">No Data Found</td>
 </tr>
 ';
}

echo $output;
}
  
     //------------------------------------------------------------------------------------------------------------------///
 
  function fetch_data3(){
  
   
   $team_id = $this->input->post('team_id');
   $tournament_id = $this->input->post('tournament_id');
   
    
   $players = $this->db->select(' team_player_id , player_name , player_credit ')
			->from('tb_team_players')
      ->join('tb_players','tb_team_players.fk_player_id = tb_players.player_id','left')
  	  ->where('fk_team_id',$team_id)
  	  ->where('fk_tournament_id',$tournament_id)
			->get()->result_array();
$output = '';

if(count($players) > 0)
{
 foreach($players as $player_key => $player)
 {
  $output .= '
  <tr>
   <td>'.$player['team_player_id']  .'</td>
   <td>'.$player['player_name'] .'</td>
   <td>'.$player['player_credit'] .'</td>
  </tr>
  ';
 }
}
else
{
 $output .= '
 <tr>
  <td colspan="4" align="center">No Data Found</td>
 </tr>
 ';
}

echo $output;
}
  
      //------------------------------------------------------------------------------------------------------------------///
 
  function fetch_header(){
  
    $tournament_id = $this->input->post('tournament_id');
   
//     $matchDetail = $this->db->select('tournament_type')
//         ->from('tb_match_list')
//         ->join('tb_tournaments','tb_match_list.fk_tournament_id = tb_tournaments.tournament_id','left')
//         ->where('match_id',$match_id)
//         ->get()->result_array();

    $matchDetail = $this->db->select('tournament_type')
        ->from('tb_tournaments')
        ->where('tournament_id',$tournament_id)
        ->get()->result_array();
    
    $tournament_type=$matchDetail[0]['tournament_type'];

    $output = '';

    if( $tournament_type == 6000 )//Battle Royale
    {
   $output .= '
     <tr>
      <td colspan="7" align="center"><input type="hidden" name="match_type" value="Battle Royale" readonly/></td>
     </tr>
     <tr>
       <th>Sl. No</th>
       <th>Player Id</th>
       <th>Player Name</th>
       <th>Kills</th>
       <th>New Kills</th>
       <th>Knocks</th>
       <th>New Knocks</th>
     </tr>
     ';
    }
    else//Multi player
    {
       $output .= '
     <tr>
      <td colspan="9" align="center"><input type="hidden" name="match_type" value="Multi Player" readonly/></td>
     </tr>
     <tr>
       <th>Sl. No</th>
       <th>Player Id</th>
       <th>Player Name</th>
       <th>Kills</th>
       <th>New Kills</th>
       <th>Assits</th>
       <th>New Assits</th>
       <th>Deaths</th>
       <th>New Deaths</th>
     </tr>
     ';
    }

    echo $output;

  }
      //------------------------------------------------------------------------------------------------------------------///
 
  function fetch_data4(){
  
   
   $match_id = $this->input->post('match_id');
   
    $matchDetail = $this->db->select('tournament_type')
        ->from('tb_match_list')
        ->join('tb_tournaments','tb_match_list.fk_tournament_id = tb_tournaments.tournament_id','left')
        ->where('match_id',$match_id)
        ->get()->result_array();
    
    $tournament_type=$matchDetail[0]['tournament_type'];

    if( $tournament_type == 6000 )
    {
      $players = $this->db->select(' br_match_details_id , player_name , kills , knocks ')
          ->from('tb_br_match_details')
          ->join('tb_team_players','tb_br_match_details.fk_team_player_id = tb_team_players.team_player_id','left')
          ->join('tb_players','tb_team_players.fk_player_id = tb_players.player_id','left')
          ->where('fk_match_id',$match_id)
    //   	  ->where('fk_tournament_id',$tournament_id)
          ->get()->result_array();
    $output = '';

    if(count($players) > 0)
    {
      $i=1;
     foreach($players as $player_key => $player)
     {
      $output .= '
      <tr>
       <td><input type="text" name="sl_no" id="sl_no" class="form-control" value="'.$i.'" readonly/></td>
       <td><input type="text" name="team_player_id_'.$i.'" id="team_player_id" class="form-control" value="'.$player['br_match_details_id'].'" readonly/></td>
       <td><input type="text" name="player_name_'.$i.'" id="player_name" class="form-control" value="'.$player['player_name'].'" readonly/></td>
       <td><input type="text" name="kills_'.$i.'" id="kills" class="form-control" value="'.$player['kills'].'" readonly/></td>
       <td><input type="text" name="new_kills_'.$i.'" id="new_kills" class="form-control" value="0"/></td>
       <td><input type="text" name="knocks_'.$i.'" id="knocks" class="form-control" value="'.$player['knocks'].'" readonly/></td>
       <td><input type="text" name="new_knocks_'.$i.'" id="new_knocks" class="form-control" value="0"/></td>
      </tr>
      ';
       $i++;
       }
    }
    else
    {
     $output .= '
     <tr>
      <td colspan="4" align="center">No Data Found</td>
     </tr>
     ';
    }
    }
    else
    {
      
       $players = $this->db->select(' mp_match_details_id , player_name , kills , assists , deaths ')
          ->from('tb_mp_match_details')
          ->join('tb_team_players','tb_mp_match_details.fk_team_player_id = tb_team_players.team_player_id','left')
          ->join('tb_players','tb_team_players.fk_player_id = tb_players.player_id','left')
          ->where('fk_match_id',$match_id)
    //   	  ->where('fk_tournament_id',$tournament_id)
          ->get()->result_array();
    $output = '';

    if(count($players) > 0)
    {
      $i=1;
     foreach($players as $player_key => $player)
     {
      $output .= '
      <tr>
       <td><input type="text" name="sl_no" id="sl_no" class="form-control" value="'.$i.'" readonly/></td>
       <td><input type="text" name="team_player_id_'.$i.'" id="team_player_id" class="form-control" value="'.$player['mp_match_details_id'].'" readonly/></td>
       <td><input type="text" name="player_name_'.$i.'" id="player_name" class="form-control" value="'.$player['player_name'].'" readonly/></td>
       <td><input type="text" name="kills_'.$i.'" id="kills" class="form-control" value="'.$player['kills'].'" readonly/></td>
       <td><input type="text" name="new_kills_'.$i.'" id="new_kills" class="form-control" value="0"/></td>
       <td><input type="text" name="assists_'.$i.'" id="assists" class="form-control" value="'.$player['assists'].'" readonly/></td>
       <td><input type="text" name="new_assists_'.$i.'" id="new_assists" class="form-control" value="0"/></td>
       <td><input type="text" name="deaths_'.$i.'" id="deaths" class="form-control" value="'.$player['deaths'].'" readonly/></td>
       <td><input type="text" name="new_deaths_'.$i.'" id="new_deaths" class="form-control" value="0"/></td>
      </tr>
      ';
       $i++;
       }
    }
    else
    {
     $output .= '
     <tr>
      <td colspan="4" align="center">No Data Found</td>
     </tr>
     ';
    }
  }
echo $output;
}

  
  //------------------------------------------------------------------------------------------------------------------///

  
public function update_kills()
	{
  
//      $location = base_url()."web/v1/api/games";
     $location = base_url()."api_old/fetch";

  $match_type = $this->input->post('match_type');
   $max_count	=	$this->input->post('sl_no');
   
  if($match_type == "Multi Player")
  {
//     echo "Multiplayer";
  $match_details = array();   
   
   for($x=1 ; $x <= $max_count ; $x++)
   {
     
//      echo $x;
     $mp_match_details_id	=	$this->input->post('team_player_id_'.$x);

     $kills	=	$this->input->post('kills_'.$x);
	   $new_kills	=	$this->input->post('new_kills_'.$x);
     $updated_kills = $kills + $new_kills;
     
     $assists	=	$this->input->post('assists_'.$x);
	   $new_assists	=	$this->input->post('new_assists_'.$x);
     $updated_assists = $assists + $new_assists;
     
     $deaths	=	$this->input->post('deaths_'.$x);
	   $new_deaths	=	$this->input->post('new_deaths_'.$x);
     $updated_deaths = $deaths + $new_deaths;

     $match_details[$x-1]['mp_match_details_id'] = $mp_match_details_id;
     $match_details[$x-1]['kills'] = $updated_kills;
     $match_details[$x-1]['assists'] = $updated_assists;
     $match_details[$x-1]['deaths'] = $updated_deaths;

   }
   
   $this->db->update_batch('tb_mp_match_details', $match_details, 'mp_match_details_id');

  }
  else
  {
//     echo "Battle Royale";
    $match_details = array();   
   
   for($x=1 ; $x <= $max_count ; $x++)
   {
     
//      echo $x;
     $br_match_details_id	=	$this->input->post('team_player_id_'.$x);

     $kills	=	$this->input->post('kills_'.$x);
	   $new_kills	=	$this->input->post('new_kills_'.$x);
     $updated_kills = $kills + $new_kills;
     
     $knocks	=	$this->input->post('knocks_'.$x);
	   $new_knocks	=	$this->input->post('new_knocks_'.$x);
     $updated_knocks = $knocks + $new_knocks;
     
//      $deaths	=	$this->input->post('deaths_'.$x);
// 	   $new_deaths	=	$this->input->post('new_deaths_'.$x);
//      $updated_deaths = $deaths + $new_deaths;

     $match_details[$x-1]['br_match_details_id'] = $br_match_details_id;
     $match_details[$x-1]['kills'] = $updated_kills;
     $match_details[$x-1]['knocks'] = $updated_knocks;
//      $match_details[$x-1]['deaths'] = $updated_deaths;

   }
   
   $this->db->update_batch('tb_br_match_details', $match_details, 'br_match_details_id');
}  

        echo '<script>alert(" Success . . !! \n Game Inserted..!!")</script>'; 
      echo '<META HTTP-EQUIV="Refresh" Content="0; URL='.$location.'">';

	}  
  
    //------------------------------------------------------------------------------------------------------------------///

  
public function update_mp_kills()
	{
  
  
   $max_count	=	$this->input->post('sl_no');
   
   $match_details = array();   
   
   for($x=1 ; $x <= $max_count ; $x++)
   {
     
//      echo $x;
     $mp_match_details_id	=	$this->input->post('team_player_id_'.$x);

     $kills	=	$this->input->post('kills_'.$x);
	   $new_kills	=	$this->input->post('new_kills_'.$x);
     $updated_kills = $kills + $new_kills;
     
     $assists	=	$this->input->post('assists_'.$x);
	   $new_assists	=	$this->input->post('new_assists_'.$x);
     $updated_assists = $assists + $new_assists;
     
     $deaths	=	$this->input->post('deaths_'.$x);
	   $new_deaths	=	$this->input->post('new_deaths_'.$x);
     $updated_deaths = $deaths + $new_deaths;

     $match_details[$x-1]['mp_match_details_id'] = $mp_match_details_id;
     $match_details[$x-1]['kills'] = $updated_kills;
     $match_details[$x-1]['assists'] = $updated_assists;
     $match_details[$x-1]['deaths'] = $updated_deaths;

   }
   
   $this->db->update_batch('tb_mp_match_details', $match_details, 'mp_match_details_id');

  $this->fetch();

  echo '<script>alert("Success..!!")</script>'; 
//   echo "Success..!!";
  
	}  
  
  
//--------------------------- get games ----------------------------/// 
	function ListGames(){
		$merged_result=array();
		$games=$this->db->select('*')
			->from('tb_games')
			->get()->result_array();
		
		$merged_result=$games;
			
		if($games){
			
		foreach($games as $game_key => $game){
				//print_r($game);
				$matches=$this->db->select('*')
					->from('tb_match_list')
			  ->join('tb_tournaments', 'tb_match_list.fk_tournament_id = tb_tournaments.tournament_id','left')
			  ->join('tb_medias','tb_tournaments.fk_logo_id = tb_medias.media_id','left')
			  ->where('tb_tournaments.fk_game_id',$game['game_id'])
					->get()->result_array();	
				
				$merged_result[$game_key]['match_list']=$matches;
				foreach($matches as $match_key => $match){
					
					
			$teams_list=$this->db->select('*')
			->from('tb_teams_list')
      ->join('tb_teams','tb_teams_list.fk_team_id = tb_teams.team_id','left')
      ->join('tb_medias','tb_teams.fk_logo_id = tb_medias.media_id','left')
			->where('tb_teams_list.fk_match_id',$match['match_id'])
			->get()->result_array();
					
					
					$merged_result[$game_key]['match_list'][$match_key]['team_list']=$teams_list;
				}
			
				
			
		}
	
		$response=array('status'=>true, 'message'=>'Success','data'=>$merged_result);
		 
			
			
		 		}
		else{
			
			$response=array('status'=>false, 'message'=>'Failed','data'=>'');
     
		}
		
    header('Content-Type: application/json');
    echo json_encode($response);
		
	}
  
  
  
	function get_games(){
		
		$games=$this->db->select('*')
			->from('tb_games')
			->get()->result_array();
		if($games){
			
		
		$response=array('status'=>true, 'message'=>'Success','data'=>$games);
		 
		 		}
		else{
			
			$response=array('status'=>false, 'message'=>'Failed','data'=>'');
     
		}
		
    header('Content-Type: application/json');
    echo json_encode($response);
		
	}
  
  //--------------------------- get games by id ----------------------------/// 

	function get_game_by_id($id){
		
		$games=$this->db->select('*')
			->from('tb_games')
			->where('game_id',$id)
			->get()->result_array();
    
		if($games)
    {
			$response=array('status'=>true, 'message'=>'Success','data'=>$games);
		}
    
		else
    {
			$response=array('status'=>false, 'message'=>'Failed','data'=>'');
		}
		
    header('Content-Type: application/json');
    echo json_encode($response);
	}	  
  

  
  //--------------------------- get matches by games id ----------------------------/// 

  
	function get_matches_by_game_id($id){
		
		$match_list=$this->db->select('*')
			->from('tb_match_list')
      ->join('tb_tournaments', 'tb_match_list.fk_tournament_id = tb_tournaments.tournament_id','left')
      ->join('tb_medias','tb_tournaments.fk_logo_id = tb_medias.media_id','left')
      ->where('tb_tournaments.fk_game_id',$id)
			->get()->result_array();
		if($match_list)
    {
			$response=array('status'=>true, 'message'=>'Success','data'=>$match_list);
		}
    
		else
    {
			$response=array('status'=>false, 'message'=>'Failed','data'=>'');
		}
		////return $match_list;
    header('Content-Type: application/json');
    echo json_encode($response);
	}	  
  
  
  //--------------------------- get teams list by match id ----------------------------/// 
  
	function get_teams_list_by_match_id($id){
		
		$teams_list=$this->db->select('*')
			->from('tb_teams_list')
      ->join('tb_teams','tb_teams_list.fk_team_id = tb_teams.team_id','left')
      ->join('tb_medias','tb_teams.fk_logo_id = tb_medias.media_id','left')
			->where('tb_teams_list.fk_match_id',$id)
			->get()->result_array();
		if($teams_list)
    {
			$response=array('status'=>true, 'message'=>'Success','data'=>$teams_list);
		}
    
		else
    {
			$response=array('status'=>false, 'message'=>'Failed','data'=>'');
		}
		////return $teams_list;
    header('Content-Type: application/json');
    echo json_encode($response);
	}	  
  

  
  //--------------------------- get final players by teams list id ----------------------------/// 
//'player_name','player_credit','team_name'
  function get_final_players_by_teams_list_id($id){
		
    
    $get_tournament_id = $this->db->select('fk_tournament_id')
			->from('tb_final_team_list')
      ->join('tb_teams_list','tb_final_team_list.fk_team_list_id = tb_teams_list.teams_list_id','left')
      //->join('tb_teams','tb_teams_list.fk_team_id = tb_teams.team_id','left')
      ->join('tb_match_list','tb_teams_list.fk_match_id = tb_match_list.match_id','left')
      ->where('tb_final_team_list.fk_team_list_id',$id)
		 // ->where('tb_match_list.fk_tournament_id','tb_team_players.fk_tournament_id')
	    ->get()->result_array(); 
		if($get_tournament_id)
    {
			$tournament_id=$get_tournament_id[1]["fk_tournament_id"];
 //     $response=array('status'=>true, 'message'=>'Success','data'=>$get_tournament_id);
		}
    
		else
    {
			$response=array('status'=>false, 'message'=>'Failed','data'=>'');
		}
    
     $get_team_name = $this->db->select('team_name,team_id')
			->from('tb_final_team_list')
      ->join('tb_teams_list','tb_final_team_list.fk_team_list_id = tb_teams_list.teams_list_id','left')
      ->join('tb_teams','tb_teams_list.fk_team_id = tb_teams.team_id','left')
      //->join('tb_match_list','tb_teams_list.fk_match_id = tb_match_list.match_id','left')
      ->where('tb_final_team_list.fk_team_list_id',$id)
		 // ->where('tb_match_list.fk_tournament_id','tb_team_players.fk_tournament_id')
	    ->get()->result_array(); 
		if($get_team_name)
    {
			$team_name=$get_team_name[1]["team_name"];
      $team_id = $get_team_name[1]["team_id"];
  //    $response=array('status'=>true, 'message'=>'Success','data'=>$get_team_name);
		}
    
		else
    {
			$response=array('status'=>false, 'message'=>'Failed','data'=>'');
		}
   
    
		$final_players_list=$this->db->select('team_player_id,player_id,player_name,player_credit')
			->from('tb_final_team_list')
      ->join('tb_players','tb_final_team_list.fk_player_id = tb_players.player_id','left')
      ->join('tb_team_players','tb_final_team_list.fk_player_id = tb_team_players.fk_player_id','left')
      ->where('tb_final_team_list.fk_team_list_id',$id)
      ->where('tb_team_players.fk_tournament_id',$tournament_id)
			->get()->result_array(); 
		if($final_players_list)
    {
			$response=array('status'=>true, 'message'=>'Success','team_name'=>$team_name,'team_id'=>$team_id,'data'=>$final_players_list);
		}
    
		else
    {
			$response=array('status'=>false, 'message'=>'Failed','data'=>'');
		}
		////return $response1;
    header('Content-Type: application/json');
    echo json_encode($response); 
    

    
	}	
  
  
      //----------------------------------------------------------------------------------/// 

  
  /*
  function get_full_game_details($id)
  {
    get_matches_by_game_id($id);
    foreach($match_list["match_id"] as $match_id)
    {
      get_teams_list_by_match_id($match_id);
      foreach($teams_list["teams_list_id"] as $final_team_id)
      {
        get_final_players_by_teams_list_id($final_team_id);
        
        $full_details[$id][$match_id][$final_team_id] = $response1;
      }
    }
  	echo json_encode($full_details); 
    }*/
  
  
  
  
    //--------------------------- inserting user teams ----------------------------/// 

  
  function insert_user_teams()
  {
     $user_id = 2;
     $match_id = 10; 
    // $this->db->where('fk_user_id', $user_id);  
             $this->db->where('fk_team_list_id', $user_id);  
             $this->db->where('fk_player_id',$match_id);  
     //$this->db->where('fk_match_id', $match_id);  
             $query = $this->db->get('tb_final_team_list');  
    // $query = $this->db->get('tb_user_teams');  
             //user = '$id1' AND match = '$id2'  
    $match_count = $query->num_rows(); 
    if($match_count > 0)  
     {  
            //$response=1;
            // $match_count = $query->num_rows;  
            // $match_count = num_rows();  
     }  
     else  
     {  
          //$response=0;
          //return false;       
     }  
    /*  
    $data = array(
				'user_team_no'	=>	$this->input->post('email'),
				'my_player_id'		=>	$this->input->post('password'),
				'my_player_points'	=>	$this->input->post('first_name'),
				'fk_match_id'	=>	$this->input->post('last_name'),
				'fk_user_id'	=>	$this->input->post('username'),
				);

			$this->api_model->insert_user_teams_api($data);

			$array = array(
				'success'		=>	true
			);
      */
      header('Content-Type: application/json');   
    echo json_encode($response); 
  
  }
  
  
  
  
  
}