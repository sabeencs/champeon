<?php defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/ImplementJwt.php';


class Users extends CI_Controller
{
  
  public function __construct()
	{
		parent::__construct();
		$this->load->model('api_model');
		$this->load->library('form_validation');
    $this->objOfJwt = new ImplementJwt();
    header('Content-Type: application/json');
    date_default_timezone_set('Asia/Kolkata');
  }

  
  //------------------------------------------------------------------------------------------------------------------///
  //-------------------------------    Get contents of Token Header      -----------------------------------------------------///
  //------------------------------------------------------------------------------------------------------------------///
  
  
    public function GetTokenData()
    {
    $received_Token = $this->input->request_headers('Authorization');
    $token_array = explode(" ",$received_Token['Authorization']);
      try
            {
            $jwtData = $this->objOfJwt->DecodeToken($token_array[1]);
        
//             $passkey="Ig2WdF605WGurIBnfhOigAxt4LemiQT3fS8XaXJlwPYhdDV7oZ4lX2jGMmqQRyHUmKb9kUDpNL6sIH9gJv6EUhaEN5HtCSxu0BlrvnM15QqjwN0z8AzckiLVbPKWdeJE";
            $decrypted_string=openssl_decrypt($jwtData['uId'],"AES-128-ECB",passkey);
            $jwtData['uId'] = (int)$decrypted_string;
        
//             $jwtData = $this->objOfJwt->DecodeToken($received_Token['Authorization']);
            //echo json_encode($jwtData);
            return($jwtData);
            }
            catch (Exception $e)
            {
            http_response_code('401');
            echo json_encode(array( "status" => false, "message" => $e->getMessage()));exit;
            }
    }
  
  //------------------------------------------------------------------------------------------------------------------///
  //-------------------------------    Get contents of Token       -----------------------------------------------------///
  //------------------------------------------------------------------------------------------------------------------///
  
    public function GetData()
    {
      $token_result = array(); 
      $token_result = $this->GetTokenData();

//       print_r($merged_result);
     
     if ($token_result['uId'] != 0)
       {
          $response=array('status'=>true, 'message'=>'Success', 'data'=>$token_result);
       }
      else
      {
          $response=array('status'=>false, 'message'=>$token_result[0]);
      }
      header('Content-Type: application/json');
      echo json_encode($response);
	  }
   
  
//------------------------------------------------------------------------------------------------------------------///
  //-------------------------------    Get Details of a user by user id       -----------------------------------------------------///
  //------------------------------------------------------------------------------------------------------------------///
  
  
  function getUserDetails()
  {
    
    $token_result = array(); 
    $token_result = $this->GetTokenData();

     
   if ($token_result['uId'] != 0)
     {

      $id = (int)$token_result['uId'];
		
     
     
      $user_details=$this->db->select('username , first_name as firstName , last_name as lastName , mobile as mobileNumber , email as userEmail , dob , gender , country as userCountry , state as userState , city as userCity , fk_user_dp_id as userDpId')
        ->from('tb_users')
        ->join('tb_user_details', 'tb_users.user_id = tb_user_details.fk_user_id',' left')
        ->where('tb_users.user_id',$id)
        ->get()->result_array();
     
     
     if($user_details[0]['gender'] == 1)
      {
        $user_details[0]['gender']='Male';
      }
      else if($user_details[0]['gender'] == 2)
      {
        $user_details[0]['gender']='Female';
      }
      else
      {
        $user_details[0]['gender']='Prefer not to say';
      }
     
      $media_list=$this->db->select('*')
       ->from('tb_medias')
       ->where('media_id',$user_details[0]['userDpId'])
       ->get()->result_array();
  
      $media_url = base_url().$media_list[0]['media_url'].'/'.$media_list[0]['media_name'].'_300x300'.$media_list[0]['media_ext'];

      $user_details[0]['userDpUrl']=$media_url;
     
      if($user_details)
      {
        $response=array('status'=>true, 'message'=>'Success','userDetails'=>$user_details[0]);
        
//         $this->viewProfilePic($user_details[0]['fk_user_dp_id']);
      }

      else
      {
        $response=array('status'=>false, 'message'=>'Failed','data'=>'');
      }
     
     }
     
    else
    {
        $response=array('status'=>false, 'message'=>$token_result[0]);
    }
		////return $match_list;
//     header('Content-Type: application/json');
		echo json_encode($response);
	}	  
  
  
  
  //------------------------------------------------------------------------------------------------------------------///
  //-------------------------------    create new User - Sign Up      -----------------------------------------------------///
  //------------------------------------------------------------------------------------------------------------------///
 
   
  public function createUser()
	{

  
    
//     $data = '{
//                   "userEmail": "samples1@samplemail.com",
//                   "password": "passwordd",
//                   "userMobile": "+919999999992",
//                   "firstName": "test",
//                   "lastName": "test"
//               }';

  
//     $input_data = json_decode($data , true);

                                                            //     $user_id = 1;
    $input_data = json_decode(trim(file_get_contents('php://input')), true);
                                                            //    $json_str = file_get_contents('php://input');
                                                            //     $json_str = file_get_contents('php://pastie.org/p/5gwT78JhW3Wm94w4tmB7rt');

                                                            # Get as an object
                                                            //     $json_obj = json_decode($json_str);
                                                            //      echo $data;
                                                            //     print_r($input_data);

    $email_id = $input_data['userEmail'] ;
    $password = $input_data['password'] ;
    $mobile_no = $input_data['userMobile'] ;
    $first_name = $input_data['firstName'] ;
    $last_name = $input_data['lastName'] ;
    $invited_by = $input_data['referralId'] ;
                                                          //     echo "\n match_id : ";
                                                          //     echo $match_id;
                                                          //     echo "\n";

                                                          //     $upload_squad_2 = array();        //for 2nd table data storage
                                                          //     $m = 0;                           //for 2nd table data storage
                                                          //     $max_squad_no=$this->db->select_max('user_squad_no')
    $email_used_count=$this->db->select('email')
      ->from('tb_users')
      ->where('email',$email_id)
      ->count_all_results();     
                                                          // 			->get()->result_array();
    $mobile_used_count=$this->db->select('mobile')
      ->from('tb_users')
      ->where('mobile',$mobile_no)
      ->count_all_results();     
    
    $id_no = 1;
    $user_name_count = 1;
    $referral_count = 1;
    if( $email_used_count > 0)
    {
 			$response=array('status'=>false, 'message'=>'Email ID already used');
    }
    elseif( $mobile_used_count > 0 )
    {
      $response=array('status'=>false, 'message'=>'Mobile Number already used');
    }
                                                              //     print_r($max_squad_no);
    else
    {
      
      $sub_names = array("_superSquad_", "_kingSquad_","_mightySquad_", "_playerSquad_", "_warriorSquad_", "_knightSquad_");

      // get random index from array $arrX
      $randIndex = array_rand($sub_names);

      // output the value for the random index
//       echo $sub_names[$randIndex];
      

      while( $user_name_count > 0)
      {

        $user_name = $first_name.$sub_names[$randIndex].$id_no;

                                                                //         echo "\n first_name : $first_name \n";

                                                                //         echo "\n user_name : ";
                                                                //         echo $user_name;
                                                                //         echo "\n";

        $user_name_count=$this->db->select('username')
          ->from('tb_users')
          ->where('username',$user_name)
          ->count_all_results();     

                                                                        //         echo "\n user_name_count : ";
                                                                        //         echo $user_name_count;
                                                                        //         echo "\n";

        if( $user_name_count == 0 )
        {
          break;
        }
        else
        {
          $id_no++;
        }
      }
      
      $upload_user_1 = array(
				'email'	=>	$email_id,
				'password'		=>	$password,
				'mobile'	=>	$mobile_no,
				'username'	=>	$user_name,
				'first_name'	=>	$first_name,
				'last_name'	=>	$last_name,
        'created_at' => date("Y-m-d H:i:s"),
        'is_active' => '1'
      );
    
		$this->db->insert('tb_users', $upload_user_1);

    $user_id=$this->db->select('user_id')
			->from('tb_users')
			->where('email',$email_id)
			->where('mobile',$mobile_no)
			->where('username',$user_name)
			->get()->result_array();

    $current_user_id = (int)$user_id[0]['user_id'];
      
    $user_string = substr($user_name,0,4);
    
     while( $referral_count > 0)
      {

       $referral_string = $this->random_strings(8);

       $my_referral_id = $user_string.$referral_string;
      
       $referral_count=$this->db->select('my_referral_id')
          ->from('tb_user_details')
          ->where('my_referral_id',$my_referral_id)
          ->count_all_results();     

        if( $referral_count == 0 )
        {
          break;
        }
      }  
      
   
      
    $upload_user_details_0 = array(
				'fk_user_id'	=>	$current_user_id,
				'my_referral_id'	=>	$my_referral_id,
				'invited_referral_id'	=>	$invited_by
      );

    $upload_user_details_1 = array(
      'fk_user_id'	=>	$current_user_id
    );

    $this->db->insert('tb_user_details', $upload_user_details_0);
  
    $this->db->insert('tb_bank', $upload_user_details_1);
    
    $this->db->insert('tb_user_wallet', $upload_user_details_1);
  
      
    $tokenData['uId'] = $current_user_id;
      
      
     $string_to_encrypt=$tokenData['uId'];
//      $passkey="Ig2WdF605WGurIBnfhOigAxt4LemiQT3fS8XaXJlwPYhdDV7oZ4lX2jGMmqQRyHUmKb9kUDpNL6sIH9gJv6EUhaEN5HtCSxu0BlrvnM15QqjwN0z8AzckiLVbPKWdeJE";
     $encrypted_string=openssl_encrypt($string_to_encrypt,"AES-128-ECB",passkey);
     $tokenData['uId'] = $encrypted_string; 

      
      
    $jwtToken = $this->objOfJwt->GenerateToken($tokenData);
                                                                      //     echo json_encode(array('Token'=>$jwtToken));
  
      
     $access_token = array(
        'access_token' => $jwtToken
    );
      
    $this->db->where('user_id', $current_user_id);
    $this->db->update('tb_users', $access_token); 
      
      
    $response=array('status'=>true, 'message'=>'User Created Successfully' , 'token'=>$jwtToken);
  
      
    }
    header('Content-Type: application/json');
    echo json_encode($response);
  
  }
  
  //------------------------------------------------------------------------------------------------------------------///
  //-------------------------------    Random strings      -----------------------------------------------------///
  //------------------------------------------------------------------------------------------------------------------///
 
  public function random_strings($length_of_string) 
{ 
  
    // String of all alphanumeric character 
    $str_result = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'; 
  
    // Shufle the $str_result and returns substring 
    // of specified length 
    return substr(str_shuffle($str_result),0, $length_of_string); 
//     echo substr(str_shuffle($str_result),0, $length_of_string); 
}
    
  //------------------------------------------------------------------------------------------------------------------///
  //-------------------------------    User Login - Sign In      -----------------------------------------------------///
  //------------------------------------------------------------------------------------------------------------------///
 
    
  public function userLogin()
	{
//     $data = '{
//               "status": true,
//               "message": "Success",
//               "userData": {
//                   "userEmail": "sample2@samplemail.com",
//                   "password": "passwordd",
//                   "userMobile": "+919999999994",
//                   "firstName": "test",
//                   "lastName": "test"
//                   }
//               }';
//     $data = '{
//                   "userEmail": "vikbaca@gmail.com",
//                   "password": "vikbaca"
//               }';

//     $input_data = json_decode($data , true);

                                                            //     $user_id = 1;
     $input_data = json_decode(trim(file_get_contents('php://input')), true);
                                                            //    $json_str = file_get_contents('php://input');
                                                            //     $json_str = file_get_contents('php://pastie.org/p/5gwT78JhW3Wm94w4tmB7rt');

                                                            # Get as an object
                                                            //     $json_obj = json_decode($json_str);
                                                              //      echo $data;
                                                              //     print_r($input_data);

                                                              //     $email_id = $input_data['userData']['userEmail'] ;
                                                              //     $password = $input_data['userData']['password'] ;
                                                              //     $mobile_no = $input_data['userData']['userMobile'] ;
                                                              //     $first_name = $input_data['userData']['firstName'] ;
                                                              //     $last_name = $input_data['userData']['lastName'] ;

    $email_id = $input_data['userEmail'] ;
    $password = $input_data['password'] ;
    
    if($email_id == null)
    {
      $response=array('status'=>false, 'message'=>'Login Failed' , 'Data'=>$input_data);
    }
    else
    {                                                  
                                                            //     $mobile_no = $input_data['userMobile'] ;
                                                            //     $first_name = $input_data['firstName'] ;
                                                            //     $last_name = $input_data['lastName'] ;
                                                            //     echo "\n match_id : ";
                                                            //     echo $match_id;
                                                            //     echo "\n";
      $email_used_count = 0;
                                                            //     $upload_squad_2 = array();        //for 2nd table data storage
                                                            //     $m = 0;                           //for 2nd table data storage
                                                            //     $max_squad_no=$this->db->select_max('user_squad_no')
      $email_used_count=$this->db->select('email')
        ->from('tb_users')
        ->where('email',$email_id)
        ->count_all_results();     
                                                            // 			->get()->result_array();
                                                            //     $tokens = array();
                                                            //     $id_no = 1;
                                                            //     $user_name_count = 1;
      if( $email_used_count == 0)
      {
        $response=array('status'=>false, 'message'=>'Email ID Not Found');
      }
                                                            //     elseif( $mobile_used_count > 0 )
                                                            //     {
                                                            //       $response=array('status'=>false, 'message'=>'Mobile Number already used');
                                                            //     }
                                                                //     print_r($max_squad_no);
      else
      {

        $user_details=$this->db->select(' user_id as uId , email as userEmail, username as userName, mobile as userMobile , first_name as firstName , last_name as lastName , wallet_balance as walletBalance , access_token as accessToken')
          ->from('tb_users')
          ->join('tb_user_wallet', 'tb_users.user_id = tb_user_wallet.fk_user_id','left')
          ->where('email',$email_id)
          ->where('password',$password)
          ->get()->result_array();
                                                                //         ->count_all_results();     
        $user_count = count($user_details,0);  


                                                                //         echo "\n user_name_count : ";
                                                                //         echo $user_name_count;
                                                                //         echo "\n";

          if( $user_count == 0 )
          {
            $response=array('status'=>false, 'message'=>'Invalid Password');
          }
          else
          {

            $user_details[0]['uId'] = (int)$user_details[0]['uId'];
            $current_user_id = $user_details[0]['uId'];

            $tokenData['uId'] = $current_user_id;
            $tokenData['iat'] = strtotime("now");                                    
            $tokenData['nbf'] = strtotime("now");                           
            $tokenData['exp'] = strtotime("+3600 Seconds");       
            
                  
            $string_to_encrypt=$tokenData['uId'];
//             $passkey="Ig2WdF605WGurIBnfhOigAxt4LemiQT3fS8XaXJlwPYhdDV7oZ4lX2jGMmqQRyHUmKb9kUDpNL6sIH9gJv6EUhaEN5HtCSxu0BlrvnM15QqjwN0z8AzckiLVbPKWdeJE";
            $encrypted_string=openssl_encrypt($string_to_encrypt,"AES-128-ECB",passkey);
            $tokenData['uId'] = $encrypted_string; 

            
            $jwtToken2 = $this->objOfJwt->GenerateToken($tokenData);
                                                                      //     echo json_encode(array('Token'=>$jwtToken));


             $user_token = array(
                'user_token' => $jwtToken2,
                'user_token_creatred_at' => date("Y-m-d H:i:s",$tokenData['iat']),
                'fk_user_id' => $current_user_id
            );
            $this->db->insert('tb_user_tokens', $user_token); 


            $user_details2['userEmail'] = $user_details[0]['userEmail'];
            $user_details2['userName'] = $user_details[0]['userName'];
            $user_details2['userMobile'] = $user_details[0]['userMobile'];
            $user_details2['firstName'] = $user_details[0]['firstName'];
            $user_details2['lastName'] = $user_details[0]['lastName'];
            $user_details2['walletBalance'] = (float)$user_details[0]['walletBalance'];

            $tokens['accessToken'] = $user_details[0]['accessToken'];
            $tokens['userToken'] = $jwtToken2;

            $response=array('status'=>true, 'message'=>'User Login Success' , 'userData'=>$user_details2 , 'tokens'=>$tokens);
          }
  
      }
    
    }
    header('Content-Type: application/json');
    echo json_encode($response);
  
  }
  
    
  //------------------------------------------------------------------------------------------------------------------///
    //-------------------------------    User Session Refresh      -----------------------------------------------------///
//------------------------------------------------------------------------------------------------------------------///

   function sessionRefresh()
   {
     $token_result = array();
     $token_result = $this->GetTokenData();

     $received_Token = $this->input->request_headers('Authorization');
//        print_r($received_Token);
     $token_array = explode(" ",$received_Token['Authorization']);
    
           
     
     if ($token_result['uId'] != 0)
       {
       
          $user_id = (int)$token_result['uId'];

          $token_in_db = $this->db->select('access_token')
            ->from('tb_users')
            ->where('user_id',$user_id)
            ->get()->result_array();  
          
//    print_r($token_in_db);    
          if($token_array[1] != $token_in_db[0]['access_token'] )   
          {
             $response=array('status'=>false, 'message'=>'Invalid Token');
          }
          else
          {
            
          $tokenData['uId'] = $user_id;
          $tokenData['iat'] = strtotime("now");                                    
          $tokenData['nbf'] = strtotime("now");                           
          $tokenData['exp'] = strtotime("+3600 Seconds");   
            
          $string_to_encrypt=$tokenData['uId'];
//           $passkey="Ig2WdF605WGurIBnfhOigAxt4LemiQT3fS8XaXJlwPYhdDV7oZ4lX2jGMmqQRyHUmKb9kUDpNL6sIH9gJv6EUhaEN5HtCSxu0BlrvnM15QqjwN0z8AzckiLVbPKWdeJE";
          $encrypted_string=openssl_encrypt($string_to_encrypt,"AES-128-ECB",passkey);
          $tokenData['uId'] = $encrypted_string; 

            
          $jwtToken2 = $this->objOfJwt->GenerateToken($tokenData);
//                                                                     //     echo json_encode(array('Token'=>$jwtToken));
           $user_token = array(
              'user_token' => $jwtToken2,
              'user_token_creatred_at' => date("Y-m-d H:i:s",$tokenData['iat']),
              'fk_user_id' => $user_id
          );
          $this->db->insert('tb_user_tokens', $user_token); 
            
          $tokens['accessToken'] = $token_in_db[0]['access_token'];
          $tokens['userToken'] = $jwtToken2;
            
          $response=array('status'=>true, 'message'=>'New Token Generated Successfully' , 'tokens'=>$tokens);
          }
       
       }
      else
      {
          $response=array('status'=>false, 'message'=>$token_result[0]);
      }
      header('Content-Type: application/json');
      echo json_encode($response);
	  }

  
    
  //------------------------------------------------------------------------------------------------------------------///
  //-------------------------------    User Profile Update      -----------------------------------------------------///
   //------------------------------------------------------------------------------------------------------------------///
 
  
  function userProfileUpdate()
   {
    
//     $data = '{
//                   "firstName": "qwerty",
//                   "lastName": "123",
//                   "dob": "1994-04-10",
//                   "gender": "Male",
//                   "country": "India",
//                   "state": "Kerala",
//                   "city": "Kochi"
//               }';


     $token_result = array();
     $token_result = $this->GetTokenData();
 
    if ($token_result['uId'] != 0)
    {
      $id2 = (int)$token_result['uId'];

      $input_data = json_decode(trim(file_get_contents('php://input')), true);

      $first_name = $input_data['firstName'] ;
      $last_name = $input_data['lastName'] ;
      $dob = $input_data['dob'] ;
      $gender = $input_data['gender'] ;
      $country = $input_data['country'] ;
      $state = $input_data['state'] ;
      $city = $input_data['city'] ;
      
      if($gender == 'Male')
      {
        $gender=1;
      }
      else if($gender == 'Female')
      {
        $gender=2;
      }
      else
      {
        $gender=0;
      }
      
      $user_profile_1 = array(
          'first_name' => $first_name,
          'last_name' => $last_name
      );

      $this->db->where('user_id', $id2);
      $this->db->update('tb_users', $user_profile_1); 
      
      $user_details_1 = array(
          'dob' => $dob,
          'gender' => $gender,
          'country' => $country,
          'state' => $state,
          'city' => $city
      );

      $this->db->where('fk_user_id', $id2);
      $this->db->update('tb_user_details', $user_details_1); 
      
      
      $response=array('status'=>true, 'message'=>'Profile Updated Succesfully');
      
    }

    else
    {
        $response=array('status'=>false, 'message'=>$token_result[0]);
    }
    header('Content-Type: application/json');
    echo json_encode($response);
  }
  
      
  //------------------------------------------------------------------------------------------------------------------///
  //-------------------------------    User Profile Pic Upload      -----------------------------------------------------///
   //------------------------------------------------------------------------------------------------------------------///
 
  
  function userProfilePicUpload()
  {
    $token_result = array();
    $token_result = $this->GetTokenData();
 
    if ($token_result['uId'] != 0)
    {
      $id2 = (int)$token_result['uId'];

     $this->upload_item("image",$id2);

    
    
    
     $response=array('status'=>true, 'message'=>'Profile Pic Updated Succesfully');
      
    }

    else
    {
        $response=array('status'=>false, 'message'=>$token_result[0]);
    }
    header('Content-Type: application/json');
    echo json_encode($response);
  }
  
  //------------------------------------------------------------------------------------------------------------------///
   //-------------------------------     Upload item      -----------------------------------------------------///
 //------------------------------------------------------------------------------------------------------------------///

  function upload_item($image_id,$user_id)
  {
    $this->load->library('image_lib');
    $this->load->helper('url');
    $current_year='./uploads/item-gallery/'.date('Y');
    $this_month='./uploads/item-gallery/'.date('Y').'/'.date('m');

      if(!is_dir($current_year)) //create the folder if it's not already exists
    {
          mkdir($current_year,0777,TRUE);
    }
      if(!is_dir($this_month)) //create the folder if it's not already exists
    {
          mkdir($this_month,0777,TRUE);
    }
    $config['upload_path'] = './uploads/item-gallery/'.date('Y').'/'.date('m');
    $config['allowed_types'] = 'gif|jpg|png|jpeg|JPEG';
    //$config['overwrite'] = 'TRUE';
    $config['encrypt_name'] = TRUE;
    $this->load->library('upload', $config);
    $this->upload->do_upload($image_id);
    //PHP
    //Configure upload.

    $store_image_link=$this->upload->data();

    $config['image_library'] = 'gd2';
    $config['source_image'] = $store_image_link['full_path'];
    $config['create_thumb'] = TRUE;
    $config['thumb_marker'] = '_300x300';
    $config['quality'] = '100%';
    $config['maintain_ratio'] = TRUE;
    $config['width'] = 300;
    $config['height'] = 300;

    $this->image_lib->initialize($config);
    $this->image_lib->resize();


    $logo_data=array('media_url'=>'uploads/item-gallery/'.date('Y').'/'.date('m'),'media_name'=>$store_image_link['raw_name'],
      'media_ext'=>$store_image_link['file_ext']
      );

    $this->db->insert('tb_medias',$logo_data);

    $uploaded_media_id = $this->db->select('media_id')
      ->from('tb_medias')
      ->where('media_name',$store_image_link['raw_name'])
      ->get()->result_array();    

    $current_media_id = $uploaded_media_id[0]['media_id'];

    $user_profile_pic = array(
        'fk_user_dp_id' => $current_media_id
      );

    $this->db->where('fk_user_id', $user_id);
    $this->db->update('tb_user_details', $user_profile_pic); 
          
  }
  
    //------------------------------------------------------------------------------------------------------------------///
  //-------------------------------    View Uploaded Profile Pic      -----------------------------------------------------///
  //------------------------------------------------------------------------------------------------------------------///
  function viewProfilePic($id)
  {
     $media_list=$this->db->select('*')
       ->from('tb_medias')
       ->where('media_id',$id)
       ->get()->result_array();
  
     $media_new = array();
     $media_new[0]['media_url'] = base_url().$media_list[0]['media_url'].'/'.$media_list[0]['media_name'].'_300x300'.$media_list[0]['media_ext'];

//      $image = $media_new[0]['media_url'];
//      $imageData = base64_encode(file_get_contents($image));
//      echo '<img src="data:image/jpeg;base64,'.$imageData.'">';
  
  }
    
  //------------------------------------------------------------------------------------------------------------------///
  //-------------------------------    User Bank Update      -----------------------------------------------------///
   //------------------------------------------------------------------------------------------------------------------///
 
  
  function userBankUpdate()
   {
    
//     $data = '{
//                   "fullName": "Sabeen CS",
//                   "accountNumber": "123123123123",
//                   "ifscCode": "SBIN00000",
//                   "bankName": "State Bank Of India"
//               }';


     $token_result = array();
     $token_result = $this->GetTokenData();
 
    if ($token_result['uId'] != 0)
    {
      $id2 = (int)$token_result['uId'];

      $input_data = json_decode(trim(file_get_contents('php://input')), true);

      $full_name = $input_data['fullName'] ;
      $bank_account_no = $input_data['accountNumber'] ;
      $ifsc_code = $input_data['ifscCode'] ;
      $bank_name = $input_data['bankName'] ;
      
     
      
      $user_bank = array(
          'full_name' => $full_name,
          'bank_account_no' => $bank_account_no,
          'ifsc_code' => $ifsc_code,
          'bank_name' => $bank_name
      );

      $this->db->where('fk_user_id', $id2);
      $this->db->update('tb_bank', $user_bank); 
      
      
      $response=array('status'=>true, 'message'=>'Bank Details Updated Succesfully');
      
    }

    else
    {
        $response=array('status'=>false, 'message'=>$token_result[0]);
    }
    header('Content-Type: application/json');
    echo json_encode($response);
  }
  
   
   
   //------------------------------------------------------------------------------------------------------------------///
   //-------------------------------    Get User Wallet Details      -----------------------------------------------------///
   //------------------------------------------------------------------------------------------------------------------///
 
  function getWalletUpdation()
  {
    
    $token_result = array(); 
    $token_result = $this->GetTokenData();
    
//     print_r($token_result);
    if ($token_result['uId'] != 0)
     {

      $id2 = (int)$token_result['uId'];

      $contests=array();

      $user_wallet=$this->db->select(' wallet_balance , total_winnings')
        ->from('tb_user_wallet')
        ->where('fk_user_id',$id2)
        ->get()->result_array();
//         ->count_all_results();   
    
      $wallet_balance = (float)$user_wallet[0]['wallet_balance'];
      $total_winnings = (float)$user_wallet[0]['total_winnings'];
      
      if( $wallet_balance >= 1000 )
      {
        $is_withdrawable = true;
        $withdraw_msg = "Amount is above 1000";
      }
      else
      {
        $is_withdrawable = false;
        $withdraw_msg = "Amount is below 1000";
      }
      
      $user_bank=$this->db->select(' full_name , bank_account_no , ifsc_code , bank_name ')
        ->from('tb_bank')
        ->where('fk_user_id',$id2)
        ->get()->result_array();
      
      if( ($user_bank[0]['full_name'] != null) && ($user_bank[0]['bank_account_no'] != null) && ($user_bank[0]['ifsc_code'] != null) && ($user_bank[0]['bank_name'] != null) )
      {
        $is_bank_added = true;
      }
      else
      {
        $is_bank_added = false;
      }
      
      if($user_wallet)
      {
         $response=array('status'=>true, 'message'=>'Success' , 'walletBalance'=>$wallet_balance , 'totalWinnings'=>$total_winnings , 'isWithdrawable'=>$is_withdrawable , 'withdrawMessage'=>$withdraw_msg , 'isBankAccountAdded'=>$is_bank_added );
      }
      else
      {
        $response=array('status'=>false, 'message'=>'Failed to Fetch Data','data'=>'');
      }
          
    }
       
       
    else
    {
        $response=array('status'=>false, 'message'=>$token_result[0]);
    }
  
    header('Content-Type: application/json');
    echo json_encode($response);
  }
  

   //------------------------------------------------------------------------------------------------------------------///
 //-------------------------------    Get 20 Recent Transaction Details      ----------------------------------------------///
 //------------------------------------------------------------------------------------------------------------------///
 
 function getRecentTransactions()
  {
    
    $token_result = array(); 
    $token_result = $this->GetTokenData();

     
   if ($token_result['uId'] != 0)
     {

      $id = (int)$token_result['uId'];
		
     
     
      $transaction_details_unslice=$this->db->select('transaction_no as transactionId , amount as transactionAmount , txn_date_time as transactionDate , type as transactionType , remarks as transactionRemarks')
        ->from('tb_transactions')
        ->where('fk_user_id',$id)
        ->order_by('txn_date_time', 'DESC')
        ->get()->result_array();
     
      $transaction_details = array_slice($transaction_details_unslice, 0, 20);

      $count_value = count($transaction_details,0);  


      for($i=0 ; $i < $count_value ; $i++)
      {

        if($transaction_details[$i]['transactionType'] == "Bank Payment")
        {
          $media_url_id = 29;
        }
        else if($transaction_details[$i]['transactionType'] == "Bank Withdraw")
        {
          $media_url_id = 30;
        }
        else if($transaction_details[$i]['transactionType'] == "Wallet Credit")
        {
          $media_url_id = 31;
        }
        else//Wallet Debit
        {
          $media_url_id = 32;
        }

        $media_list=$this->db->select('*')
         ->from('tb_medias')
         ->where('media_id',$media_url_id)
         ->get()->result_array();

        $media_url = base_url().$media_list[0]['media_url'].'/'.$media_list[0]['media_name'].'_300x300'.$media_list[0]['media_ext'];

        $transaction_details[$i]['iconUrl'] = $media_url;
        $transaction_details[$i]['transactionAmount'] = (float)$transaction_details[$i]['transactionAmount'];

      }
     
      if($count_value > 0)
      {
        $response=array('status'=>true, 'message'=>'Success','recentTransactions'=>$transaction_details);
        
//         $this->viewProfilePic($user_details[0]['fk_user_dp_id']);
      }

      else
      {
        $response=array('status'=>false, 'message'=>'Failed','recentTransactions'=>'');
      }
     
     }
     
    else
    {
        $response=array('status'=>false, 'message'=>$token_result[0]);
    }
		////return $match_list;
    header('Content-Type: application/json');
		echo json_encode($response);
	}	  
  
  
  //------------------------------------------------------------------------------------------------------------------///
   //-------------------------------    User Profile Update      -----------------------------------------------------///
   //------------------------------------------------------------------------------------------------------------------///

  
  
  
  
  
  
  
  
}