<?php defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/ImplementJwt.php';

class Games extends CI_Controller
{
  
  public function __construct()
	{
		parent::__construct();
		$this->load->model('api_model');
		$this->load->library('form_validation');
    $this->objOfJwt = new ImplementJwt();
    header('Content-Type: application/json');
    date_default_timezone_set('Asia/Kolkata');
	}

  
  
  
  
  //------------------------------------------------------------------------------------------------------------------///
  //-------------------------------    Get contents of Token Header      -----------------------------------------------------///
  //------------------------------------------------------------------------------------------------------------------///
  
  
    public function GetTokenData()
    {
    $received_Token = $this->input->request_headers('Authorization');
    $token_array = explode(" ",$received_Token['Authorization']);
      try
            {
            $jwtData = $this->objOfJwt->DecodeToken($token_array[1]);
        
            $decrypted_string=openssl_decrypt($jwtData['uId'],"AES-128-ECB",passkey);
            $jwtData['uId'] = (int)$decrypted_string;
        
//             $jwtData = $this->objOfJwt->DecodeToken($received_Token['Authorization']);
            //echo json_encode($jwtData);
            return($jwtData);
            }
            catch (Exception $e)
            {
            http_response_code('401');
            echo json_encode(array( "status" => false, "message" => $e->getMessage()));exit;
            }
    }
  
  //------------------------------------------------------------------------------------------------------------------///
  //-------------------------------    Get contents of Token       -----------------------------------------------------///
  //------------------------------------------------------------------------------------------------------------------///
  
    public function GetData()
    {
      $token_result = array(); 
      $token_result = $this->GetTokenData();

     
     if ($token_result['uId'] != 0)
       {
          $response=array('status'=>true, 'message'=>'Success', 'data'=>$token_result);
       }
      else
      {
          $response=array('status'=>false, 'message'=>$token_result[0]);
      }
      header('Content-Type: application/json');
      echo json_encode($response);
	  }
   
  
  
  
  
  //------------------------------------------------------------------------------------------------------------------///
  //--------------------------- 1. get list of user joined matches w.r.t games ----------------------------///
  //--------------------------- 2. get list of upcoming matches w.r.t games ----------------------------///
  // $id1 = user_id 
   //------------------------------------------------------------------------------------------------------------------///

  
  function listGames()
  {
    
      $token_result = array(); 
      $token_result = $this->GetTokenData();

     
     if ($token_result['uId'] != 0)
       {
          
        $id1 = (int)$token_result['uId'];

        $merged_result=array();
        $user_matches=array();

        $user_matches_unslice=$this->db->select(' match_id as matchId , match_name as matchName , match_status as matchStatus , date_and_time as matchTime, tournament_name as tournamentName , tournament_id as tournamentId , tournament_type as matchType , media_url as mediaUrl , media_name as mediaName , media_ext as mediaExt ')
          ->from('tb_user_squads')
          ->join('tb_user_join_pool', 'tb_user_squads.user_squad_id = tb_user_join_pool.fk_user_squad_id','left')
          ->join('tb_prize_pool_list','tb_user_join_pool.fk_pool_id = tb_prize_pool_list.pool_id','left')
          ->join('tb_match_list','tb_prize_pool_list.fk_match_id = tb_match_list.match_id','left')
          ->join('tb_tournaments', 'tb_match_list.fk_tournament_id = tb_tournaments.tournament_id','left')
          ->join('tb_medias','tb_tournaments.fk_logo_id = tb_medias.media_id','left')
          ->where('tb_user_squads.fk_user_id',$id1)
          ->where('tb_match_list.match_status >=',0)
          ->order_by('date_and_time', 'DESC')
          ->group_by('match_id')
          ->get()->result_array();	

       

          $user_matches= array_slice($user_matches_unslice, 0, 10);

            $count_value = count($user_matches,0);  


            for($i=0 ; $i < $count_value ; $i++)
            {

            $user_matches[$i]['matchId'] = (int)$user_matches[$i]['matchId'];
            $user_matches[$i]['matchStatus'] = (int)$user_matches[$i]['matchStatus'];
            $user_matches[$i]['matchType'] = (int)$user_matches[$i]['matchType'];
            $user_matches[$i]['tournamentId'] = (int)$user_matches[$i]['tournamentId'];
              if($user_matches[$i]['matchType'] == 6000)
              {
              $user_matches[$i]['matchLogo'] = base_url().$user_matches[$i]['mediaUrl'].'/'.$user_matches[$i]['mediaName'].'_300x300'.$user_matches[$i]['mediaExt'];
              }
              else
              {
                $team_details=$this->db->select(' team_id as teamId , team_name as teamName , media_url as mediaUrl , media_name as mediaName , media_ext as mediaExt')
                  ->from('tb_teams_list')
                  ->join('tb_teams', 'tb_teams_list.fk_team_id = tb_teams.team_id','left')
                  ->join('tb_medias','tb_teams.fk_logo_id = tb_medias.media_id','left')
                  ->where('tb_teams_list.fk_match_id',$user_matches[$i]['matchId'])
                  ->get()->result_array();	
                
                $user_matches[$i]['teamOne'] = $team_details[0]['teamName'];
                $user_matches[$i]['matchLogo'] = base_url().$team_details[0]['mediaUrl'].'/'.$team_details[0]['mediaName'].'_300x300'.$team_details[0]['mediaExt'];
                $user_matches[$i]['teamTwo'] = $team_details[1]['teamName'];
                $user_matches[$i]['matchLogo2'] = base_url().$team_details[1]['mediaUrl'].'/'.$team_details[1]['mediaName'].'_300x300'.$team_details[1]['mediaExt'];
                
              }

            }

        $games=$this->db->select('game_id as gameId , game_name as gameName')
          ->from('tb_games')
    			->where('is_active',1)
          ->get()->result_array();

        $merged_result=$games;

    //      print_r($games);

    //     $games['gameId'] = array_map('intval', $games['gameId']);

        if($games)
        {

        foreach($games as $game_key => $game)
          {
                                              /*
                                                $user_matches=$this->db->select(' match_id , match_name , match_status , date_and_time , tournament_name , tournament_type , media_url ')
                                                ->from('tb_user_squads')
                                                ->join('tb_match_list','tb_user_squads.fk_match_id = tb_match_list.match_id','left')
                                                ->join('tb_tournaments', 'tb_match_list.fk_tournament_id = tb_tournaments.tournament_id','left')
                                                ->join('tb_medias','tb_tournaments.fk_logo_id = tb_medias.media_id','left')
                                                ->where('tb_user_squads.fk_user_id',$id1)
                                                ->where('tb_tournaments.fk_game_id',$game['game_id'])
                                                ->group_by('match_id')
                                                ->get()->result_array();	

                                                $merged_result[$game_key]['match_list_1']=$user_matches;


                                                $user_matches=$this->db->select(' match_id , match_name , match_status , date_and_time , tournament_name , tournament_type , media_url ')
                                                ->from('tb_user_squads')
                                                ->join('tb_user_join_pool', 'tb_user_squads.user_squad_id = tb_user_join_pool.fk_user_squad_id','left')
                                                ->join('tb_prize_pool_list','tb_user_join_pool.fk_pool_id = tb_prize_pool_list.pool_id','left')
                                                ->join('tb_match_list','tb_prize_pool_list.fk_match_id = tb_match_list.match_id','left')
                                                ->join('tb_tournaments', 'tb_match_list.fk_tournament_id = tb_tournaments.tournament_id','left')
                                                ->join('tb_medias','tb_tournaments.fk_logo_id = tb_medias.media_id','left')
                                                ->where('tb_user_squads.fk_user_id',$id1)
                                                ->where('tb_tournaments.fk_game_id',$game['game_id'])
                                                ->group_by('match_id')
                                                ->get()->result_array();	

                                                $merged_result[$game_key]['match_list_1']=$user_matches;
                                            */  

                                            //print_r($game);
            $matches_unslice=$this->db->select(' match_id as matchId , match_name as matchName , match_status as matchStatus , date_and_time as matchTime, tournament_name as tournamentName , tournament_id as tournamentId , tournament_type as matchType , media_url as mediaUrl , media_name as mediaName , media_ext as mediaExt')
            ->from('tb_match_list')
            ->join('tb_tournaments', 'tb_match_list.fk_tournament_id = tb_tournaments.tournament_id','left')
            ->join('tb_medias','tb_tournaments.fk_logo_id = tb_medias.media_id','left')
            ->where('tb_match_list.match_status',0)
            ->order_by('date_and_time', 'ASC')
            ->where('tb_tournaments.fk_game_id',$game['gameId'])
            ->get()->result_array();	

           $matches = array_slice($matches_unslice, 0, 15);


            $count_value = count($matches,0);  


            for($i=0 ; $i < $count_value ; $i++)
            {

            $matches[$i]['matchId'] = (int)$matches[$i]['matchId'];
            $matches[$i]['matchStatus'] = (int)$matches[$i]['matchStatus'];
            $matches[$i]['matchType'] = (int)$matches[$i]['matchType'];
            $matches[$i]['tournamentId'] = (int)$matches[$i]['tournamentId'];
              
              if($matches[$i]['matchType'] == 6000)
              {
              $matches[$i]['matchLogo'] = base_url().$matches[$i]['mediaUrl'].'/'.$matches[$i]['mediaName'].'_300x300'.$matches[$i]['mediaExt'];
              }
              else
              {
                $team_details_1=$this->db->select(' team_id as teamId , team_name as teamName , media_url as mediaUrl , media_name as mediaName , media_ext as mediaExt')
                  ->from('tb_teams_list')
                  ->join('tb_teams', 'tb_teams_list.fk_team_id = tb_teams.team_id','left')
                  ->join('tb_medias','tb_teams.fk_logo_id = tb_medias.media_id','left')
                  ->where('tb_teams_list.fk_match_id',$matches[$i]['matchId'])
                  ->get()->result_array();	
                
                if($team_details_1)
                {
                  $matches[$i]['teamOne'] = $team_details_1[0]['teamName'];
                  $matches[$i]['matchLogo'] = base_url().$team_details_1[0]['mediaUrl'].'/'.$team_details_1[0]['mediaName'].'_300x300'.$team_details_1[0]['mediaExt'];
                  $matches[$i]['teamTwo'] = $team_details_1[1]['teamName'];
                  $matches[$i]['matchLogo2'] = base_url().$team_details_1[1]['mediaUrl'].'/'.$team_details_1[1]['mediaName'].'_300x300'.$team_details_1[1]['mediaExt'];
                }                
                
              }

            }
            $merged_result[$game_key]['gameId'] = (int) $merged_result[$game_key]['gameId'];

            $merged_result[$game_key]['matchesList']=$matches;

          }

        $response=array('status'=>true, 'message'=>'Success','userMatches'=>$user_matches, 'gamesData'=>$merged_result);
        }
        else
        {
          $response=array('status'=>false, 'message'=>'Failed','data'=>'');
        }

      }
     
      else
      {
          $response=array('status'=>false, 'message'=>$token_result[0]);
      }
    header('Content-Type: application/json');
		echo json_encode($response);
	}
  
 
  
  
  //------------------------------------------------------------------------------------------------------------------///
  //--------------------------- get final players by match id ----------------------------///
  // $id = match_id   id2 = user_id
    //------------------------------------------------------------------------------------------------------------------///

//   function getMatchDetails($id,$id2)
  function getMatchDetails($id)
  {
    
    $token_result = array(); 
      $token_result = $this->GetTokenData();

     
     if ($token_result['uId'] != 0)
       {
          
        $id2 = (int)$token_result['uId'];

            $merged_result=array();
            $merged_result2=array();
            $merged_result3=array();
            $pool_details=array();

            $merged_result2 = $this->squadsBeforeMatch($id,$id2);

            $matchDetail = $this->db->select('*')
              ->from('tb_match_list')
              ->where('match_id',$id)
              ->get()->result_array();

           $pool_details=$this->db->select_max('total_amount')
              ->from('tb_prize_pool_list')
              ->join('tb_pool_types','tb_prize_pool_list.fk_pool_type_id = tb_pool_types.pool_type_id','left')
              ->where('fk_match_id',$id)
              ->get()->result_array();   
       
              if($matchDetail)
              {
                                                    //  $response=array('status'=>true, 'message'=>'Success','data'=>'');
                $match_name=$matchDetail[0]["match_name"];
                $date_and_time = $matchDetail[0]["date_and_time"];
                $tournament_id = (int)$matchDetail[0]["fk_tournament_id"];
                $match_status = (int)$matchDetail[0]["match_status"];
                $mega_prize = (int)$pool_details[0]["total_amount"];
              }

              else
              {
                $response=array('status'=>false, 'message'=>'Failed','data'=>'');
              }

       
            $tournament_details = $this->db->select(' tournament_name , tournament_type')
              ->from('tb_tournaments')
              ->where('tournament_id',$tournament_id)
              ->get()->result_array();

            if($tournament_details)
              {
                                                 //  $response=array('status'=>true, 'message'=>'Success','data'=>'');
                $tournament_name=$tournament_details[0]["tournament_name"];
                $tournament_type=(int)$tournament_details[0]["tournament_type"];
              }


            if($tournament_type == 6000) // Battle Royale
            {
              $merged_result3['totalCount'] = 4;
              $merged_result3['selectedCount'] = 0;
              $merged_result4['totalCredits'] = 35;
              $merged_result4['usedCredits'] = 0;
            }
            else //($tournament_type == 7000) // Multi Player
            {
              $merged_result3['totalCount'] = 5;
              $merged_result3['selectedCount'] = 0;
              $merged_result4['totalCredits'] = 45;
              $merged_result4['usedCredits'] = 0;
            }




            $teams_list=$this->db->select(' teams_list_id as teamsListId , team_id as teamId , team_name as teamName ,  media_url as mediaUrl , media_name as mediaName , media_ext as mediaExt ')
              ->from('tb_teams_list')
              ->join('tb_teams','tb_teams_list.fk_team_id = tb_teams.team_id','left')
              ->join('tb_medias','tb_teams.fk_logo_id = tb_medias.media_id','left')
              ->where('tb_teams_list.fk_match_id',$id)
              ->get()->result_array();

       
       
            $merged_result=$teams_list;

                                                  //if($tournament_type == )

            if($teams_list)
            {
              foreach($teams_list as $team_key => $team_list)
              {

//                 $teams_list[$team_key]['teamLogo'] = base_url().$teams_list[$team_key]['mediaUrl'].'/'.$teams_list[$team_key]['mediaName'].'_300x300'.$teams_list[$team_key]['mediaExt'];

                
                
              $final_players_list=$this->db->select('team_player_id as teamPlayerId , player_id as playerId, player_name as playerName, br_role as brRole, mp_role as mpRole, player_credit as playerCredit , player_total_points as playerTotalPoints ,  media_url as playerMediaUrl , media_name as playerMediaName , media_ext as playerMediaExt')
                ->from('tb_final_team_list')
                ->join('tb_team_players','tb_final_team_list.fk_team_player_id = tb_team_players.team_player_id','left')
                ->join('tb_players','tb_team_players.fk_player_id = tb_players.player_id','left')
                ->join('tb_medias','tb_players.fk_logo_id = tb_medias.media_id','left')
                ->where('tb_final_team_list.fk_team_list_id',$team_list['teamsListId'])
                ->where('tb_team_players.fk_tournament_id',$tournament_id)
                ->get()->result_array(); 


               $count_value = count($final_players_list,0);  

                for($i = 0 ; $i < $count_value ; $i++)
                {
                   $final_players_list[$i]['teamName'] = $team_list['teamName'];
                   $final_players_list[$i]['isPlayerAdded'] = false;
                   $final_players_list[$i]['isIGL'] = false;
                   $final_players_list[$i]['teamPlayerId'] = (int)$final_players_list[$i]['teamPlayerId'];
                   $final_players_list[$i]['playerId'] = (int)$final_players_list[$i]['playerId'];
                   $final_players_list[$i]['playerCredit'] = (float)$final_players_list[$i]['playerCredit'];
                   $final_players_list[$i]['playerTotalPoints'] = (int)$final_players_list[$i]['playerTotalPoints'];
                   $final_players_list[$i]['playerLogo'] = base_url().$final_players_list[$i]['playerMediaUrl'].'/'.$final_players_list[$i]['playerMediaName'].'_300x300'.$final_players_list[$i]['playerMediaExt'];

                }

              $merged_result[$team_key]['teamLogo'] = base_url().$teams_list[$team_key]['mediaUrl'].'/'.$teams_list[$team_key]['mediaName'].'_300x300'.$teams_list[$team_key]['mediaExt'];
              $merged_result[$team_key]['teamsListId']=(int)$merged_result[$team_key]['teamsListId'];
              $merged_result[$team_key]['teamId']=(int)$merged_result[$team_key]['teamId'];
              $merged_result[$team_key]['isPlayerSelected']=false;
              $merged_result[$team_key]['selectedPlayerCount']=0;
              $merged_result[$team_key]['playersList']=$final_players_list;


              }


              $response=array('status'=>true, 'message'=>'Success','matchName'=>$match_name, 'matchTime'=>$date_and_time, 'matchStatus'=>$match_status, 'tournamentName'=>$tournament_name, 'tournamentId'=>$tournament_id, 'tournamentType'=>$tournament_type, 'megaPrize'=>$mega_prize, 'squadList'=>$merged_result2 ,'teamList'=>$merged_result, 'playerCount'=>$merged_result3,'creditsLeft'=>$merged_result4 );
            }
            else
            {
              $response=array('status'=>false, 'message'=>'Failed','data'=>'');
            }
    
     }
     
      else
      {
          $response=array('status'=>false, 'message'=>$token_result[0]);
      }
    
    header('Content-Type: application/json');
		echo json_encode($response);
	}
  
  
  
  
//------------------------------------------------------------------------------------------------------------------///
  //--------------------------- get final players by match id for Battle Royale----------------------------///
  // $id = match_id
   //------------------------------------------------------------------------------------------------------------------///

  /*
  function getMatchDetailsBattleRoyale($id)
  {
    $merged_result=array();
    
    $matchDetail = $this->db->select('*')
			->from('tb_match_list')
      ->where('match_id',$id)
      ->get()->result_array();
    
    	if($matchDetail)
      {
                                      //  $response=array('status'=>true, 'message'=>'Success','data'=>'');
        $match_name=$matchDetail[0]["match_name"];
        $date_and_time = $matchDetail[0]["date_and_time"];
        $tournament_id = $matchDetail[0]["fk_tournament_id"];
        $match_status = $matchDetail[0]["match_status"];
      }

      else
      {
        $response=array('status'=>false, 'message'=>'Failed','data'=>'');
      }
    
    
    $tournament_details = $this->db->select(' tournament_name , tournament_type')
			->from('tb_tournaments')
      ->where('tournament_id',$tournament_id)
			->get()->result_array();
    
    if($tournament_details)
      {
                                          //  $response=array('status'=>true, 'message'=>'Success','data'=>'');
        $tournament_name=$tournament_details[0]["tournament_name"];
        $tournament_type=$tournament_details[0]["tournament_type"];
      }

    
    $teams_list=$this->db->select(' teams_list_id , team_id , team_name , media_url ')
			->from('tb_teams_list')
      ->join('tb_teams','tb_teams_list.fk_team_id = tb_teams.team_id','left')
      ->join('tb_medias','tb_teams.fk_logo_id = tb_medias.media_id','left')
			->where('tb_teams_list.fk_match_id',$id)
			->get()->result_array();
     
    $merged_result=$teams_list;
    
                                     //if($tournament_type == )
    
    if($teams_list)
    {
      foreach($teams_list as $team_key => $team_list)
      {
        $final_players_list=$this->db->select('team_player_id, player_id as playerId, player_name as playerName, br_role, player_credit')
          ->from('tb_final_team_list')
          ->join('tb_team_players','tb_final_team_list.fk_team_player_id = tb_team_players.team_player_id','left')
          ->join('tb_players','tb_team_players.fk_player_id = tb_players.player_id','left')
          ->where('tb_final_team_list.fk_team_list_id',$team_list['teams_list_id'])
          ->where('tb_team_players.fk_tournament_id',$tournament_id)
          ->get()->result_array(); 
      
        
       $count_value = count($final_players_list,0);  

        for($i = 0 ; $i < $count_value ; $i++)
        {
           $final_players_list[$i]['isPlayerAdded'] = false;
           $final_players_list[$i]['isIGL'] = false;
        }
        
        
      $merged_result[$team_key]['player_list']=$final_players_list;
      
      }
         
    
      $response=array('status'=>true, 'message'=>'Success','matchName'=>$match_name, 'matchTime'=>$date_and_time, 'matchStatus'=>$match_status, 'tournamentName'=>$tournament_name,'tournamentId'=>$tournament_id, 'teamList'=>$merged_result );
	  }
		else
    {
			$response=array('status'=>false, 'message'=>'Failed','data'=>'');
    }
    header('Content-Type: application/json');
		echo json_encode($response);
	}
 */
  
   
    
//------------------------------------------------------------------------------------------------------------------///
  //--------------------------- get final players by match id for MultiPlayer----------------------------///
  // $id = match_id
   //------------------------------------------------------------------------------------------------------------------///
 
  /*
  function getmatchDetailsMultiPlayer($id)
  {
    $merged_result=array();
    
    $matchDetail = $this->db->select('*')
			->from('tb_match_list')
      ->where('match_id',$id)
      ->get()->result_array();
    
    	if($matchDetail)
      {
                                     //  $response=array('status'=>true, 'message'=>'Success','data'=>'');
        $match_name=$matchDetail[0]["match_name"];
        $date_and_time = $matchDetail[0]["date_and_time"];
        $tournament_id = $matchDetail[0]["fk_tournament_id"];
        $match_status = $matchDetail[0]["match_status"];
      }

      else
      {
        $response=array('status'=>false, 'message'=>'Failed','data'=>'');
      }
    
    
    $tournament_details = $this->db->select(' tournament_name , tournament_type')
			->from('tb_tournaments')
      ->where('tournament_id',$tournament_id)
			->get()->result_array();
    
    if($tournament_details)
      {
                                              //  $response=array('status'=>true, 'message'=>'Success','data'=>'');
        $tournament_name=$tournament_details[0]["tournament_name"];
        $tournament_type=$tournament_details[0]["tournament_type"];
      }

    
    $teams_list=$this->db->select(' teams_list_id , team_id , team_name , media_url ')
			->from('tb_teams_list')
      ->join('tb_teams','tb_teams_list.fk_team_id = tb_teams.team_id','left')
      ->join('tb_medias','tb_teams.fk_logo_id = tb_medias.media_id','left')
			->where('tb_teams_list.fk_match_id',$id)
			->get()->result_array();
     
    $merged_result=$teams_list;
    
                                                 //if($tournament_type == )
    
    if($teams_list)
    {
      foreach($teams_list as $team_key => $team_list)
      {
        $final_players_list=$this->db->select('team_player_id, player_id as playerId, player_name as playerName, mp_role, player_credit')
          ->from('tb_final_team_list')
          ->join('tb_team_players','tb_final_team_list.fk_team_player_id = tb_team_players.team_player_id','left')
          ->join('tb_players','tb_team_players.fk_player_id = tb_players.player_id','left')
          ->where('tb_final_team_list.fk_team_list_id',$team_list['teams_list_id'])
          ->where('tb_team_players.fk_tournament_id',$tournament_id)
          ->get()->result_array(); 
        
        
       $count_value = count($final_players_list,0);  

        for($i = 0 ; $i < $count_value ; $i++)
        {
           $final_players_list[$i]['isPlayerAdded'] = false;
           $final_players_list[$i]['isIGL'] = false;
        }
        
      
      $merged_result[$team_key]['player_list']=$final_players_list;
      
      }
         
    
      $response=array('status'=>true, 'message'=>'Success','matchName'=>$match_name, 'matchTime'=>$date_and_time, 'matchStatus'=>$match_status, 'tournamentName'=>$tournament_name,'tournamentId'=>$tournament_id, 'teamList'=>$merged_result );
	  }
		else
    {
			$response=array('status'=>false, 'message'=>'Failed','data'=>'');
    }
    header('Content-Type: application/json');
		echo json_encode($response);
	}
 
 */ 
 
  
  
//-------------------------------------------------------------------------------------------------------------------------///
//--------------------------- get User squads in the beginning by match id & User id ----- before starting the match -----------------------/// 
  // $id1 = match_id  --------- $id2 = user_id
  //------------------------------------------------------------------------------------------------------------------///

   
  function squadsBeforeMatch($id1,$id2)
  {
    
   
    $merged_result=array();
    $merged_result2=array();
    
		$squads=$this->db->select('user_squad_id as userSquadId , user_squad_no as userSquadNo , igl_team_player_id as iglTeamPlayerId , total_squad_points as totalSquadPoints')
			->from('tb_user_squads')
      ->where('fk_match_id',$id1)
      ->where('fk_user_id',$id2)
			->get()->result_array();

 		$merged_result=$squads;

    $tournament_details=$this->db->select('*')
			->from('tb_match_list')
      ->join('tb_tournaments','tb_match_list.fk_tournament_id = tb_tournaments.tournament_id','left')
			->where('match_id',$id1)
			->get()->result_array();
    
    if($tournament_details)
      {
                                                 //  $response=array('status'=>true, 'message'=>'Success','data'=>'');
        $match_name=$tournament_details[0]["match_name"];
        $tournament_name=$tournament_details[0]["tournament_name"];
        $tournament_id=$tournament_details[0]["tournament_id"];
      }
			
    
                                                //  if($tournament_type==6000) //tournament type is 6000 Battle royale
                                                //  {

      if($squads)
      {

      foreach($squads as $squad_key => $squad)
        {

          $squad_details=$this->db->select(' team_player_id as teamPlayerId, player_id as playerId, player_name as playerName, br_role as brRole, mp_role as mpRole, player_credit as playerCredit, team_name as teamName')
          ->from('tb_user_squads')
          ->join('tb_user_squad_players', 'tb_user_squads.user_squad_id = tb_user_squad_players.fk_user_squad_id','left')
          ->join('tb_team_players','tb_user_squad_players.my_player_id = tb_team_players.team_player_id','left')
          ->join('tb_players','tb_team_players.fk_player_id = tb_players.player_id','left')
          ->join('tb_teams','tb_team_players.fk_team_id = tb_teams.team_id','left')
          ->where('tb_user_squads.user_squad_id',$squad['userSquadId'])
          ->where('tb_team_players.fk_tournament_id',$tournament_id)
          ->get()->result_array();	

        $count_value = count($squad_details,0);  

        for($i = 0 ; $i < $count_value ; $i++)
        {
           $squad_details[$i]['teamPlayerId'] = (int)$squad_details[$i]['teamPlayerId'];
           $squad_details[$i]['playerId'] = (int)$squad_details[$i]['playerId'];
           $squad_details[$i]['playerCredit'] = (float)$squad_details[$i]['playerCredit'];

           $squad_details[$i]['isPlayerAdded'] = true;
          if($squad_details[$i]['teamPlayerId'] == $squad['iglTeamPlayerId'])
          {
           $squad_details[$i]['isIGL'] = true;
          }
          else
          {
           $squad_details[$i]['isIGL'] = false;
          }
            
        }  
        
        
          $merged_result[$squad_key]['userSquadId']=(int)$merged_result[$squad_key]['userSquadId'];
          $merged_result[$squad_key]['userSquadNo']=(int)$merged_result[$squad_key]['userSquadNo'];
          $merged_result[$squad_key]['iglTeamPlayerId']=(int)$merged_result[$squad_key]['iglTeamPlayerId'];
          $merged_result[$squad_key]['totalSquadPoints']=(int)$merged_result[$squad_key]['totalSquadPoints'];
          $merged_result[$squad_key]['squadPlayerList']=$squad_details;
          
        }

      return($merged_result);
      }
      else
      {
        return($merged_result2);
      }
                                      //    }

                                       //   else //tournament type is 7000 Multiplayer
                                      //    {
                                      //    }
    
    header('Content-Type: application/json');
		echo json_encode($response);
	}
    
  
  
  
//-------------------------------------------------------------------------------------------------------------------------///
//--------------------------- get User squads by match id & User id ----- before starting the match -----------------------/// 
  // $id1 = match_id  --------- $id2 = user_id
  //------------------------------------------------------------------------------------------------------------------///
  
//   function getMySquadDetails($id1,$id2)
  function getMySquadDetails($id1)
  {
     
    $token_result = array(); 
      $token_result = $this->GetTokenData();

     
     if ($token_result['uId'] != 0)
       {
          
        $id2 = (int)$token_result['uId'];
    
        $merged_result=array();

        $squads=$this->db->select('user_squad_id as userSquadId, user_squad_no as userSquadNo , igl_team_player_id as iglTeamPlayerId, total_squad_points as totalSquadPoints')
          ->from('tb_user_squads')
          ->where('fk_match_id',$id1)
          ->where('fk_user_id',$id2)
          ->get()->result_array();

        $merged_result=$squads;

        $tournament_details=$this->db->select('*')
          ->from('tb_match_list')
          ->join('tb_tournaments','tb_match_list.fk_tournament_id = tb_tournaments.tournament_id','left')
          ->where('match_id',$id1)
          ->get()->result_array();

        if($tournament_details)
          {
                                                     //  $response=array('status'=>true, 'message'=>'Success','data'=>'');
            $match_name=$tournament_details[0]["match_name"];
            $tournament_name=$tournament_details[0]["tournament_name"];
            $tournament_id=(int)$tournament_details[0]["tournament_id"];
          }


                                                    //  if($tournament_type==6000) //tournament type is 6000 Battle royale
                                                    //  {

          if($squads)
          {

          foreach($squads as $squad_key => $squad)
            {

              $squad_details=$this->db->select(' team_player_id as teamPlayerId, player_id as playerId, player_name as playerName, br_role as brRole, mp_role as mpRole, player_credit as playerCredit, team_name as teamName')
              ->from('tb_user_squads')
              ->join('tb_user_squad_players', 'tb_user_squads.user_squad_id = tb_user_squad_players.fk_user_squad_id','left')
              ->join('tb_team_players','tb_user_squad_players.my_player_id = tb_team_players.team_player_id','left')
              ->join('tb_players','tb_team_players.fk_player_id = tb_players.player_id','left')
              ->join('tb_teams','tb_team_players.fk_team_id = tb_teams.team_id','left')
              ->where('tb_user_squads.user_squad_id',$squad['userSquadId'])
              ->where('tb_team_players.fk_tournament_id',$tournament_id)
              ->get()->result_array();	

            $count_value = count($squad_details,0);  

            for($i = 0 ; $i < $count_value ; $i++)
            {
               $squad_details[$i]['teamPlayerId'] = (int)$squad_details[$i]['teamPlayerId'];
               $squad_details[$i]['playerId'] = (int)$squad_details[$i]['playerId'];
               $squad_details[$i]['playerCredit'] = (float)$squad_details[$i]['playerCredit'];

               $squad_details[$i]['isPlayerAdded'] = true;
              if($squad_details[$i]['teamPlayerId'] == $squad['iglTeamPlayerId'])
              {
               $squad_details[$i]['isIGL'] = true;
              }
              else
              {
               $squad_details[$i]['isIGL'] = false;
              }

            }  


              $merged_result[$squad_key]['userSquadId']=(int)$merged_result[$squad_key]['userSquadId'];
              $merged_result[$squad_key]['userSquadNo']=(int)$merged_result[$squad_key]['userSquadNo'];
              $merged_result[$squad_key]['iglTeamPlayerId']=(int)$merged_result[$squad_key]['iglTeamPlayerId'];
              $merged_result[$squad_key]['totalSquadPoints']=(int)$merged_result[$squad_key]['totalSquadPoints'];
              $merged_result[$squad_key]['squadPlayerList']=$squad_details;

            }

          $response=array('status'=>true, 'message'=>'Success','matchName'=>$match_name, 'tournamentName'=>$tournament_name,'tournamentId'=>$tournament_id, 'squadData'=>$merged_result);
          }
          else
          {
            $response=array('status'=>false, 'message'=>'Failed','data'=>'');
          }
                                                    //    }

                                                     //   else //tournament type is 7000 Multiplayer
                                                    //    {
                                                    //    }

        
     }
     
      else
      {
          $response=array('status'=>false, 'message'=>$token_result[0]);
      }

    header('Content-Type: application/json');
		echo json_encode($response);
	}
  
  
  
  //------------------------------------------------------------------------------------------------------------------///
  //------------------------------------------------------------------------------------------------------------------///
 //--------------------------- get stats of all players in a match by match id ----- after starting the match -----------------------/// 
  // $id1 = match_id
    //------------------------------------------------------------------------------------------------------------------///

  
  function getPlayerStats($id1)
  {
    
    $token_result = array(); 
      $token_result = $this->GetTokenData();

     
     if ($token_result['uId'] != 0)
       {
          

        $tournament_details=$this->db->select('*')
          ->from('tb_match_list')
          ->join('tb_tournaments','tb_match_list.fk_tournament_id = tb_tournaments.tournament_id','left')
          ->where('match_id',$id1)
          ->get()->result_array();

        if($tournament_details)
          {
                                                      //  $response=array('status'=>true, 'message'=>'Success','data'=>'');
            $match_name=$tournament_details[0]["match_name"];
            $tournament_name=$tournament_details[0]["tournament_name"];
            $tournament_id=(int)$tournament_details[0]["tournament_id"];
            $tournament_type=(int)$tournament_details[0]["tournament_type"];
          }

        if($tournament_type==6000)                       //tournament type is 6000 Battle royale
        {


          $player_stats=$this->db->select('player_name as playerName , team_name as teamName , kills as playerKills , knocks as playerKnocks, position as teamPosition, points as playerTotalPoints')
            ->from('tb_br_match_details')
            ->join('tb_team_players','tb_br_match_details.fk_team_player_id = tb_team_players.team_player_id','left')
            ->join('tb_players','tb_team_players.fk_player_id = tb_players.player_id','left')
            ->join('tb_teams','tb_team_players.fk_team_id = tb_teams.team_id','left')
            ->where('fk_match_id',$id1)
            ->order_by('points','DESC')
            ->get()->result_array();

           $count_value = count($player_stats,0);  

            for($i = 0 ; $i < $count_value ; $i++)
            {
               $player_stats[$i]['playerKills'] = (int)$player_stats[$i]['playerKills'];
               $player_stats[$i]['playerKnocks'] = (int)$player_stats[$i]['playerKnocks'];
               $player_stats[$i]['teamPosition'] = (int)$player_stats[$i]['teamPosition'];
               $player_stats[$i]['playerTotalPoints'] = (int)$player_stats[$i]['playerTotalPoints'];
            }

          if($player_stats)
          {

            $response=array('status'=>true, 'message'=>'Success','matchName'=>$match_name, 'tournamentName'=>$tournament_name,'tournamentId'=>$tournament_id, 'playersStatusList'=>$player_stats);
          }
          else
          {
            $response=array('status'=>false, 'message'=>'Failed','data'=>'');
          }
        }
    
        else                                        //tournament type is 7000 Multiplayer
        {
          $player_stats=$this->db->select('player_name as playerName , team_name as teamName , kills as playerKills  , assists as playerAssists , deaths as playerDeaths, win_lose as teamWinLose , points as playerTotalPoints')
            ->from('tb_mp_match_details')
            ->join('tb_team_players','tb_mp_match_details.fk_team_player_id = tb_team_players.team_player_id','left')
            ->join('tb_players','tb_team_players.fk_player_id = tb_players.player_id','left')
            ->join('tb_teams','tb_team_players.fk_team_id = tb_teams.team_id','left')
            ->where('fk_match_id',$id1)
            ->order_by('points','DESC')
            ->get()->result_array();

           $count_value = count($player_stats,0);  

            for($i = 0 ; $i < $count_value ; $i++)
            {
               $player_stats[$i]['playerKills'] = (int)$player_stats[$i]['playerKills'];
               $player_stats[$i]['playerAssists'] = (int)$player_stats[$i]['playerAssists'];
               $player_stats[$i]['playerDeaths'] = (int)$player_stats[$i]['playerDeaths'];
               $player_stats[$i]['teamWinLose'] = (int)$player_stats[$i]['teamWinLose'];
               $player_stats[$i]['playerTotalPoints'] = (int)$player_stats[$i]['playerTotalPoints'];
            }
          if($player_stats)
          {

            $response=array('status'=>true, 'message'=>'Success','matchName'=>$match_name, 'tournamentName'=>$tournament_name,'tournamentId'=>$tournament_id, 'playersStatusList'=>$player_stats);
          }
          else
          {
            $response=array('status'=>false, 'message'=>'Failed','data'=>'');
          }
        }
    
       }
     
      else
      {
          $response=array('status'=>false, 'message'=>$token_result[0]);
      }

		       
    header('Content-Type: application/json');
		echo json_encode($response);
	}
   
  
  
  //------------------------------------------------------------------------------------------------------------------///
  //-------------------------------get leaderboard of a contest by pool id-----------------------------------------------------///
  //------------------------------- Multiple users with same points will get same rank ----------------------------------------///
  //$id1 = Pool id , id2 =user_id
   //------------------------------------------------------------------------------------------------------------------///
 
//    function getLeaderBoard($id1,$id2)
   function getLeaderBoard($id1)
  {
     
    $token_result = array(); 
    $token_result = $this->GetTokenData();


   if ($token_result['uId'] != 0)
     {

      $id2 = (int)$token_result['uId'];


        $leaderboard=array();
        $userInPool=array();


        $pool_details=$this->db->select('*')
          ->from('tb_pool_types')
          ->join('tb_prize_pool_list','tb_pool_types.pool_type_id = tb_prize_pool_list.fk_pool_type_id','left')
          ->join('tb_match_list','tb_prize_pool_list.fk_match_id = tb_match_list.match_id','left')
          ->join('tb_tournaments','tb_match_list.fk_tournament_id = tb_tournaments.tournament_id','left')
          ->where('pool_id',$id1)
          ->get()->result_array();

        if($pool_details)
          {
                                                 //  $response=array('status'=>true, 'message'=>'Success','data'=>'');
            $match_name=$pool_details[0]["match_name"];
            $tournament_name=$pool_details[0]["tournament_name"];
            $tournament_id=(int)$pool_details[0]["tournament_id"];
                                                //  $match_status=$pool_details[0]["match_status"];
            $total_amount=(int)$pool_details[0]["total_amount"];
            $total_slots=(int)$pool_details[0]["total_slots"];
          } 


          $value_count = $this->db->select('fk_user_squad_id')
            ->from('tb_user_join_pool')
            ->where('fk_pool_id',$id1)
            ->count_all_results();     

//              echo "count of leaders : ". $value_count;

          $userSquadId = $this->db->select('fk_user_squad_id')
            ->from('tb_user_join_pool')
            ->join('tb_user_squads','tb_user_join_pool.fk_user_squad_id = tb_user_squads.user_squad_id','left')
            ->where('fk_pool_id',$id1)
            ->where('fk_user_id',$id2)
            ->order_by('total_squad_points', 'DESC')
            ->get()->result_array();

//      print_r($userSquadId);
     
         $count_value = count($userSquadId,0);  
//         echo "count of userSquadId : ". $count_value;

          $leaderboard=$this->db->select('user_squad_id as userSquadId , username , user_squad_no as userSquadNo , total_squad_points as points ')
            ->from('tb_user_join_pool')
            ->join('tb_user_squads','tb_user_join_pool.fk_user_squad_id = tb_user_squads.user_squad_id','left')
            ->join('tb_users','tb_user_squads.fk_user_id = tb_users.user_id','left')
            ->where('fk_pool_id',$id1)
            ->order_by('total_squad_points', 'DESC')
            ->get()->result_array();

         for ($x = 1; $x <= $value_count; $x++) 
         { 
           $leaderboard[$x-1]['userSquadId'] = (int)$leaderboard[$x-1]['userSquadId'];
           $leaderboard[$x-1]['userSquadNo'] = (int)$leaderboard[$x-1]['userSquadNo'];
           $leaderboard[$x-1]['points'] = (int)$leaderboard[$x-1]['points'];

           $leaderboard[$x-1]['sNo'] = $x;
           $leaderboard[$x-1]['position'] = $x;
         }

         for ($x = 0; $x < ($value_count-1); $x++) 
         {
           if($leaderboard[$x]['points'] == $leaderboard[$x+1]['points'])
           {
             $leaderboard[$x+1]['position'] = $leaderboard[$x]['position'];
           }
           else
           {
             $response=array('status'=>false, 'message'=>'Failed','data'=>'');
           }
         }


         for ($y = 0; $y < $count_value; $y++)
         {

           $userSquadIdInPool = $userSquadId[$y]['fk_user_squad_id'];

      //      foreach($squads as $squad_key => $squad)

           for ($x = 0; $x < $value_count; $x++) 
           {

             if($leaderboard[$x]['userSquadId'] == $userSquadIdInPool)
             {
               $userInPool[$y] = $leaderboard[$x];
                break;
      //         $leaderboard[$x+1]['position'] = $leaderboard[$x]['position'];
             }
             else
             {
               $response=array('status'=>false, 'message'=>'Failed','data'=>'');
             }
           }
         }

    //       print_r($userInPool);

          if($leaderboard)
          {

            $response=array('status'=>true, 'message'=>'Success','matchName'=>$match_name, 'tournamentName'=>$tournament_name,'tournamentId'=>$tournament_id, 'totalAmount'=>$total_amount, 'totalSlots'=>$total_slots,'userInPool'=>$userInPool, 'leaderBoardList'=>$leaderboard);
          }
          else
          {
            $response=array('status'=>false, 'message'=>'Failed','data'=>'No leader Board');
          }

   }
     
      else
      {
          $response=array('status'=>false, 'message'=>$token_result[0]);
      }

		
    header('Content-Type: application/json');
		echo json_encode($response);
	}
  
  
 
  
  //------------------------------------------------------------------------------------------------------------------///
  //-------------------------------get Prize Distribution of a contest by pool id-----------------------------------------------------///
  //$id1 = Pool id , id2 =user_id
   //------------------------------------------------------------------------------------------------------------------///
 
   function getPrizeDistribution($id1)
  {
     
    $token_result = array(); 
    $token_result = $this->GetTokenData();


   if ($token_result['uId'] != 0)
     {

      $id2 = (int)$token_result['uId'];


        $distributedPrize=array();
        $leaderboard=array();


        $pool_details=$this->db->select('*')
          ->from('tb_prize_pool_list')
          ->join('tb_pool_types','tb_prize_pool_list.fk_pool_type_id = tb_pool_types.pool_type_id','left')
//           ->join('tb_prize_distribution','tb_pool_types.pool_type_id = tb_prize_distribution.fk_pool_type_id','left')
          ->where('pool_id',$id1)
          ->get()->result_array();

        if($pool_details)
          {
                                                 //  $response=array('status'=>true, 'message'=>'Success','data'=>'');
            $reg_fee=(int)$pool_details[0]["reg_fee"];
            $win_percentage=(float)$pool_details[0]["win_percentage"];
            $pool_type=(int)$pool_details[0]["fk_pool_type_id"];
            $total_amount=(int)$pool_details[0]["total_amount"];
            $total_slots=(int)$pool_details[0]["total_slots"];
          } 


          $value_count = $this->db->select('fk_pool_type_id')
            ->from('tb_prize_distribution')
            ->where('fk_pool_type_id',$pool_type)
            ->count_all_results();     

//              echo "count of leaders : ". $value_count;

//           $userSquadId = $this->db->select('fk_user_squad_id')
//             ->from('tb_user_join_pool')
//             ->join('tb_user_squads','tb_user_join_pool.fk_user_squad_id = tb_user_squads.user_squad_id','left')
//             ->where('fk_pool_id',$id1)
//             ->where('fk_user_id',$id2)
//             ->order_by('total_squad_points', 'DESC')
//             ->get()->result_array();

// //      print_r($userSquadId);
     
//          $count_value = count($userSquadId,0);  
//         echo "count of userSquadId : ". $count_value;

          $leaderboard=$this->db->select('start_slot as startSlot , end_slot as endSlot , amount ')
            ->from('tb_prize_distribution')
//             ->join('tb_users','tb_user_squads.fk_user_id = tb_users.user_id','left')
            ->where('fk_pool_type_id',$pool_type)
//             ->order_by('total_squad_points', 'DESC')
            ->get()->result_array();

         for ($x = 1; $x <= $value_count; $x++) 
         { 
           $leaderboard[$x-1]['sNo'] = $x;
           $leaderboard[$x-1]['startSlot'] = (int)$leaderboard[$x-1]['startSlot'];
           $leaderboard[$x-1]['endSlot'] = (int)$leaderboard[$x-1]['endSlot'];
           $leaderboard[$x-1]['amount'] = (float)$leaderboard[$x-1]['amount'];

         }

      
          //       print_r($userInPool);

          if($leaderboard)
          {

            $response=array('status'=>true, 'message'=>'Success','regFee'=>$reg_fee, 'winPercentage'=>$win_percentage,'poolType'=>$pool_type, 'totalAmount'=>$total_amount, 'totalSlots'=>$total_slots, 'distributedPrize'=>$leaderboard);
          }
          else
          {
            $response=array('status'=>false, 'message'=>'Failed','data'=>'No Prize Distribution');
          }

   }
     
      else
      {
          $response=array('status'=>false, 'message'=>$token_result[0]);
      }

		
    header('Content-Type: application/json');
		echo json_encode($response);
	}
  
  
   
  
  //-------------------------------------------------------------------------------------------------------------------------///
//--------------------------- get User squad stats by match id & User id ----- after starting the match -----------------------/// 
//-------------------------------------------------- 2x point calculation for IGL  ----------------------------------------------/// 
  // $id1 = match_id  --------- $id2 = user_id
   //------------------------------------------------------------------------------------------------------------------///

  
   
//   function getMySquadStats($id1,$id2)
  function getMySquadStats($id1)
  {
     
    $token_result = array(); 
    $token_result = $this->GetTokenData();


    if ($token_result['uId'] != 0)
     {

      $id2 = (int)$token_result['uId'];

 
      $merged_result=array();

      $squads=$this->db->select('user_squad_id as userSquadId , user_squad_no as userSquadNo , igl_team_player_id as iglTeamPlayerId , total_squad_points as points')
        ->from('tb_user_squads')
        ->where('fk_match_id',$id1)
        ->where('fk_user_id',$id2)
        ->order_by('total_squad_points', 'DESC')
        ->get()->result_array();

      $merged_result=$squads;

      $tournament_details=$this->db->select('*')
        ->from('tb_match_list')
        ->join('tb_tournaments','tb_match_list.fk_tournament_id = tb_tournaments.tournament_id','left')
        ->where('match_id',$id1)
        ->get()->result_array();

      if($tournament_details)
        {
                                             //  $response=array('status'=>true, 'message'=>'Success','data'=>'');
          $match_name=$tournament_details[0]["match_name"];
          $tournament_name=$tournament_details[0]["tournament_name"];
          $tournament_id=(int)$tournament_details[0]["tournament_id"];
          $tournament_type=(int)$tournament_details[0]["tournament_type"];

        }


      if($tournament_type==6000)                  //tournament type is 6000 Battle royale
      {

        if($squads)
        {

        foreach($squads as $squad_key => $squad)
          {

            $squad_stats=$this->db->select(' team_player_id as teamPlayerId , player_name as playerName, kills as playerKills, knocks as playerKnocks, position as teamPosition, points as playerTotalPoints')
            ->from('tb_user_squads')
            ->join('tb_user_squad_players', 'tb_user_squads.user_squad_id = tb_user_squad_players.fk_user_squad_id','left')
            ->join('tb_team_players','tb_user_squad_players.my_player_id = tb_team_players.team_player_id','left')
            ->join('tb_players','tb_team_players.fk_player_id = tb_players.player_id','left')
            ->join('tb_br_match_details','tb_user_squad_players.my_player_id = tb_br_match_details.fk_team_player_id','left')
            ->where('tb_user_squads.user_squad_id',$squad['userSquadId'])
            ->where('tb_team_players.fk_tournament_id',$tournament_id)
            ->group_by('team_player_id')
            ->get()->result_array();	

            for($i=0 ; $i<4 ; $i++)
            {
              $squad_stats[$i]['teamPlayerId'] = (int)$squad_stats[$i]['teamPlayerId'];
              $squad_stats[$i]['playerKills'] = (int)$squad_stats[$i]['playerKills'];
              $squad_stats[$i]['playerKnocks'] = (int)$squad_stats[$i]['playerKnocks'];
              $squad_stats[$i]['teamPosition'] = (int)$squad_stats[$i]['teamPosition'];
              $squad_stats[$i]['playerTotalPoints'] = (int)$squad_stats[$i]['playerTotalPoints'];
              if($squad_stats[$i]['teamPlayerId']==$merged_result[$squad_key]['iglTeamPlayerId'])
              {
                                                  //print_r($squad_stats[$i]['points']);
                                                  //echo "\t";
                $squad_stats[$i]['playerTotalPoints'] = $squad_stats[$i]['playerTotalPoints'] * 2 ; 
                                                  //echo "\t";
                                                  //print_r($squad_stats[$i]['points']);
                                                  //echo "\n";
              }
            }

            $merged_result[$squad_key]['userSquadId'] = (int)$merged_result[$squad_key]['userSquadId'];
            $merged_result[$squad_key]['userSquadNo'] = (int)$merged_result[$squad_key]['userSquadNo'];
            $merged_result[$squad_key]['iglTeamPlayerId'] = (int)$merged_result[$squad_key]['iglTeamPlayerId'];
            $merged_result[$squad_key]['points'] = (int)$merged_result[$squad_key]['points'];
            $merged_result[$squad_key]['squadPlayerStats']=$squad_stats;

                                                   // echo $merged_result[$squad_key]['igl_team_player_id'];
                                                   // echo "\n";
                                           /*         if(  == $merged_result[$squad_key]['igl_team_player_id'] )
                                                      {
                                                        echo "success";
                                                      }
                                                    else
                                                      {
                                                        echo "fail";
                                                      }
                                           */         
//         print_r($merged_result) ;
        }
//         $response=array('status'=>true, 'message'=>'Success');

        $response=array('status'=>true, 'message'=>'Success','matchName'=>$match_name, 'tournamentName'=>$tournament_name,'tournamentId'=>$tournament_id, 'mySquadStatuslist'=>$merged_result);
        }
        else
        {
          $response=array('status'=>false, 'message'=>'Failed','data'=>'');
        }
      }

      else                                //tournament type is 7000 Multiplayer
      {

        if($squads)
        {

        foreach($squads as $squad_key => $squad)
          {

            $squad_stats=$this->db->select(' team_player_id as teamPlayerId, player_name as playerName, kills as playerKills, assists as playerAssists, deaths as playerDeaths, win_lose as teamWinLose , points as playerTotalPoints')
            ->from('tb_user_squads')
            ->join('tb_user_squad_players', 'tb_user_squads.user_squad_id = tb_user_squad_players.fk_user_squad_id','left')
            ->join('tb_team_players','tb_user_squad_players.my_player_id = tb_team_players.team_player_id','left')
            ->join('tb_players','tb_team_players.fk_player_id = tb_players.player_id','left')
            ->join('tb_mp_match_details','tb_user_squad_players.my_player_id = tb_mp_match_details.fk_team_player_id','left')
            ->where('tb_user_squads.user_squad_id',$squad['userSquadId'])
            ->where('tb_team_players.fk_tournament_id',$tournament_id)
            ->group_by('team_player_id')
            ->get()->result_array();	

            for($i=0 ; $i<5 ; $i++)
              {
                $squad_stats[$i]['teamPlayerId'] = (int)$squad_stats[$i]['teamPlayerId'];
                $squad_stats[$i]['playerKills'] = (int)$squad_stats[$i]['playerKills'];
                $squad_stats[$i]['playerAssists'] = (int)$squad_stats[$i]['playerAssists'];
                $squad_stats[$i]['playerDeaths'] = (int)$squad_stats[$i]['playerDeaths'];
                $squad_stats[$i]['teamWinLose'] = (int)$squad_stats[$i]['teamWinLose'];
                $squad_stats[$i]['playerTotalPoints'] = (int)$squad_stats[$i]['playerTotalPoints'];
                if($squad_stats[$i]['teamPlayerId']==$merged_result[$squad_key]['iglTeamPlayerId'])
                {
                                              //print_r($squad_stats[$i]['points']);
                                              //echo "\t";
                  $squad_stats[$i]['playerTotalPoints'] = $squad_stats[$i]['playerTotalPoints'] * 2 ; 
                                              //echo "\t";
                                              //print_r($squad_stats[$i]['points']);
                                              //echo "\n";
                }
              }


            $merged_result[$squad_key]['userSquadId'] = (int)$merged_result[$squad_key]['userSquadId'];
            $merged_result[$squad_key]['userSquadNo'] = (int)$merged_result[$squad_key]['userSquadNo'];
            $merged_result[$squad_key]['iglTeamPlayerId'] = (int)$merged_result[$squad_key]['iglTeamPlayerId'];
            $merged_result[$squad_key]['points'] = (int)$merged_result[$squad_key]['points'];

            $merged_result[$squad_key]['squadPlayerStats']=$squad_stats;

          }

        $response=array('status'=>true, 'message'=>'Success','matchName'=>$match_name, 'tournamentName'=>$tournament_name,'tournamentId'=>$tournament_id, 'mySquadStatuslist'=>$merged_result);
        }
        else
        {
          $response=array('status'=>false, 'message'=>'Failed','data'=>'');
        }
      }
    
     }

    else
    {
        $response=array('status'=>false, 'message'=>$token_result[0]);
    }
    
    header('Content-Type: application/json');
		echo json_encode($response);
	}
  
  
  
  
   //------------------------------------------------------------------------------------------------------------------///
  //-------------------------------get Contest for a match by  match id-----------------------------------------------------///
  //$id1 = match id , $id2 = user id , $id3 = sort id
 // user joined pools
  ///  check 
    //------------------------------------------------------------------------------------------------------------------///
  
  
//   function getContest($id1,$id2,$id3)
  function getContest($id1,$id3)
  {
    
    $token_result = array(); 
    $token_result = $this->GetTokenData();


    if ($token_result['uId'] != 0)
     {

      $id2 = (int)$token_result['uId'];

      $contests=array();

      $sort_options[0]['sortOption'] = "Prize Pool";
      $sort_options[0]['sortId'] = 50;

      $sort_options[1]['sortOption'] = "Size";
      $sort_options[1]['sortId'] = 51;

      $sort_options[2]['sortOption'] = "Entry Fee";
      $sort_options[2]['sortId'] = 52;


      $user_joined=$this->db->select(' fk_pool_id ')
        ->from('tb_user_squads')
        ->join('tb_user_join_pool', 'tb_user_squads.user_squad_id = tb_user_join_pool.fk_user_squad_id','left')
        ->where('fk_match_id',$id1)
        ->where('fk_user_id',$id2)
        ->order_by('fk_pool_id', 'ASC')
        ->group_by('fk_pool_id')
        ->get()->result_array();

                                        /*    $user_joined_count=$this->db->select(' fk_pool_id ')
                                              ->from('tb_user_squads')
                                              ->join('tb_user_join_pool', 'tb_user_squads.user_squad_id = tb_user_join_pool.fk_user_squad_id','left')
                                              ->where('fk_match_id',$id1)
                                              ->where('fk_user_id',$id2)
                                              ->order_by('fk_pool_id', 'ASC')
                                              ->group_by('fk_pool_id')
                                              ->count_all_results();   
                                        */
      $user_joined_count = count($user_joined,0);  

                                        /*     $pool_type_count = $this->db->select('pool_id')
                                                ->from('tb_prize_pool_list')
                                                ->where('fk_match_id',$id1)
                                                ->group_by('fk_pool_type_id')
                                                ->count_all_results();   
                                         */   
      switch ($id3)
      {


                                          //    if( $id3 == 52 )
                                          //    {

        case 52: $contests=$this->db->select(' pool_id as poolId , total_amount as prizePoolAmount , total_slots as totalNumberOfSpots , reg_slots as numberOfSpotsReg , reg_fee as entryFee , win_percentage as winPercentage , max_entry as maxEntry ')
          ->from('tb_pool_types')
          ->join('tb_prize_pool_list', 'tb_pool_types.pool_type_id = tb_prize_pool_list.fk_pool_type_id','left')
          ->where('fk_match_id',$id1)
          ->where('total_slots != reg_slots')
          ->order_by('reg_fee', 'ASC')
          ->group_by('fk_pool_type_id')
          ->get()->result_array();
        break;  
                                              //    }
                                              //    elseif( $id3 == 51 )
                                              //    {
        case 51: $contests=$this->db->select(' pool_id as poolId , total_amount as prizePoolAmount , total_slots as totalNumberOfSpots , reg_slots as numberOfSpotsReg , reg_fee as entryFee , win_percentage as winPercentage , max_entry as maxEntry ')
          ->from('tb_pool_types')
          ->join('tb_prize_pool_list', 'tb_pool_types.pool_type_id = tb_prize_pool_list.fk_pool_type_id','left')
          ->where('fk_match_id',$id1)
          ->where('total_slots != reg_slots')
          ->order_by('total_slots', 'DESC')
          ->group_by('fk_pool_type_id')
          ->get()->result_array();
        break;

                                            //    }
                                            //    else //( $id3 == 50 )
                                          //    {

        default: $contests=$this->db->select(' pool_id as poolId , total_amount as prizePoolAmount , total_slots as totalNumberOfSpots , reg_slots as numberOfSpotsReg , reg_fee as entryFee , win_percentage as winPercentage , max_entry as maxEntry ')
          ->from('tb_pool_types')
          ->join('tb_prize_pool_list', 'tb_pool_types.pool_type_id = tb_prize_pool_list.fk_pool_type_id','left')
          ->where('fk_match_id',$id1)
          ->where('total_slots != reg_slots')
          ->order_by('total_amount', 'DESC')
          ->group_by('fk_pool_type_id')
          ->get()->result_array();
        break;
                                            //    }    
      }


    $pool_type_count = count($contests,0);  


      for ($x = 0; $x < $pool_type_count; $x++) 
      { 
        $contests[$x]['poolId'] = (int)$contests[$x]['poolId'];
        $contests[$x]['prizePoolAmount'] = (int)$contests[$x]['prizePoolAmount'];
        $contests[$x]['totalNumberOfSpots'] = (int)$contests[$x]['totalNumberOfSpots'];
        $contests[$x]['numberOfSpotsReg'] = (int)$contests[$x]['numberOfSpotsReg'];
        $contests[$x]['entryFee'] = (int)$contests[$x]['entryFee'];
        $contests[$x]['winPercentage'] = (float)$contests[$x]['winPercentage'];
        $contests[$x]['maxEntry'] = (int)$contests[$x]['maxEntry'];
        $contests[$x]['userJoined'] = 0;
        $contests[$x]['numberOfSpotsLeft'] = (int)$contests[$x]['totalNumberOfSpots'] - (int)$contests[$x]['numberOfSpotsReg'];

         $joined = 0;
  //       $contests[$x]['isJoined'] = false;

        for ($y = 0; $y < $user_joined_count; $y++) 
        {
          if($contests[$x]['poolId'] == $user_joined[$y]['fk_pool_id'])
          {
            $contests[$x]['isJoined'] = true;
  //           $joined++;
            $joined=$this->db->select(' user_pool_id ')
              ->from('tb_user_squads')
              ->join('tb_user_join_pool', 'tb_user_squads.user_squad_id = tb_user_join_pool.fk_user_squad_id','left')
              ->where('fk_pool_id',$contests[$x]['poolId'])
              ->where('fk_user_id',$id2)
              ->count_all_results();     
  //             ->order_by('fk_pool_id', 'ASC')
  //             ->group_by('fk_pool_id')
            $contests[$x]['userJoined'] = $joined;

            break;
          }
          else
          {
            $contests[$x]['isJoined'] = false;
          }
        }

  //       echo $contests[$x]['poolId'];
  //       echo " : "; 
  //       echo $joined;
  //       echo "\n";
      }


                                                //     if($user_joined)
       if($contests)
        {

          $response=array('status'=>true, 'message'=>'Success', 'sortOptions'=>$sort_options, 'contestList'=>$contests);
                                             //        $response=array('status'=>true, 'message'=>'Success','contestList'=>$user_joined);
        }
        else
        {
          $response=array('status'=>false, 'message'=>'Failed','data'=>'');
        }
     }

    else
    {
        $response=array('status'=>false, 'message'=>$token_result[0]);
    }
    
		
    header('Content-Type: application/json');
		echo json_encode($response);
	}
  
 
  
  //------------------------------------------------------------------------------------------------------------------///
 //-------------------------------get Contest for a match by  match id-----------------------------------------------------///
  //$id1 = match id , $id2 = user id
  //------------------------------------------------------------------------------------------------------------------///
  
  function getMyContest($id1)
  {
    
    $token_result = array(); 
    $token_result = $this->GetTokenData();
    
//     print_r($token_result);
    if ($token_result['uId'] != 0)
     {

      $id2 = (int)$token_result['uId'];

      $contests=array();

      $contests_joined=$this->db->select('  pool_id as poolId , total_amount as prizePoolAmount , total_slots as totalNumberOfSpots ')
        ->from('tb_user_squads')
        ->join('tb_user_join_pool', 'tb_user_squads.user_squad_id = tb_user_join_pool.fk_user_squad_id','left')
        ->join('tb_prize_pool_list', 'tb_user_join_pool.fk_pool_id = tb_prize_pool_list.pool_id','left')
        ->join('tb_pool_types', 'tb_prize_pool_list.fk_pool_type_id = tb_pool_types.pool_type_id','left')
        ->where('tb_prize_pool_list.fk_match_id',$id1)
        ->where('fk_user_id',$id2)
        ->order_by('total_amount', 'DESC')
        ->order_by('fk_pool_id', 'ASC')
        ->group_by('fk_pool_id')
        ->get()->result_array();

      $contests = $contests_joined;

      foreach($contests_joined as $contest_key => $contest)
      {

        $contests[$contest_key]['poolId'] = (int)$contests[$contest_key]['poolId'];
        $contests[$contest_key]['prizePoolAmount'] = (float)$contests[$contest_key]['prizePoolAmount'];
        $contests[$contest_key]['totalNumberOfSpots'] = (int)$contests[$contest_key]['totalNumberOfSpots'];

        
        $leaderboard=$this->db->select('user_squad_id as userSquadId , user_squad_no as userSquadNo , total_squad_points as points , rank ')
          ->from('tb_user_join_pool')
          ->join('tb_user_squads','tb_user_join_pool.fk_user_squad_id = tb_user_squads.user_squad_id','left')
          ->where('fk_pool_id',$contest['poolId'])
          ->where('fk_user_id',$id2)
          ->order_by('total_squad_points', 'DESC')
          ->get()->result_array();
       
        foreach($leaderboard as $leaderboard_key => $leaderboard_1)
        {
          $leaderboard[$leaderboard_key]['userSquadId'] = (int)$leaderboard[$leaderboard_key]['userSquadId'];
          $leaderboard[$leaderboard_key]['userSquadNo'] = (int)$leaderboard[$leaderboard_key]['userSquadNo'];
          $leaderboard[$leaderboard_key]['points'] = (int)$leaderboard[$leaderboard_key]['points'];
          $leaderboard[$leaderboard_key]['rank'] = (int)$leaderboard[$leaderboard_key]['rank'];
        }
        $contests[$contest_key]['squadList'] = $leaderboard;
        
      }
      
      if($contests)
      {
         $response=array('status'=>true, 'message'=>'Success' , 'contestList'=>$contests);

      }
      else
      {
        $response=array('status'=>false, 'message'=>'Failed to Fetch Data','data'=>'');
      }
          
    }
       
       
    else
    {
        $response=array('status'=>false, 'message'=>$token_result[0]);
    }
  
    header('Content-Type: application/json');
    echo json_encode($response);
  }
   
  
  
  //------------------------------------------------------------------------------------------------------------------///
 //-------------------------------get Stream Links for a match by  match id-----------------------------------------------------///
  //$id1 = match id , $id2 = user id
  //------------------------------------------------------------------------------------------------------------------///
  
  function getStreamLinks($id1)
  {
    
    $token_result = array(); 
    $token_result = $this->GetTokenData();
    
//     print_r($token_result);
    if ($token_result['uId'] != 0)
     {

//       $id2 = (int)$token_result['uId'];

      $streams=array();

      $streams=$this->db->select(' link ')
        ->from('tb_streams')
        ->where('fk_match_id',$id1)
        ->where('link_type','public')
        ->get()->result_array();

      
      if($streams)
      {
         
        $public_link = $streams[0]['link'];
         $response=array('status'=>true, 'message'=>'Success' , 'streamLink'=>$public_link);

      }
      else
      {
        $response=array('status'=>false, 'message'=>'No Link','data'=>'');
      }
          
    }
       
       
    else
    {
        $response=array('status'=>false, 'message'=>$token_result[0]);
    }
  
    header('Content-Type: application/json');
    echo json_encode($response);
  }
   
   
  //------------------------------------------------------------------------------------------------------------------///
 //-------------------------------get Kill Feed for a match by  match id-----------------------------------------------------///
  //$id1 = match id , $id2 = user id
  //------------------------------------------------------------------------------------------------------------------///
  
  function getKillFeed($id1)
  {
    
    $token_result = array(); 
    $token_result = $this->GetTokenData();
    
//     print_r($token_result);
    if ($token_result['uId'] != 0)
     {

//       $id2 = (int)$token_result['uId'];

      $killFeed=array();
      $kills=array();

      $kills=$this->db->select(' fk_team_player_id_1 , fk_team_player_id_2 , action ')
        ->from('tb_kill_feeds')
        ->where('fk_match_id',$id1)
        ->order_by('kill_feed_id','DESC')
        ->get()->result_array();

      
      if($kills)
      {
        $i=1;
        foreach($kills as $kill_key => $kill)
        {
          $player_1 = $this->db->select('player_name')
            ->from('tb_team_players')
            ->join('tb_players','tb_team_players.fk_player_id = tb_players.player_id','left')
            ->where('team_player_id',$kill['fk_team_player_id_1'])
            ->get()->result_array();

           $player_1_name = $player_1[0]['player_name'];

            $player_2 = $this->db->select('player_name')
            ->from('tb_team_players')
            ->join('tb_players','tb_team_players.fk_player_id = tb_players.player_id','left')
            ->where('team_player_id',$kill['fk_team_player_id_2'])
            ->get()->result_array();

           $player_2_name = $player_2[0]['player_name'];

           $killFeed[$kill_key]['player_1'] = $player_1_name;
           $killFeed[$kill_key]['player_2'] = $player_2_name;
           $killFeed[$kill_key]['action'] = $kills[$kill_key]['action'];
          
          
           $i++;
         }

         $response=array('status'=>true, 'message'=>'Success' , 'streamLink'=>$killFeed);

      }
      else
      {
        $response=array('status'=>false, 'message'=>'Failed','data'=>'');
      }
          
    }
       
       
    else
    {
        $response=array('status'=>false, 'message'=>$token_result[0]);
    }
  
    header('Content-Type: application/json');
    echo json_encode($response);
  }
   
  
    
 //-----------------------------------------------------------------------------------------------------------------------------------///
//--------------------------- get Weekly leaderboard ----- list of users with most wins in corresponding weeks -----------------------/// 
//-------------------------------------------------------------------------------------------------------------------------------------/// 
  // $id1 = number of weeks
   //------------------------------------------------------------------------------------------------------------------///

  
   
  function getWeeklyLeaderBoard($id1)
  {
    
    
    $token_result = array(); 
      $token_result = $this->GetTokenData();

     
     if ($token_result['uId'] != 0)
       {

        $merged_result=array();

        $weeks_count = 0 ;
        for($i=1 ; $i <= ($id1 + 10) ; $i++)
        {

           
          $previous_week = strtotime("-$i week +1 day");

          $start_week = strtotime("last sunday midnight",$previous_week);
          $end_week = strtotime("next saturday",$start_week);

          $start_week = date("Y-m-d",$start_week);
          $end_week = date("Y-m-d",$end_week);

          if( $weeks_count < $id1)
          {

            $match_count = $this->db->select('match_id')
              ->from('tb_match_list')
              ->where('date_and_time <=',$end_week)
              ->where('date_and_time >=',$start_week)
              ->where('match_status',3)
              ->count_all_results();
            
            if( $match_count != 0 )
            {
              
            
                                           //echo $start_week.' '.$end_week ;
                                           // echo "\n";

            $leaderBoard=$this->db->select(' user_id as userId, username , media_url as mediaUrl , media_name as mediaName , media_ext as mediaExt')
            ->from('tb_match_list')
            ->join('tb_prize_pool_list', 'tb_match_list.match_id = tb_prize_pool_list.fk_match_id','left')
            ->join('tb_user_join_pool', 'tb_prize_pool_list.pool_id = tb_user_join_pool.fk_pool_id','left')
            ->join('tb_user_squads', 'tb_user_join_pool.fk_user_squad_id = tb_user_squads.user_squad_id','left')
            ->join('tb_users', 'tb_user_squads.fk_user_id = tb_users.user_id','left')
            ->join('tb_user_details', 'tb_users.user_id = tb_user_details.fk_user_id','left')
            ->join('tb_medias', 'tb_user_details.fk_user_dp_id = tb_medias.media_id','left')
            ->where('date_and_time <=',$end_week)
            ->where('date_and_time >=',$start_week)
            ->where('rank',1)
            ->group_by('user_id')
            ->get()->result_array();

            foreach ($leaderBoard as $key => $value)
            {
                                          //  foreach ($value as $key2 => $value2)
                                            //{
              $leaderBoard[$key]['profilePic'] = base_url().$leaderBoard[$key]['mediaUrl'].'/'.$leaderBoard[$key]['mediaName'].'_300x300'.$leaderBoard[$key]['mediaExt'];


                 $leaderBoard[$key]['numberOfWins']=$this->db->select(' fk_user_id ')
                  ->from('tb_match_list')
                  ->join('tb_prize_pool_list', 'tb_match_list.match_id = tb_prize_pool_list.fk_match_id','left')
                  ->join('tb_user_join_pool', 'tb_prize_pool_list.pool_id = tb_user_join_pool.fk_pool_id','left')
                  ->join('tb_user_squads', 'tb_user_join_pool.fk_user_squad_id = tb_user_squads.user_squad_id','left')
                  ->where('date_and_time <=',$end_week)
                  ->where('date_and_time >=',$start_week)
                  ->where('rank',1)
                  ->where('fk_user_id',$value['userId'])
                  ->count_all_results();   
                                           // }
            }

                                            //      print_r($leaderBoard);
                                             //     print_r(array_count_values($leaderBoard));
                                            /*      foreach ($data as $key => $row) 
                                                  {
                                                    $volume[$key]  = $row['volume'];
                                                    $edition[$key] = $row['edition'];
                                                  }
                                            */
                                            // as of PHP 5.5.0 you can use array_column() instead of the above code
            $user_id_value  = array_column($leaderBoard, 'fk_user_id');
            $user_id_count = array_column($leaderBoard, 'numberOfWins');

                                            // Sort the data with volume descending, edition ascending
                                            // Add $data as the last parameter, to sort by the common key
            array_multisort($user_id_count, SORT_DESC, $leaderBoard);


            $count_value = count($leaderBoard,0);  

            for($x = 1 ; $x <= $count_value ; $x++)
            {
              $leaderBoard[$x-1]['rank']=$x;
            }

            $leaderBoard_slice= array_slice($leaderBoard, 0, 7);

            foreach ($leaderBoard_slice as $key => $value)
            {
                                              //  foreach ($value as $key2 => $value2)
                                                //{
                 $sum_of_wins=$this->db->select_sum('amount_won')
                  ->from('tb_match_list')
                  ->join('tb_prize_pool_list', 'tb_match_list.match_id = tb_prize_pool_list.fk_match_id','left')
                  ->join('tb_user_join_pool', 'tb_prize_pool_list.pool_id = tb_user_join_pool.fk_pool_id','left')
                  ->join('tb_user_squads', 'tb_user_join_pool.fk_user_squad_id = tb_user_squads.user_squad_id','left')
                  ->where('date_and_time <=',$end_week)
                  ->where('date_and_time >=',$start_week)
                                            //            ->where('rank',1)
                  ->where('fk_user_id',$value['userId'])
                  ->get()->result_array();

              $leaderBoard_slice[$key]['amountWon'] = (float)$sum_of_wins[0]['amount_won'];
              $leaderBoard_slice[$key]['userId'] = (int)$leaderBoard_slice[$key]['userId'];


                                            // }
            }


                                            //      array_multisort($user_id_count, SORT_DESC, $user_id_value, SORT_ASC, $leaderBoard);

                                            /*   // Using the variable $arr for your array, you could do this:

                                                  $count = array();
                                                  foreach ($leaderBoard as $key => $value)
                                                  {
                                                      foreach ($value as $key2 => $value2)
                                                      {
                                                         // $index = $key2.'-'.$value2;
                                                          $index = $value2;
                                                          if (array_key_exists($index, $count))
                                                          {
                                                              $count[$index]++;
                                                              //$index++;
                                                          } 
                                                          else 
                                                          {
                                                              $count[$index] = 1;
                                                              //$index = 1;
                                                          }
                                                      }

                                                //      $leaderBoard2[$key]['count'] = $count[$index];
                                                  }
                                                  //print_r($count);

                                            */



            $weeklyLeaderBoard[$weeks_count]['dateFrom']=$start_week;
            $weeklyLeaderBoard[$weeks_count]['dateTo']=$end_week;
            $weeklyLeaderBoard[$weeks_count]['leaders']=array_slice($leaderBoard_slice, 0, 5);
            
            $weeks_count++;
              
            }
            
          }
          
        }

                                          /*
                                          echo $matchDate = $leaderBoard[0]['date_and_time'];
                                          echo "\n";
                                          echo $nextDate = date("Y-m-d H:i:s", strtotime("$matchDate -10 day"));
                                          echo "\n";
                                         */
        $merged_result=$weeklyLeaderBoard;

        if($merged_result)
          {

            $response=array('status'=>true, 'message'=>'Success', 'weeklyLeaderBoard'=>$merged_result);
          }
          else
          {
            $response=array('status'=>false, 'message'=>'Failed','data'=>'');
          }
       
       }

    else
    {
        $response=array('status'=>false, 'message'=>$token_result[0]);
    }
    

    header('Content-Type: application/json');
		echo json_encode($response);
	}
  
  
  
     
  //-------------------------------------------------------------------------------------------------------------------------///
//-------------------------------------------get winners for pools in recent matches ----------------------------------------------/// 
//------------------------------------------------- 10 matches ---- top 10 prize pools -------------------------------------------/// 
  //------------------------------------------------------------------------------------------------------------------///

 
  
   
  function getAllWinners()
  {
    
    $token_result = array(); 
      $token_result = $this->GetTokenData();

     
     if ($token_result['uId'] != 0)
       {

        $merged_result=array();

        $recent_matches=$this->db->select(' match_id as matchId, match_name as matchName ,tournament_name as tournamentName , tournament_id as tournamentId ')
          ->from('tb_match_list')
          ->join('tb_tournaments', 'tb_match_list.fk_tournament_id = tb_tournaments.tournament_id','left')
          ->where('match_status',3)
          ->order_by('date_and_time','DESC')
          ->get()->result_array();

        $recent_matches_slice = array_slice($recent_matches, 0, 10);

        $merged_result=$recent_matches_slice;


         foreach ($recent_matches_slice as $match_key => $recent_match)
          {
            $pool_details=$this->db->select(' pool_id as poolId , total_amount as prizePoolAmount , user_id as userId , username , media_url as mediaUrl , media_name as mediaName , media_ext as mediaExt , amount_won as amountWon')
              ->from('tb_pool_types')
              ->join('tb_prize_pool_list', 'tb_pool_types.pool_type_id = tb_prize_pool_list.fk_pool_type_id','left')
              ->join('tb_user_join_pool', 'tb_prize_pool_list.pool_id = tb_user_join_pool.fk_pool_id','left')
              ->join('tb_user_squads','tb_user_join_pool.fk_user_squad_id = tb_user_squads.user_squad_id','left')
              ->join('tb_users','tb_user_squads.fk_user_id = tb_users.user_id','left')
              ->join('tb_user_details','tb_users.user_id = tb_user_details.fk_user_id','left')
              ->join('tb_medias','tb_user_details.fk_user_dp_id = tb_medias.media_id','left')
              ->where('rank',1)
              ->where('tb_prize_pool_list.fk_match_id',$recent_match['matchId'])
              ->order_by('total_amount','DESC')
              ->get()->result_array();	

            $pool_details_slice = array_slice($pool_details, 0, 10);

            $count_value = count($pool_details_slice,0);  


          for ($x = 0; $x < $count_value; $x++) 
          { 
            $pool_details_slice[$x]['poolId'] = (int)$pool_details_slice[$x]['poolId'];
            $pool_details_slice[$x]['prizePoolAmount'] = (int)$pool_details_slice[$x]['prizePoolAmount'];
            $pool_details_slice[$x]['userId'] = (int)$pool_details_slice[$x]['userId'];
            $pool_details_slice[$x]['amountWon'] = (float)$pool_details_slice[$x]['amountWon'];
            $pool_details_slice[$x]['profilePic'] = base_url().$pool_details_slice[$x]['mediaUrl'].'/'.$pool_details_slice[$x]['mediaName'].'_300x300'.$pool_details_slice[$x]['mediaExt'];
  
          }


            $merged_result[$match_key]['matchId'] = (int)$merged_result[$match_key]['matchId'];
            $merged_result[$match_key]['contestDetails']=$pool_details_slice;

          }




        if($merged_result)
          {

            $response=array('status'=>true, 'message'=>'Success', 'winnersList'=>$merged_result);
          }
          else
          {
            $response=array('status'=>false, 'message'=>'Failed','data'=>'');
          }

       }

    else
    {
        $response=array('status'=>false, 'message'=>$token_result[0]);
    }
    
		
    header('Content-Type: application/json');
		echo json_encode($response);
    
    
  }
    
  
  
  
  //------------------------------------------------------------------------------------------------------------------///
  //-------------------------------     Refresh everything during a match     -----------------------------------------------------///
  //$id = match_id 
    //------------------------------------------------------------------------------------------------------------------///

 function pointRefresh($id){
		
                                                      // 	$data = array(
                                                      //    array(
                                                      //       'game_id' => 1 ,
                                                      // //       'game_name' => 'My Name 2' ,
                                                      //       'game_name' => 'PUBG Mobile'
                                                      //    ),
                                                      //    array(
                                                      //       'game_id' => 2 ,
                                                      // //       'name' => 'Another Name 2' ,
                                                      //       'game_name' => 'COD Mobile'
                                                      //    )
                                                      // );

                                                      // $this->db->update_batch('tb_games', $data, 'game_id');
     $tournament_details=$this->db->select('*')
			->from('tb_match_list')
      ->join('tb_tournaments','tb_match_list.fk_tournament_id = tb_tournaments.tournament_id','left')
			->where('match_id',$id)
			->get()->result_array();
       
   
    if($tournament_details)
      {
                                                  //  $response=array('status'=>true, 'message'=>'Success','data'=>'');
                                                  //         $match_name=$tournament_details[0]["match_name"];
                                                  //         $tournament_name=$tournament_details[0]["tournament_name"];
        $tournament_type=$tournament_details[0]["tournament_type"];
      }
    

   
   if($tournament_type==6000)                       //tournament type is 6000 Battle royale
    {

        $br_details=$this->db->select('*')
          ->from('tb_br_match_details')
                                                          //       ->join('tb_tournaments', 'tb_match_list.fk_tournament_id = tb_tournaments.tournament_id')
                                                          //       ->join('tb_medias','tb_tournaments.fk_logo_id = tb_medias.media_id')
          ->where('fk_match_id',$id)
          ->get()->result_array();

        $count_br_details = count($br_details,0);  

        for ($x = 0; $x < $count_br_details; $x++) 
          { 

            $player_position = $this->db->select('team_position')
              ->from('tb_teams_list')
              ->join('tb_final_team_list', 'tb_teams_list.teams_list_id = tb_final_team_list.fk_team_list_id','left')
              ->join('tb_br_match_details', 'tb_final_team_list.fk_team_player_id = tb_br_match_details.fk_team_player_id','left')
              ->where('tb_br_match_details.fk_match_id',$id)
              ->where('tb_br_match_details.fk_team_player_id',$br_details[$x]['fk_team_player_id'])
              ->get()->result_array();


            $br_details[$x]['position'] = (int)$player_position[0]['team_position'];      


            $position_points = 0 ;  

            if( $br_details[$x]['position'] == 0 )
            {
              $position_points = 0 ;  
            }
            elseif( $br_details[$x]['position'] > 10 )
            {
              $position_points = 0 ;  
            }
            else
            {
              $position_points = 22 - ( 2 * $br_details[$x]['position'] );
            }

            $br_details[$x]['points'] = $position_points + ( $br_details[$x]['kills'] * 10 ) + ( $br_details[$x]['knocks'] * 5 ) ;
            $br_details[$x]['points'] = (int)$br_details[$x]['points'];
          }

        $this->db->update_batch('tb_br_match_details', $br_details, 'br_match_details_id');



        $user_sqd_player_details=$this->db->select('user_squad_player_id , fk_user_squad_id , my_player_id , my_player_points ')
          ->from('tb_user_squad_players')
          ->join('tb_user_squads', 'tb_user_squad_players.fk_user_squad_id = tb_user_squads.user_squad_id','left')
                                                                      //       ->join('tb_medias','tb_tournaments.fk_logo_id = tb_medias.media_id')
          ->where('fk_match_id',$id)
          ->get()->result_array();

        $count_sqd_player = count($user_sqd_player_details,0);  

       for ($y = 0; $y < $count_sqd_player; $y++) 
          { 
            $player_point =  $this->db->select('points')
              ->from('tb_br_match_details')
              ->where('fk_match_id',$id)
              ->where('fk_team_player_id',$user_sqd_player_details[$y]['my_player_id'])
              ->get()->result_array();


            $user_sqd_player_details[$y]['my_player_points'] = $player_point[0]['points'];
          }

        $this->db->update_batch('tb_user_squad_players', $user_sqd_player_details, 'user_squad_player_id');

    }                              //ending of tournament type is 6000 Battle royale

   
   
   else //($tournament_type==7000)                       //tournament type is 7000 Multiplayer
    {


        $mp_details=$this->db->select('*')
          ->from('tb_mp_match_details')
                                                          //       ->join('tb_tournaments', 'tb_match_list.fk_tournament_id = tb_tournaments.tournament_id')
                                                          //       ->join('tb_medias','tb_tournaments.fk_logo_id = tb_medias.media_id')
          ->where('fk_match_id',$id)
          ->get()->result_array();

        $count_mp_details = count($mp_details,0);  

        for ($x = 0; $x < $count_mp_details; $x++) 
          { 

            $player_position = $this->db->select('team_position')
              ->from('tb_teams_list')
              ->join('tb_final_team_list', 'tb_teams_list.teams_list_id = tb_final_team_list.fk_team_list_id','left')
              ->join('tb_mp_match_details', 'tb_final_team_list.fk_team_player_id = tb_mp_match_details.fk_team_player_id','left')
              ->where('tb_mp_match_details.fk_match_id',$id)
              ->where('tb_mp_match_details.fk_team_player_id',$mp_details[$x]['fk_team_player_id'])
              ->get()->result_array();


            $mp_details[$x]['win_lose'] = (int)$player_position[0]['team_position'];      


            $position_points = 0 ;  

            if( $mp_details[$x]['win_lose'] == 1 )                    //win
            {
              $position_points = 15 ;  
            }
            elseif( $mp_details[$x]['win_lose'] == 2 )                //lose
            {
              $position_points = 5 ;  
            }
            else                                                      //tie
            {
              $position_points = 10;
            }

            $mp_details[$x]['points'] = $position_points + ( $mp_details[$x]['kills'] * 5 ) + ( $mp_details[$x]['assists'] * 2 ) - ( $mp_details[$x]['deaths'] * 1 ) ;
            $mp_details[$x]['points'] = (int)$mp_details[$x]['points'];
          }

        $this->db->update_batch('tb_mp_match_details', $mp_details, 'mp_match_details_id');



        $user_sqd_player_details=$this->db->select('user_squad_player_id , fk_user_squad_id , my_player_id , my_player_points ')
          ->from('tb_user_squad_players')
          ->join('tb_user_squads', 'tb_user_squad_players.fk_user_squad_id = tb_user_squads.user_squad_id','left')
                                                                      //       ->join('tb_medias','tb_tournaments.fk_logo_id = tb_medias.media_id')
          ->where('fk_match_id',$id)
          ->get()->result_array();

        $count_sqd_player = count($user_sqd_player_details,0);  

       for ($y = 0; $y < $count_sqd_player; $y++) 
          { 
            $player_point =  $this->db->select('points')
              ->from('tb_mp_match_details')
              ->where('fk_match_id',$id)
              ->where('fk_team_player_id',$user_sqd_player_details[$y]['my_player_id'])
              ->get()->result_array();


            $user_sqd_player_details[$y]['my_player_points'] = $player_point[0]['points'];
          }

        $this->db->update_batch('tb_user_squad_players', $user_sqd_player_details, 'user_squad_player_id');

    }                              //ending of tournament type is 7000 Multiplayer
   
   
   
   
   
   
    $usr_sqd_details=$this->db->select('*')
			->from('tb_user_squads')
                                                      //       ->join('tb_tournaments', 'tb_match_list.fk_tournament_id = tb_tournaments.tournament_id')
                                                      //       ->join('tb_medias','tb_tournaments.fk_logo_id = tb_medias.media_id')
      ->where('fk_match_id',$id)
  		->get()->result_array();
   
    $count_usr_sqd = count($usr_sqd_details,0);  

    for ($z = 0; $z < $count_usr_sqd; $z++) 
      { 
        $position_points = 0 ;  

        $igl_point =  $this->db->select('my_player_points')
          ->from('tb_user_squad_players')
          ->join('tb_user_squads', 'tb_user_squad_players.fk_user_squad_id = tb_user_squads.user_squad_id','left')
          ->where('fk_match_id',$id)
          ->where('my_player_id',$usr_sqd_details[$z]['igl_team_player_id'])
          ->get()->result_array();

        $squad_point =  $this->db->select_sum('my_player_points')
          ->from('tb_user_squad_players')
          ->where('fk_user_squad_id',$usr_sqd_details[$z]['user_squad_id'])
          ->get()->result_array();
//       echo "IGL : ";
//       print_r($igl_point);
//       echo "\n";
      
//       echo "Total : ";
//       print_r($squad_point);
//       echo "\n";

      
         $usr_sqd_details[$z]['total_squad_points'] = $igl_point[0]['my_player_points'] + $squad_point[0]['my_player_points'] ;
         $usr_sqd_details[$z]['total_squad_points'] = (int)$usr_sqd_details[$z]['total_squad_points'];
      }
   
     $this->db->update_batch('tb_user_squads', $usr_sqd_details, 'user_squad_id');
   
   
   
   
   
        
    $user_sqd = $this->db->select('*')
			->from('tb_user_squads')
//       ->join('tb_user_squads', 'tb_user_squad_players.fk_user_squad_id = tb_user_squads.user_squad_id')
                                                                  //       ->join('tb_medias','tb_tournaments.fk_logo_id = tb_medias.media_id')
      ->where('fk_match_id',$id)
      ->order_by('total_squad_points','DESC')
      ->get()->result_array();
          
   
   
                                                                  //       $br_details1=$this->db->select('player_name as playerName , team_name as teamName , kills as playerKills , knocks as playerKnocks, position as teamPosition, points as playerTotalPoints')
                                                                  // 			  ->from('tb_br_match_details')
                                                                  //         ->join('tb_team_players','tb_br_match_details.fk_team_player_id = tb_team_players.team_player_id','left')
                                                                  //         ->join('tb_players','tb_team_players.fk_player_id = tb_players.player_id','left')
                                                                  //         ->join('tb_teams','tb_team_players.fk_team_id = tb_teams.team_id','left')
                                                                  //         ->where('fk_match_id',$id)
                                                                  //         ->order_by('points','DESC')
                                                                  // 			  ->get()->result_array();
   
   
		if($user_sqd)
    {
//     return($user_sqd);
			$response=array('status'=>true, 'message'=>'Success','data'=>$user_sqd);
		}
    
		else
    {
			$response=array('status'=>false, 'message'=>'Failed','data'=>'');
		}
                                                                   ////return $match_list;
    header('Content-Type: application/json');
//    echo json_encode($response);
	}	  
  
  
//------------------------------------------------------------------------------------------------------------------///
  //-------------------------------    Refresh everything after a match      -----------------------------------------------------///
  //$id = match_id 
   //------------------------------------------------------------------------------------------------------------------///
 
 function pointRefresh2($id){
   
     $pools = array();
   
       $this->pointRefresh($id);
       
   $match_details = $this->db->select('match_status')
			->from('tb_match_list')
			->where('match_id',$id)
			->get()->result_array();
   
   if( $match_details[0]['match_status'] == 2 )
   {
      
                                                          //      $match_details=$this->db->select('*')
                                                          // 			->from('tb_match_list')
                                                          // //       ->join('tb_tournaments','tb_match_list.fk_tournament_id = tb_tournaments.tournament_id','left')
                                                          // 			->where('match_id',$id)
                                                          // 			->get()->result_array();


                                                          //     if($tournament_details)
                                                          //       {
                                                          //                                                   //  $response=array('status'=>true, 'message'=>'Success','data'=>'');
                                                          //         $match_status=$tournament_details[0]["match_status"];
                                                          //                                                   //         $tournament_name=$tournament_details[0]["tournament_name"];
                                                          //                                                   //         $tournament_type=$tournament_details[0]["tournament_type"];
                                                          //       }

                                                          //    if($match_status == 2)
                                                          //    {
           
     $pools=$this->db->select(' pool_id , fk_pool_type_id')
			->from('tb_prize_pool_list')
                                                        //       ->join('tb_tournaments','tb_match_list.fk_tournament_id = tb_tournaments.tournament_id','left')
			->where('fk_match_id',$id)
			->where('reg_slots >',0)
			->get()->result_array();
     
     
      $count_pools = count($pools,0);  

      for ($x = 0; $x < $count_pools; $x++) 
      { 

         $leaderboard = $this->db->select(' user_pool_id , fk_user_squad_id , fk_user_id , total_squad_points , amount_won , rank')
        ->from('tb_user_join_pool')
        ->join('tb_user_squads','tb_user_join_pool.fk_user_squad_id = tb_user_squads.user_squad_id','left')
                                                        //         ->join('tb_users','tb_user_squads.fk_user_id = tb_users.user_id','left')
   			->where('fk_pool_id',$pools[$x]['pool_id'])
        ->order_by('total_squad_points', 'DESC')
        ->get()->result_array();
        
        $count_leaderboard = count($leaderboard,0);  
        
        for ($y = 0; $y < $count_leaderboard; $y++) 
        { 
          $leaderboard[$y]['rank'] = $y+1;
        }   

        for ($y = 0; $y < ($count_leaderboard-1); $y++) 
        { 
           if($leaderboard[$y]['total_squad_points'] == $leaderboard[$y+1]['total_squad_points'])
           {
             $leaderboard[$y+1]['rank'] = $leaderboard[$y]['rank'];
           }
        }
        
                                                                                  //         echo "\n";
                                                                                  //         echo "count_leaderboard : ";
                                                                                  //         echo $count_leaderboard;
                                                                                  //         echo "\n";
        
           
//           $amount_won = array();
          
             $amount_won = $this->db->select('start_slot , end_slot , amount')
              ->from('tb_prize_distribution')
              ->where('fk_pool_type_id',$pools[$x]['fk_pool_type_id'])
//               ->where('start_slot >=',$y+1)
//               ->where('end_slot <=',$y+1)
              ->get()->result_array();
        
            $count_amount_won = count($amount_won,0);  

//              echo "\n";
//              echo $y;
//              echo "\n";
//               print_r($amount_won);       

        for ($y = 0; $y < $count_leaderboard; $y++) 
        { 
            for ($i = 0; $i < $count_amount_won; $i++) 
            {
               if( ($y+1 >= $amount_won[$i]['start_slot']) && ($y+1 <= $amount_won[$i]['end_slot']) ) 
               {
                  $leaderboard[$y]['amount_won'] = (float)$amount_won[$i]['amount'];
                  break;
               }
            }
        }
        
        for ($y = 1; $y < $count_leaderboard; $y++) 
        { 
           $same_rank_count = 1;
           $end = $y-1; 
           $start = $y-1;
           $prize_amount = (float)$leaderboard[$start]['amount_won'];
           while(($y+1) != $leaderboard[$y]['rank'])
           {
             $prize_amount = (float)$prize_amount + (float)$leaderboard[$y]['amount_won']; 
             $same_rank_count++;
             $end = $y; 
             $y++;
             if($y == $count_leaderboard)
             {
               break;
             }
           }
            
          $new_amount = (float)$prize_amount / $same_rank_count ;
          for ($z = $start; $z <= $end; $z++) 
          {
            $leaderboard[$z]['amount_won'] = (float)$new_amount;
          } 
        
        }
        
        $leaderboard1 = array();
        $leaderboard1 = null;
        
        for($j=0 ; $j < $count_leaderboard ; $j++)
        {
            $leaderboard1[$j]['user_pool_id'] = $leaderboard[$j]['user_pool_id'];
            $leaderboard1[$j]['fk_user_squad_id'] = $leaderboard[$j]['fk_user_squad_id'];
            $leaderboard1[$j]['amount_won'] = $leaderboard[$j]['amount_won'];
            $leaderboard1[$j]['rank'] = $leaderboard[$j]['rank'];
          
            $amount = $leaderboard[$j]['amount_won'];
            $user_id = $leaderboard[$j]['fk_user_id'];
          
            $wallet_details=$this->db->select('wallet_balance')
              ->from('tb_user_wallet')
              ->where('fk_user_id',$user_id)
              ->get()->result_array();

            $wallet_balance = $wallet_details[0]['wallet_balance'];

            $wallet_balance = $wallet_balance + $amount;

            $wallet_balance_update = array(
                'wallet_balance' => $wallet_balance
            );

            $this->db->where('fk_user_id', $user_id);
            $this->db->update('tb_user_wallet', $wallet_balance_update);
          
            $transaction_no = uniqid('walletp_',true);      

            $transaction = array(
              'transaction_no' => $transaction_no,
              'amount' => $amount,
              'type' => 'Wallet Credit',
              'txn_date_time' => date("Y-m-d H:i:s"),
              'fk_user_id' => $user_id,
              'remarks' => 'Amount '. (float)$amount .' added to wallet. ');
            $this->db->insert('tb_transactions', $transaction); 
          
        }
//         $user_pool_id_array = array_column($leaderboard, 'user_pool_id');
//         $fk_user_squad_id_array = array_column($leaderboard, 'fk_user_squad_id');
//         $amount_won_array = array_column($leaderboard, 'amount_won' );
//         $rank_array = array_column($leaderboard, 'rank' );
//         $leaderboard1  = array_combine( $user_pool_id_array , $fk_user_squad_id_array , $amount_won_array , $rank_array);
        $pools[$x]['leaderboard_list'] = $leaderboard1;

//         echo "value of x is : ".$x."\n";
//         print_r($pools[$x]);

        $this->db->update_batch('tb_user_join_pool', $leaderboard1, 'user_pool_id');

      }
    
      $match_details_update = array(
                'match_status' => 3
            );

            $this->db->where('match_id', $id);
            $this->db->update('tb_match_list', $match_details_update);
          
       
//      $user_sqd=$this->db->select('player_name , position ')
// 			->from('tb_br_match_details')
//       ->join('tb_team_players', 'tb_br_match_details.fk_team_player_id = tb_team_players.team_player_id')
//       ->join('tb_players','tb_team_players.fk_player_id = tb_players.player_id')
//       ->where('tb_br_match_details.fk_match_id',$id)
//   		->get()->result_array();      
   
   
   
   		if($pools)
      {
        $response=array('status'=>true, 'message'=>'Success','data'=>$pools);
      }

      else
      {
        $response=array('status'=>false, 'message'=>'Failed');
      }
   }
   else if($match_details[0]['match_status'] < 2)
   {
			$response=array('status'=>false, 'message'=>'Match is still Ongoing');
   }
   else
   {
			$response=array('status'=>false, 'message'=>'Match already Finished and Updated');
   }
   ////return $match_list;
//     header('Content-Type: application/json');
    echo json_encode($response);
	}	 
  
  
  
  //------------------------------------------------------------------------------------------------------------------///
  //-------------------------------   for storing created user squad        -----------------------------------------------------///
    //------------------------------------------------------------------------------------------------------------------///

 
  function createSquad()
	{
//     $data = '{
//    "selectionList": [
//       {
//          "playersList": [
//             {
//                "teamPlayerId": 1,
//                "playerId": 8,
//                "playerName": "Dronzer Baca",
//                "brRole": "Assualt",
//                "mpRole": "Left",
//                "playerCredit": 6,
//                "teamName": "Baca",
//                "isPlayerAdded": false,
//                "isIGL": false
//             },
//             {
//                "teamPlayerId": 2,
//                "playerId": 8,
//                "playerName": "Vikraman Baca",
//                "brRole": "Assualt",
//                "mpRole": "Left",
//                "playerCredit": 6,
//                "teamName": "Baca",
//                "isPlayerAdded": false,
//                "isIGL": false
//             }
//          ],
//          "teamId": 21,
//          "teamName": "Baca"
//       },
//       {
//          "playersList": [
//             {
//                "teamPlayerId": 9,
//                "playerId": 8,
//                "playerName": "Blind Satan",
//                "brRole": "Assualt",
//                "mpRole": "Left",
//                "playerCredit": 6,
//                "teamName": "Blind",
//                "isPlayerAdded": false,
//                "isIGL": false
//             }
//          ],
//          "teamId": 22,
//          "teamName": "Blind"
//       },
//       {
//          "playersList": [
//             {
//                "teamPlayerId": 16,
//                "playerId": 8,
//                "playerName": "Rare Player 1",
//                "brRole": "Assualt",
//                "mpRole": "Left",
//                "playerCredit": 6,
//                "teamName": "Rare",
//                "isPlayerAdded": false,
//                "isIGL": true
//             }
//          ],
//          "teamId": 24,
//          "teamName": "Rare"
//       }
//    ],
//    "matchId": 1
// }';
    
    
//     $input_data = json_decode($data , true);
//     $user_id = 1;

    $input_data = json_decode(trim(file_get_contents('php://input')), true);
    
    
    
    $token_result = array(); 
    
    $token_result = $this->GetTokenData();
    $igl_team_player_id=0;

    if ($token_result['uId'] != 0)
    {

      $user_id = (int)$token_result['uId'];

      $match_id = $input_data['matchId'] ;
                                                              //    $json_str = file_get_contents('php://input');
                                                              //     $json_str = file_get_contents('php://pastie.org/p/5gwT78JhW3Wm94w4tmB7rt');

                                                              # Get as an object
                                                              //     $json_obj = json_decode($json_str);
                                                              //     print_r($input_data);
      if( $match_id == null)
      {
        $response=array('status'=>false, 'message'=>'Match Id is null ,  Failed to Create Squad','data'=>$input_data);
      }
      else
      {
        
        $upload_squad_2 = array();        //for 2nd table data storage

        $current_squad_no=$this->db->select_max('user_squad_no')
          ->from('tb_user_squads')
          ->where('fk_match_id',$match_id)
          ->where('fk_user_id',$user_id)
          ->count_all_results();     

        $count_input_data = count($input_data['selectionList'],0);  


        for($i=0 ; $i < $count_input_data ; $i++)
        {
          $input_selection = $input_data['selectionList'][$i];
          $count_selection = count($input_selection,0);  

          $count_players = count($input_selection['playersList'],0); 
            for($k=0 ; $k < $count_players ; $k++)
            {
              if($input_selection['playersList'][$k]['isIGL'] == true)
              {
                $igl_team_player_id = $input_selection['playersList'][$k]['teamPlayerId'];
              }
            }
        }



        $upload_squad_1 = array(
            'user_squad_no'	=>	($current_squad_no + 1),
            'igl_team_player_id'		=>	$igl_team_player_id,
            'fk_match_id'	=>	$match_id,
            'fk_user_id'	=>	$user_id
            );

        $this->db->insert('tb_user_squads', $upload_squad_1);

        $user_squad_id=$this->db->select('user_squad_id')
          ->from('tb_user_squads')
          ->where('fk_match_id',$match_id)
          ->where('fk_user_id',$user_id)
          ->where('user_squad_no',$current_squad_no + 1)
          ->where('igl_team_player_id',$igl_team_player_id)
          ->get()->result_array();

        $current_user_squad_id = $user_squad_id[0]['user_squad_id'];


        for($i=0 ; $i < $count_input_data ; $i++)
        {
          $input_selection = $input_data['selectionList'][$i];
          $count_selection = count($input_selection,0);  

          $count_players = count($input_selection['playersList'],0); 
          for($k=0 ; $k < $count_players ; $k++)
          {
            $upload_squad_2 = array(
              'my_player_id'	=>	$input_selection['playersList'][$k]['teamPlayerId'],
              'fk_user_squad_id'		=>	$current_user_squad_id
              );

            $this->db->insert('tb_user_squad_players', $upload_squad_2);
          }
        }

        if($current_user_squad_id)
        {
          $response=array('status'=>true, 'message'=>'Squad Created Successfully');
        }

        else
        {
          $response=array('status'=>false, 'message'=>'Failed to Create Squad');
        }
         
      }
        ////return $match_list;

    }

    else
    {
        $response=array('status'=>false, 'message'=>$token_result[0]);
    }
    
    header('Content-Type: application/json');
    echo json_encode($response);
    
    
  }

  //------------------------------------------------------------------------------------------------------------------///
  //-------------------------------   for updating user squad        -----------------------------------------------------///
    //------------------------------------------------------------------------------------------------------------------///

 
    function updateSquad()
    {
  /*     $data = '{
     "selectionList": [
        {
           "playersList": [
              {
                 "teamPlayerId": 1,
                 "playerId": 8,
                 "playerName": "Dronzer Baca",
                 "brRole": "Assualt",
                 "mpRole": "Left",
                 "playerCredit": 6,
                 "teamName": "Baca",
                 "isPlayerAdded": false,
                 "isIGL": false
              },
              {
                 "teamPlayerId": 2,
                 "playerId": 8,
                 "playerName": "Vikraman Baca",
                 "brRole": "Assualt",
                 "mpRole": "Left",
                 "playerCredit": 6,
                 "teamName": "Baca",
                 "isPlayerAdded": false,
                 "isIGL": false
              }
           ],
           "teamId": 21,
           "teamName": "Baca"
        },
        {
           "playersList": [
              {
                 "teamPlayerId": 9,
                 "playerId": 8,
                 "playerName": "Blind Satan",
                 "brRole": "Assualt",
                 "mpRole": "Left",
                 "playerCredit": 6,
                 "teamName": "Blind",
                 "isPlayerAdded": false,
                 "isIGL": false
              }
           ],
           "teamId": 22,
           "teamName": "Blind"
        },
        {
           "playersList": [
              {
                 "teamPlayerId": 16,
                 "playerId": 8,
                 "playerName": "Rare Player 1",
                 "brRole": "Assualt",
                 "mpRole": "Left",
                 "playerCredit": 6,
                 "teamName": "Rare",
                 "isPlayerAdded": false,
                 "isIGL": true
              }
           ],
           "teamId": 24,
           "teamName": "Rare"
        }
     ],
     "matchId": 1,
     "userSquadId": 1
  }'; */
      
      
  //     $input_data = json_decode($data , true);
  //     $user_id = 1;
  
      $input_data = json_decode(trim(file_get_contents('php://input')), true);
      
      
      
      $token_result = array(); 
      
      $token_result = $this->GetTokenData();
      $igl_team_player_id=0;
  
      if ($token_result['uId'] != 0)
      {
  
        $user_id = (int)$token_result['uId'];
  
        $match_id = $input_data['matchId'] ;
        $user_squad_id = $input_data['userSquadId'] ;
                                                                //    $json_str = file_get_contents('php://input');
                                                                //     $json_str = file_get_contents('php://pastie.org/p/5gwT78JhW3Wm94w4tmB7rt');
  
                                                                # Get as an object
                                                                //     $json_obj = json_decode($json_str);
                                                                //     print_r($input_data);
        if( $match_id == null)
        {
          $response=array('status'=>false, 'message'=>'Match Id is null ,  Failed to Update Squad','data'=>$input_data);
        }
        else
        {
         
          $current_squad_players = array();        
          $new_squad_players = array();        
          $current_squad_flag = array();       
          $new_squad_flag = array();       
        
          /* $current_squad_no=$this->db->select_max('user_squad_no')
            ->from('tb_user_squads')
            ->where('fk_match_id',$match_id)
            ->where('fk_user_id',$user_id)
            ->count_all_results();      */
    
          $count_input_data = count($input_data['selectionList'],0);  
    
          $s2 = 0;
          for($i=0 ; $i < $count_input_data ; $i++)
          {
            $input_selection = $input_data['selectionList'][$i];
            $count_selection = count($input_selection,0);  
    
            $count_players = count($input_selection['playersList'],0); 
              for($k=0 ; $k < $count_players ; $k++)
              {
                $new_squad_players[$s2] = $input_selection['playersList'][$k]['teamPlayerId'];
                $s2++;
                if($input_selection['playersList'][$k]['isIGL'] == true)
                {
                  $igl_team_player_id = $input_selection['playersList'][$k]['teamPlayerId'];
                }
              }
          }
    
          $current_squad_players =  $this->db->select('my_player_id')
          ->from('tb_user_squad_players')
          ->where('fk_user_squad_id',$user_squad_id)
          ->get()->result_array();

          $size_of_current = sizeof($current_squad_players);
          for($s1 = 0 ; $s1 < $size_of_current ; $s1++){
            $current_squad_flag[$s1] = 0;       
            $new_squad_flag[$s1] = 0;
          }
    
          $update_squad_1 = array(
              'igl_team_player_id'		=>	$igl_team_player_id
              );
          $this->db->where('user_squad_id', $user_squad_id);
          $this->db->update('tb_user_squads', $update_squad_1);
    
          /* $user_squad_id=$this->db->select('user_squad_id')
            ->from('tb_user_squads')
            ->where('fk_match_id',$match_id)
            ->where('fk_user_id',$user_id)
            ->where('user_squad_no',$current_squad_no + 1)
            ->where('igl_team_player_id',$igl_team_player_id)
            ->get()->result_array();
    
          $current_user_squad_id = $user_squad_id[0]['user_squad_id']; */
    
          $s3 = 1;
          for($i=0 ; $i < $size_of_current ; $i++)
          {
            for($j=0 ; $j < $size_of_current ; $j++){
              if( $current_squad_players[$i]['my_player_id'] == $new_squad_players[$j] ){
                $current_squad_flag[$i] = 1;
                $new_squad_flag[$j] = 1;
                $s3++;
              }
            }
          }
          if($s3 == $size_of_current){
            $response=array('status'=>false, 'message'=>'same squad');
          }
          else{
            for($i=0 ; $i < $size_of_current ; $i++)
            {

              // echo "new sqd <pre>"; print_r($new_squad_players[$j]); echo "</pre>";
              // echo "<br/>current <pre>"; print_r($current_squad_players); echo "</pre>"; exit;
              for($j=0 ; $j < $size_of_current ; $j++){
                if( ($current_squad_flag[$i] == 0) && ($new_squad_flag[$j] == 0 )){
                  $current_squad_flag[$i] = 1;
                  $new_squad_flag[$j] = 1;
              
                
                  $update_new_squad_player = array(
                    'my_player_id'	=> $new_squad_players[$j]	
                    );
        
                  $this->db->where('my_player_id', $current_squad_players[$i]['my_player_id']);
                  $this->db->where('fk_user_squad_id', $user_squad_id);
                  $res = $this->db->update('tb_user_squad_players', $update_new_squad_player);
                }
              }
            }
          
          
            
            
            
            if($res)
            {
              $response=array('status'=>true, 'message'=>'Squad Updated Successfully');
            }
            
            else
            {
              $response=array('status'=>false, 'message'=>'Failed to Update Squad');
            }
          }
          
        }
          ////return $match_list;
  
      }
  
      else
      {
          $response=array('status'=>false, 'message'=>$token_result[0]);
      }
      
      header('Content-Type: application/json');
      echo json_encode($response);
      
      
    }
  
 //------------------------------------------------------------------------------------------------------------------///
  //-------------------------------   for clone user squad        -----------------------------------------------------///
  //-------------------------------   $id  = user_squad_id        -----------------------------------------------------///
    //------------------------------------------------------------------------------------------------------------------///

 
  function cloneSquad($id)
	{

    
    
    $token_result = array(); 
    $token_result = $this->GetTokenData();
    
    $igl_team_player_id=0;
    $user_squad_id = $id;


    if ($token_result['uId'] != 0)
    {

      $user_id = (int)$token_result['uId'];

      if( $user_squad_id == null)
      {
        $response=array('status'=>false, 'message'=>'Squad Id is null ,  Failed to Clone Squad');
      }
      else
      {
       
        $match_details=$this->db->select('*')
          ->from('tb_user_squads')
          ->where('user_squad_id',$user_squad_id)
          ->get()->result_array();

        $match_id = $match_details[0]['fk_match_id'];
        $igl_team_player_id = $match_details[0]['igl_team_player_id'];

        $upload_squad_2 = array();        //for 2nd table data storage

        $current_squad_no=$this->db->select_max('user_squad_no')
          ->from('tb_user_squads')
          ->where('fk_match_id',$match_id)
          ->where('fk_user_id',$user_id)
          ->count_all_results();     


        $upload_squad_1 = array(
            'user_squad_no'	=>	($current_squad_no + 1),
            'igl_team_player_id'		=>	$igl_team_player_id,
            'fk_match_id'	=>	$match_id,
            'fk_user_id'	=>	$user_id
            );

        $this->db->insert('tb_user_squads', $upload_squad_1);

        $new_squad_id=$this->db->select('user_squad_id')
          ->from('tb_user_squads')
          ->where('fk_match_id',$match_id)
          ->where('fk_user_id',$user_id)
          ->where('user_squad_no',$current_squad_no + 1)
          ->where('igl_team_player_id',$igl_team_player_id)
          ->get()->result_array();

        $new_user_squad_id = $new_squad_id[0]['user_squad_id'];

        
        $current_squad_players = $this->db->select('*')
          ->from('tb_user_squad_players')
          ->where('fk_user_squad_id',$user_squad_id)
          ->get()->result_array();

        foreach ($current_squad_players as $key => $player) {
        
            $upload_squad_2 = array(
              'my_player_id'	=>	$player['my_player_id'],
              'fk_user_squad_id'		=>	$new_user_squad_id
              );

            $this->db->insert('tb_user_squad_players', $upload_squad_2);
        }

        if($new_user_squad_id)
        {
          $response=array('status'=>true, 'message'=>'Squad Cloned Successfully');
        }

        else
        {
          $response=array('status'=>false, 'message'=>'Failed to Clone Squad');
        }
         
      }
        ////return $match_list;

    }

    else
    {
        $response=array('status'=>false, 'message'=>$token_result[0]);
    }
    
    header('Content-Type: application/json');
    echo json_encode($response);
    
    
  }
  
  //------------------------------------------------------------------------------------------------------------------///
  //-------------------------------   to join a contest       -----------------------------------------------------///
    //------------------------------------------------------------------------------------------------------------------///

    
	  public function userJoinContest()
	{

//     $data = '{
//                   "poolId": 28,
//                   "userSquadId": 22
//               }';

  
//     $input_data = json_decode($data , true);

     
      $token_result = array(); 
      $token_result = $this->GetTokenData();

     
     if ($token_result['uId'] != 0)
       {
          
        $user_id = (int)$token_result['uId'];
//         $user_id = 4;

        $input_data = json_decode(trim(file_get_contents('php://input')), true);

        $pool_id = $input_data['poolId'] ;
        $user_squad_id = $input_data['userSquadId'] ;
    //     $reg_slot = 0;
    //     $total_slot = 0;
    //     $reg_fee = 0;
        $pool_details=$this->db->select('reg_slots , total_slots , reg_fee , max_entry')
          ->from('tb_prize_pool_list')
          ->join('tb_pool_types','tb_prize_pool_list.fk_pool_type_id = tb_pool_types.pool_type_id','left')
          ->where('pool_id',$pool_id)
          ->get()->result_array();
                                                              //       ->count_all_results();     
    //     print_r($pool_details);
      $count_pools = count($pool_details,0);  

       if($count_pools != 0)
       {
       
          $reg_slot = $pool_details[0]['reg_slots'];
          $total_slot = $pool_details[0]['total_slots'];
          $reg_fee = $pool_details[0]['reg_fee'];
          $max_entry = $pool_details[0]['max_entry'];

         $joined_pool_count=$this->db->select('user_pool_id')
          ->from('tb_user_join_pool')
          ->join('tb_user_squads','tb_user_join_pool.fk_user_squad_id = tb_user_squads.user_squad_id','left')
          ->where('fk_pool_id',$pool_id)
          ->where('fk_user_id',$user_id)
          ->count_all_results();     
//           ->get()->result_array();

         if($joined_pool_count >= $max_entry)
         {
             $response=array('status'=>false, 'message'=>'Maximum Joining Limit Reached');
         }
      //      echo 'reg slots before : '.$reg_slot;
          else
          {
          $wallet_details=$this->db->select('*')
            ->from('tb_user_wallet')
            ->where('fk_user_id',$user_id)
            ->get()->result_array();

           $wallet_balance = $wallet_details[0]['wallet_balance'];

      //      echo 'reg wallet_balance before : '.$wallet_balance;


          if( $reg_fee > $wallet_balance)
          {
            $response=array('status'=>false, 'message'=>'Not enough balance in the wallet. Please Add money to wallet');
          } 
      //     $id_no = 1;
      //     $user_name_count = 1;
          elseif( $reg_slot >= $total_slot)
          {
            $response=array('status'=>false, 'message'=>'Contest is full. Please join another Contest');
          }
          else
          {
               $join_pool = array(
                    'fk_pool_id' => $pool_id,
                    'fk_user_squad_id' => $user_squad_id
                );
                $this->db->insert('tb_user_join_pool', $join_pool); 

               $user_pool=$this->db->select('user_pool_id')
                  ->from('tb_user_join_pool')
                  ->where('fk_pool_id',$pool_id)
                  ->where('fk_user_squad_id',$user_squad_id)
                  ->get()->result_array();

                $user_pool_id = $user_pool[0]['user_pool_id'];

                $reg_slot++;

                $reg_slot_update = array(
                    'reg_slots' => $reg_slot
                );

                $this->db->where('pool_id', $pool_id);
                $this->db->update('tb_prize_pool_list', $reg_slot_update); 

      //           echo 'reg slots after : '.$reg_slot;


                $transaction_no = uniqid('walletm_',true);      

                $transaction = array(
                    'transaction_no' => $transaction_no,
                    'amount' => $reg_fee,
                    'type' => 'Wallet Debit',
                    'txn_date_time' => date("Y-m-d H:i:s"),
                    'fk_user_id' => $user_id,
                    'remarks' => 'joined pool : '.$pool_id .' , user join pool id : '.$user_pool_id.' , using squad id : '.$user_squad_id
                );
                $this->db->insert('tb_transactions', $transaction); 

                $transaction_details=$this->db->select('*')
                  ->from('tb_transactions')
                  ->where('transaction_no',$transaction_no)
                  ->get()->result_array();

                $wallet_balance = (int)$wallet_balance - (int)$reg_fee ;

                $wallet_balance_update = array(
                    'wallet_balance' => $wallet_balance
                );

                $this->db->where('fk_user_id', $user_id);
                $this->db->update('tb_user_wallet', $wallet_balance_update); 

      //           echo 'reg wallet_balance after : '.$wallet_balance;


      //           $response=array('status'=>true, 'message'=>'Contest Joined Successfully' , 'user_pool_id'=>$user_pool_id , 'tokens'=>$transaction_details);
                $response=array('status'=>true, 'message'=>'Contest Joined Successfully');
              }
            }
          }
       else
       {
           $response=array('status'=>false, 'message'=>'Invalid Pool ID');
       }
       
     }
     
      else
      {
          $response=array('status'=>false, 'message'=>$token_result[0]);
      }
    
    
    header('Content-Type: application/json');
    echo json_encode($response);
  
  }
  
  
  
  //------------------------------------------------------------------------------------------------------------------///
  //-------------------------------   to join a contest       -----------------------------------------------------///
    //------------------------------------------------------------------------------------------------------------------///

  
  
  
  
  
  
  
}