<?php defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/ImplementJwt.php';
require_once(APPPATH."libraries/razorpay-php/Razorpay.php");
use Razorpay\Api\Api;
use Razorpay\Api\Errors\SignatureVerificationError;


class Payment extends CI_Controller
{
  
  public function __construct()
	{
		parent::__construct();
    $this->objOfJwt = new ImplementJwt();
//     header('Content-Type: application/json');
    date_default_timezone_set('Asia/Kolkata');
  }
  
  //------------------------------------------------------------------------------------------------------------------///

    //////// get data from token ////////////
        
    public function GetTokenData()
    {
    $received_Token = $this->input->request_headers('Authorization');
    $token_array = explode(" ",$received_Token['Authorization']);
      try
            {
             $jwtData = $this->objOfJwt->DecodeToken($token_array[1]);
             
             $decrypted_string=openssl_decrypt($jwtData['uId'],"AES-128-ECB",passkey);
             $jwtData['uId'] = (int)$decrypted_string;
            return($jwtData);
            }
            catch (Exception $e)
            {
            echo json_encode(array( "status" => false, "message" => $e->getMessage()));exit;
            }
    }

  //------------------------------------------------------------------------------------------------------------------///
  //------------------------------------------------------------------------------------------------------------------///

 /**
   * This function creates order and loads the payment methods
   */
  public function pay()
  {
   
    $token_result = array();
    $token_result = $this->GetTokenData();
 
    if ($token_result['uId'] != 0)
    {
      $id2 = (int)$token_result['uId'];
      $input_data = json_decode(trim(file_get_contents('php://input')), true);

      $_SESSION['payable_amount'] = $input_data['amount'] ;

      $api = new Api(RAZOR_KEY_ID, RAZOR_KEY_SECRET);
      /**
       * You can calculate payment amount as per your logic
       * Always set the amount from backend for security reasons
       */
//       $_SESSION['payable_amount'] = $_POST['amount'];
      $razorpayOrder = $api->order->create(array(
        'receipt'         => rand(),
        'amount'          => $_SESSION['payable_amount'] * 100, // 2000 rupees in paise
        'currency'        => 'INR',
        'payment_capture' => 1 // auto capture
      ));
      $amount = $razorpayOrder['amount'];
      $razorpayOrderId = $razorpayOrder['id'];
      $_SESSION['razorpay_order_id'] = $razorpayOrderId;
//       $data = $this->prepareData($amount,$razorpayOrderId);
//       $this->load->view('rezorpay',array('data' => $data));

      $response=array('status'=>true, 'message'=>'Order Created' , 'razorpayOrderId' => $razorpayOrderId , 'amount' => $amount);
      
    }

    else
    {
        $response=array('status'=>false, 'message'=>$token_result[0]);
    }
    header('Content-Type: application/json');
    echo json_encode($response);
  }

  //------------------------------------------------------------------------------------------------------------------///

  /**
   * This function verifies the payment,after successful payment
   */
  public function verify()
  {
    $token_result = array();
    $token_result = $this->GetTokenData();
 
    if ($token_result['uId'] != 0)
    {
      $id2 = (int)$token_result['uId'];
      $input_data = json_decode(trim(file_get_contents('php://input')), true);
      
//       $amount = $_SESSION['payable_amount'];  
//       $amount = $input_data['amount'] ;  

      
      $_SESSION['razorpay_order_id']=$input_data['razorpay_order_id'] ;
      $razorpay_order_id = $input_data['razorpay_order_id'] ;
      $razorpay_payment_id = $input_data['razorpay_payment_id'] ;
      $razorpay_signature = $input_data['razorpay_signature'] ;

      $order_count=$this->db->select('razor_transaction_id')
      ->from('tb_razor_transactions')
      ->where('razorpay_order_id',$razorpay_order_id)
      ->count_all_results();
      
      if($order_count == 0)
      {

        $api = new Api(RAZOR_KEY_ID, RAZOR_KEY_SECRET);
        $order = $api->order->fetch($razorpay_order_id);

  //       print_r($order);

        $amount = $order['amount'] / 100;
        $_SESSION['payable_amount'] = $amount;


        $success = true;
        $error = "payment_failed";
        if (empty($_POST['razorpay_payment_id']) === false) {
          $api2 = new Api(RAZOR_KEY_ID, RAZOR_KEY_SECRET);
        try {
            $attributes = array(
              'razorpay_order_id' => $razorpay_order_id,
              'razorpay_payment_id' => $razorpay_payment_id,
              'razorpay_signature' => $razorpay_signature
            );
            $api2->utility->verifyPaymentSignature($attributes);
          } catch(SignatureVerificationError $e) {
            $success = false;
            $error = 'Razorpay_Error : ' . $e->getMessage();
          }
        }
        if ($success === true) {
          /**
           * Call this function from where ever you want
           * to save save data before of after the payment
           */
         $this->setRegistrationData($id2);

         $response=array('status'=>true, 'message'=>'Payment Successful. Amount '. $amount .' Added to wallet' , 'amount' => $amount);
  //         redirect(base_url().'payment/payment_test/success');
        }
        else {
  //         redirect(base_url().'payment/payment_test/paymentFailed');
         $response=array('status'=>false, 'message'=>'Payment Failed' );
        }
    
      }
      else
      {
        $response=array('status'=>false, 'message'=>'Order already Processed' );
      }
    }

    else
    {
        $response=array('status'=>false, 'message'=>$token_result[0]);
    }
    header('Content-Type: application/json');
    echo json_encode($response);
  }

  //------------------------------------------------------------------------------------------------------------------///

  /**
   * This function preprares payment parameters
   * @param $amount
   * @param $razorpayOrderId
   * @return array
   */
  public function prepareData($amount,$razorpayOrderId)
  {
    $data = array(
      "key" => RAZOR_KEY_ID,
      "amount" => $amount,
      "name" => "CHAMPEON FL",
      "description" => "Champeon FL",
      "image" => "https://img.freepik.com/free-vector/realistic-golden-trophy-with-text-space_48799-1062.jpg?size=626&ext=jpg",
      "prefill" => array(
        "name"  => $this->input->post('name'),
        "email"  => $this->input->post('email'),
        "contact" => $this->input->post('contact'),
      ),
      "notes"  => array(
        "address"  => "Hello World",
        "merchant_order_id" => rand(),
      ),
      "theme"  => array(
        "color"  => "#528FF0"
      ),
      "order_id" => $razorpayOrderId,
    );
    return $data;
  }

  //------------------------------------------------------------------------------------------------------------------///

  /**
   * This function saves your form data to session,
   * After successfull payment you can save it to database
   */
  public function setRegistrationData($user_id)
  {
//     $name = $this->input->post('name');
//     $email = $this->input->post('email');
//     $contact = $this->input->post('contact');
    $amount = $_SESSION['payable_amount'];
    
//     $amount_rs = $amount / 100 ;
    
    $transaction_no = uniqid('walletp_',true);      
  
    $registrationData = array(
        'razorpay_order_id' => $_SESSION['razorpay_order_id'],
        'transaction_no' => $transaction_no,
        'type' => 'Bank Payment',
        'amount' => (float)$amount
      );
    $this->db->insert('tb_razor_transactions', $registrationData); 
      
    $transaction = array(
          'transaction_no' => $transaction_no,
          'amount' => $amount,
          'type' => 'Bank Payment',
          'txn_date_time' => date("Y-m-d H:i:s"),
          'fk_user_id' => $user_id,
          'remarks' => 'Amount '. (float)$amount .' added to wallet. '
      );
      $this->db->insert('tb_transactions', $transaction); 

      $wallet_details=$this->db->select('wallet_balance')
        ->from('tb_user_wallet')
        ->where('fk_user_id',$user_id)
        ->get()->result_array();
    
      $wallet_balance = $wallet_details[0]['wallet_balance'];

      $wallet_balance = $wallet_balance + $amount ;

      $wallet_balance_update = array(
          'wallet_balance' => $wallet_balance
      );

      $this->db->where('fk_user_id', $user_id);
      $this->db->update('tb_user_wallet', $wallet_balance_update); 

    
    
    
    // save this to database
  }

  //------------------------------------------------------------------------------------------------------------------///

  /**
   * This is a function called when payment successfull,
   * and shows the success message
   */
  public function success()
  {
//     $this->load->view('success');
    echo "Payment Successful..!!";
  }

  //------------------------------------------------------------------------------------------------------------------///

  /**
   * This is a function called when payment failed,
   * and shows the error message
   */
  public function paymentFailed()
  {
//     $this->load->view('error');
    echo "Payment Failed..!!";
  }
  
  
  //------------------------------------------------------------------------------------------------------------------///


  
  
}