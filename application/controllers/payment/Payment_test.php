<?php defined('BASEPATH') OR exit('No direct script access allowed');

require_once(APPPATH."libraries/razorpay-php/Razorpay.php");
use Razorpay\Api\Api;
use Razorpay\Api\Errors\SignatureVerificationError;


class Payment_test extends CI_Controller
{
  
  public function __construct()
	{
		parent::__construct();
//     header('Content-Type: application/json');
    date_default_timezone_set('Asia/Kolkata');
  }

  //------------------------------------------------------------------------------------------------------------------///

   function payments(){
    $this->load->view('register.php');
   }
  
  //------------------------------------------------------------------------------------------------------------------///

 /**
   * This function creates order and loads the payment methods
   */
  public function pay()
  {
    $api = new Api(RAZOR_KEY_ID, RAZOR_KEY_SECRET);
    /**
     * You can calculate payment amount as per your logic
     * Always set the amount from backend for security reasons
     */
    $_SESSION['payable_amount'] = $_POST['amount'];
    $razorpayOrder = $api->order->create(array(
      'receipt'         => rand(),
      'amount'          => $_SESSION['payable_amount'] * 100, // 2000 rupees in paise
      'currency'        => 'INR',
      'payment_capture' => 1 // auto capture
    ));
    $amount = $razorpayOrder['amount'];
    $razorpayOrderId = $razorpayOrder['id'];
    $_SESSION['razorpay_order_id'] = $razorpayOrderId;
    $data = $this->prepareData($amount,$razorpayOrderId);
    $this->load->view('rezorpay',array('data' => $data));
  }

  //------------------------------------------------------------------------------------------------------------------///
 public function verify1()
  {
    
    if (empty($_POST['razorpay_payment_id']) === false) {
        $attributes = array(
          'razorpay_order_id' => $_SESSION['razorpay_order_id'],
          'razorpay_payment_id' => $_POST['razorpay_payment_id'],
          'razorpay_signature' => $_POST['razorpay_signature']
        );
  
    }
   print_r ($attributes);
 }
      
      
    //------------------------------------------------------------------------------------------------------------------///

  /**
   * This function verifies the payment,after successful payment
   */
  public function verify()
  {
    $success = true;
    $error = "payment_failed";
    if (empty($_POST['razorpay_payment_id']) === false) {
      $api = new Api(RAZOR_KEY_ID, RAZOR_KEY_SECRET);
    try {
        $attributes = array(
          'razorpay_order_id' => $_SESSION['razorpay_order_id'],
          'razorpay_payment_id' => $_POST['razorpay_payment_id'],
          'razorpay_signature' => $_POST['razorpay_signature']
        );
        $api->utility->verifyPaymentSignature($attributes);
      } catch(SignatureVerificationError $e) {
        $success = false;
        $error = 'Razorpay_Error : ' . $e->getMessage();
      }
    }
    if ($success === true) {
      /**
       * Call this function from where ever you want
       * to save save data before of after the payment
       */
      $this->setRegistrationData();
      redirect(base_url().'payment/payment_test/success');
    }
    else {
      redirect(base_url().'payment/payment_test/paymentFailed');
    }
  }

  //------------------------------------------------------------------------------------------------------------------///

  /**
   * This function preprares payment parameters
   * @param $amount
   * @param $razorpayOrderId
   * @return array
   */
  public function prepareData($amount,$razorpayOrderId)
  {
    $data = array(
      "key" => RAZOR_KEY_ID,
      "amount" => $amount,
      "name" => "CHAMPEON FL",
      "description" => "Champeon FL",
      "image" => "https://img.freepik.com/free-vector/realistic-golden-trophy-with-text-space_48799-1062.jpg?size=626&ext=jpg",
      "prefill" => array(
        "name"  => $this->input->post('name'),
        "email"  => $this->input->post('email'),
        "contact" => $this->input->post('contact'),
      ),
      "notes"  => array(
        "address"  => "Hello World",
        "merchant_order_id" => rand(),
      ),
      "theme"  => array(
        "color"  => "#528FF0"
      ),
      "order_id" => $razorpayOrderId,
    );
    return $data;
  }

  //------------------------------------------------------------------------------------------------------------------///

  /**
   * This function saves your form data to session,
   * After successfull payment you can save it to database
   */
  public function setRegistrationData()
  {
    $name = $this->input->post('name');
    $email = $this->input->post('email');
    $contact = $this->input->post('contact');
    $amount = $_SESSION['payable_amount'];
    $registrationData = array(
      'order_id' => $_SESSION['razorpay_order_id'],
      'name' => $name,
      'email' => $email,
      'contact' => $contact,
      'amount' => $amount
    );
    // save this to database
  }

  //------------------------------------------------------------------------------------------------------------------///

  /**
   * This is a function called when payment successfull,
   * and shows the success message
   */
  public function success()
  {
//     $this->load->view('success');
    echo "Payment Successful..!!";
  }

  //------------------------------------------------------------------------------------------------------------------///

  /**
   * This is a function called when payment failed,
   * and shows the error message
   */
  public function paymentFailed()
  {
//     $this->load->view('error');
    echo "Payment Failed..!!";
  }
  
  
  //------------------------------------------------------------------------------------------------------------------///


  
  
}